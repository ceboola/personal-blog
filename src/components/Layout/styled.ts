import styled, { createGlobalStyle, css } from 'styled-components';

import { media } from '@utils/media';
import { rhythm } from '@utils/typography';

export const Container = styled.div`
  margin-left: auto;
  margin-right: auto;
  max-width: ${rhythm(34)};
  padding: ${`${rhythm(1)} 0 0 0`};
  display: flex;
  flex-direction: column;
  min-height: 100vh;
`;

export const GlobalStyle = createGlobalStyle`
  html {
    overflow-x: hidden;
  }
  body {
    overflow: hidden;
    font-family: 'Ubuntu','Georgia',serif;
  }
  h1,h2,h3,h4,h5,h6 {
    font-family: "Montserrat", 'Georgia',serif;
  }
  blockquote {
    ${media.sm`
        margin-left: 0;
    `}
  }
`;

export const NavigationObserver = styled.div`
  position: absolute;
  top: 0;
  width: 100%;
  height: 1px;
`;

export const ContentWrapper = styled.main<{ isHome: boolean }>`
  ${(props) =>
    props.isHome &&
    css`
      ${media.lg`
        margin-left: 10px;
        margin-right: 10px;
      `}
    `};
`;
