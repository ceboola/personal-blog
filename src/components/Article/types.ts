import type { FluidObject } from 'gatsby-image';

export interface Props {
  title: string;
  post: string;
  date: string;
  featuredImage: {
    childImageSharp: {
      fluid: FluidObject;
    };
  };
  slug: string | null | undefined;
  currentPageViews: number | false | undefined;
  timeToRead: number;
}
