import styled, { css } from 'styled-components';

import CommonButton from '@common/CommonButton';
import CommonIcon from '@common/CommonIcon';
import { media } from '@utils/media';

export const Wrapper = styled.div<{ coverEl: boolean }>`
  position: absolute;
  left: 1px;
  align-items: center;
  width: 100%;
  height: 100%;
  margin: 0 15px;
  ${media.xxs`
    margin: 0 7px;
  `}
  ${(props) =>
    props.coverEl &&
    css`
      background-color: #fff;
      z-index: 3;
    `};
`;

export const Search = styled.div<{ show: boolean }>`
  position: relative;
  border-radius: 4px;
  margin-left: 4px;
  width: 100%;
  input[type='search'] {
    height: 40px;
    color: black;
    padding-left: 5px;
    font-weight: 700;
    border-width: 1px;
    background-color: #fff;
    padding-right: 30px;
    &:focus {
      outline: 1px solid #000;
    }
    &::placeholder {
      color: #000;
      font-weight: 400;
    }
    ${media.md`
        display: none;
    `}
  }
  ${(props) =>
    props.show &&
    css`
      width: 100%;
      background-color: #fff;
      ${media.md`
        display: inherit;
        z-index: 1;
        input[type='search'] {
          display: inherit;
        }
      `}
    `};
`;

export const CloseButton = styled(CommonButton)`
  position: absolute;
  right: 15px;
  bottom: -6px;
  padding: 0 !important;
  background-color: transparent !important;
  border: none;
  cursor: pointer;
  ${media.md`
      bottom: 5px;
  `}
`;

export const Phrase = styled.span`
  letter-spacing: 3px;
  color: black;
  position: absolute;
  font-size: 10px;
  top: -13px;
  left: -3px;
  text-transform: lowercase;
  font-weight: 900;
`;

export const StyledIcon = styled(CommonIcon)`
  ${media.xs`
    height: 22px;
    width: 22px;
  `}
`;

export const StyledButton = styled(CommonButton)`
  padding: 0;
  border: none;
  background-color: transparent;
  align-items: center;
  display: flex;
  pointer-events: none;
  ${media.md`
    pointer-events: all;
  `}
`;
