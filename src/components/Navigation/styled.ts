import { Link } from 'gatsby';
import styled, { css, keyframes } from 'styled-components';

import CommonIcon from '@common/CommonIcon';
import { media } from '@utils/media';

const slideAnimation = keyframes`
0% {
  transform: translateY(-142px);
}

100% {
  transform: translateY(0px);
}
`;

export const Container = styled.nav<{ isSticky: boolean }>`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100vw;
  height: 50px;
  position: relative;
  left: 50%;
  right: 50%;
  margin-left: -50vw;
  margin-right: -50vw;
  z-index: 3;
  ${(props) =>
    props.isSticky &&
    css`
      background-color: white;
      position: fixed;
      top: 0;
      transition: all 0.5s ease;
      animation: ${slideAnimation} 1s forwards;
      & > ${List} {
        margin-bottom: 0;
        padding: 10px;
      }
    `};
  & > div {
    display: flex;
    ${media.xs`
      justify-content: center;
  `}
  }
`;

export const List = styled.ul`
  display: flex;
  list-style: none;
  justify-content: center;
  margin: 1px;
  align-items: center;
  height: 100%;
  margin: 0 -5px 0 20px;
  will-change: transform;
`;

export const Item = styled.li`
  position: relative;
  margin: 0 0 0 32px;
  font-size: 16px;
  line-height: 24px;
  font-weight: 900;
  & > a {
    position: relative;
    color: #14143c;
    transition: 0.3s;
  }
  & > a:hover {
    color: #128723;
  }
  & > a:after {
    position: absolute;
    bottom: -6px;
    left: 50%;
    content: '';
    display: flex;
    height: 3px;
    background: #128723;
    transition: width 0.3s ease 0s, left 0.3s ease 0s;
    width: 0;
    overflow: hidden;
  }
  & > a:hover:after {
    width: 100%;
    left: 0;
  }
  ${media.xxs`
      margin: 0 0 0 14px;
      font-size: 14px;
  `}
  ${media.xs`
      font-size: 3.5vw;
  `}
`;

export const Wrapper = styled.div`
  width: 100%;
  justify-content: center;
  margin: 0;
  align-items: center;
`;

export const Languages = styled.div`
  position: absolute;
  right: 30px;
  flex-direction: column;
  ${media.md`
      position: initial;
      right: 10px;
  `}
  ${media.sm`
      right: 0;
  `}
`;

export const StyledIcon = styled(CommonIcon)`
  position: absolute;
  top: 0;
  right: 0;
  ${media.xs`
      &:nth-child(1) {
        width: 12px;
        height: 12px;
      }
    right: -5px;
  `}
`;

export const StyledLink = styled(Link)`
  position: relative;
  box-shadow: none;
  display: inherit;
  width: 40px;
  margin-right: 10px;
  ${media.xs`
      width: 25px;
  `}
`;

export const Description = styled.div`
  display: inherit;
  font-size: 9px;
  justify-content: center;
  ${media.xs`
      display: none;
  `}
`;

export const LinkWrapper = styled.div`
  display: flex;
  flex-direction: row;
  ${media.xs`
      svg {
        width: 25px;
        height: 25px;
      }
  `}
`;
