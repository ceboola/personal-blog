import { Link } from 'gatsby';
import styled from 'styled-components';

import { media } from '@utils/media';

export const StyledLink = styled(Link)`
  box-shadow: none;
  color: #000;
  background-image: linear-gradient(#00c899, #00c899);
  background-size: 100% 3px;
  background-position: 0% 100%;
  background-repeat: no-repeat;
`;

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  ${media.md`
    flex: 0 1 100%;
  `}
`;

export const Row = styled.div`
  display: flex;
  flex-direction: row;
`;
