import { memo, useContext } from 'react';

import loadable from '@loadable/component';
import { useIntl } from 'gatsby-plugin-react-intl';

import CommonIcon from '@common/CommonIcon';
import NavLink from '@components/NavLink';
import { PostsContext } from '@providers/PostsProvider';

import { navData } from './static';

const SearchWidget = loadable(
  () =>
    import(
      /* webpackChunkName: "SearchWidget" */ '@components/Search/SearchWidget'
    ),
);

import {
  Container,
  List,
  Item,
  Wrapper,
  Languages,
  StyledIcon,
  StyledLink,
  Description,
  LinkWrapper,
} from './styled';
import { Props } from './types';

const Navigation: React.FC<Props> = ({ refs, isSticky }) => {
  const {
    pageContext: { clearSlug },
  } = useContext(PostsContext);
  const intl = useIntl();
  const locale = intl.locale !== 'en' ? `/pl` : '';

  return (
    <Container isSticky={isSticky} ref={refs}>
      <SearchWidget />
      <Wrapper>
        <List>
          {navData.map(({ url, name }) => {
            return (
              <Item key={`${name}_${url}`}>
                <NavLink to={`${locale}${url}`}>
                  {intl.formatMessage({ id: name })}
                </NavLink>
              </Item>
            );
          })}
        </List>
      </Wrapper>
      <Languages>
        <Description>
          {intl.formatMessage({ id: 'choose_language' })}
        </Description>
        <LinkWrapper>
          <StyledLink to={`${clearSlug}`}>
            {intl.locale === 'en' && (
              <StyledIcon name="checkbox" width={16} height={16} />
            )}
            <CommonIcon
              aria-label="english version of site"
              name="united-states"
              width={60}
              height={30}
            />
          </StyledLink>
          <StyledLink to={`/pl${clearSlug}`}>
            {intl.locale === 'pl' && (
              <StyledIcon name="checkbox" width={16} height={16} />
            )}
            <CommonIcon
              aria-label="polish version of site"
              name="poland"
              width={60}
              height={30}
            />
          </StyledLink>
        </LinkWrapper>
      </Languages>
    </Container>
  );
};

export default memo(Navigation);
