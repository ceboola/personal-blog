export const parsePathname = (
  pathname: string,
): (string | null | undefined | number)[] => {
  // if (typeof pathname !== 'string') throw Error('The pathname is not a string');
  // console.log('pathname', pathname);
  // if (!/^\//.test(pathname))
  //   throw Error("The pathname doesn't start with a slash");
  const slashes = pathname.match(/(\/)+/gi);
  const firstPart = pathname.match(/([a-z]+)/gi);
  const endsWithSlash = /\/$/.test(pathname);
  const slashNumber = endsWithSlash
    ? slashes?.length
    : (slashes?.length as number) + 1;
  return [slashNumber, firstPart && firstPart[0].toLowerCase()];
};
