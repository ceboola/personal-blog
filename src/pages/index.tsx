import { useContext, memo } from 'react';

import loadable from '@loadable/component';
import { PageProps, graphql, Link } from 'gatsby';
import { useIntl } from 'gatsby-plugin-react-intl';

import SEO from '@components/SEO';
import PostsProvider, { PostsContext } from '@providers/PostsProvider';

const SiteHeader = loadable(
  () => import(/* webpackChunkName: "SiteHeader" */ '@components/SiteHeader'),
);
const PopularArticles = loadable(
  () =>
    import(
      /* webpackChunkName: "PopularArticles" */ '@components/PopularArticles'
    ),
);
const TagsList = loadable(
  () => import(/* webpackChunkName: "TagsList" */ '@components/TagsList'),
);
const ArticleStart = loadable(
  () =>
    import(/* webpackChunkName: "ArticleStart" */ '@components/ArticleStart'),
);

const BlogIndex: React.FC<PageProps<QueryProps>> = ({ location }) => {
  const { data, posts, pageContext } = useContext(PostsContext);
  const siteTitle = data.site.siteMetadata.title;
  const totalPosts = data.totalPosts.totalCount;
  const tags = data.tags.group;
  const intl = useIntl();

  return (
    <>
      <SEO
        title={intl.formatMessage({ id: 'all_posts' })}
        description={intl.formatMessage({ id: 'seo.description' })}
      />
      <SiteHeader title={siteTitle} location={location} />
      <PostsProvider data={data} posts={posts} pageContext={pageContext}>
        <PopularArticles />
        <ArticleStart />
      </PostsProvider>
      <TagsList
        link={Link}
        tags={tags}
        totalPosts={totalPosts}
        slug={location.pathname}
      />
    </>
  );
};

export default memo(BlogIndex);

export const pageQuery = graphql`
  fragment CurrentProject on Gitlab {
    name
    statistics {
      commit_count
    }
    web_url
    created_at(formatString: "DD.MM.YYYY")
    usedLanguages {
      CSS
      Dockerfile
      HTML
      JavaScript
      TypeScript
      Pug
      Shell
    }
  }
  fragment SitePageViews on PageViews {
    totalCount
    totalSessions
  }
  fragment SummarizedStats on SummarizedStats {
    articles
    articlesEn
    articlesPl
    commits
    projects
    comments
    totalTimeToRead
    totalTimeToReadPl
    totalTimeToReadEn
    totalTags
    totalUniqueTags
    projectSize
    mostPopularTagEn {
      fieldValue
      totalCount
    }
    mostPopularTagPl {
      fieldValue
      totalCount
    }
    programmingLanguages {
      CSS
      Dockerfile
      HTML
      JavaScript
      TypeScript
      Pug
      Shell
    }
    pages {
      path
      totalCount
      totalSessions
    }
  }
  fragment ProjectStatsClocFragment on ProjectStatsCloc {
    Bourne_Shell {
      nFiles
      code
    }
    Dockerfile {
      nFiles
      code
    }
    JSON {
      nFiles
      code
    }
    JavaScript {
      nFiles
      code
    }
    TypeScript {
      nFiles
      code
    }
    YAML {
      nFiles
      code
    }
    Markdown {
      nFiles
      code
    }
    SVG {
      nFiles
      code
    }
  }
  fragment TotalProjectStatsClocFragment on ProjectStatsCloc {
    SUM {
      code
      nFiles
    }
  }
  query ($language: String!, $slug: String!) {
    site {
      buildTime(formatString: "DD/MM/YYYY")
      siteMetadata {
        title
      }
    }
    SummarizedStats {
      ...SummarizedStats
    }
    gitlab(name: { eq: "personal-blog" }) {
      ...CurrentProject
    }
    currentPageViews: pageViews(path: { eq: $slug }) {
      ...SitePageViews
    }
    totalPosts: allMarkdownRemark(
      filter: { frontmatter: { lang: { eq: $language } } }
    ) {
      totalCount
    }
    tags: allMarkdownRemark(
      filter: { frontmatter: { lang: { eq: $language } } }
    ) {
      group(field: frontmatter___tags) {
        fieldValue
        totalCount
      }
    }
    allMarkdownRemark(sort: { fields: [frontmatter___date], order: DESC }) {
      totalCount
      edges {
        node {
          timeToRead
          excerpt
          fields {
            slug
          }
          frontmatter {
            date(formatString: "MMMM DD, YYYY")
            title
            description
            tags
            category
            lang
            featuredImage {
              childImageSharp {
                fluid(quality: 70) {
                  ...GatsbyImageSharpFluid_withWebp
                }
              }
            }
          }
        }
      }
    }
    GAData: allPageViews(sort: { order: DESC, fields: totalCount }) {
      edges {
        node {
          id
          totalCount
        }
      }
    }
    Placeholder: file(relativePath: { eq: "placeholder.jpg" }) {
      childImageSharp {
        fluid(quality: 70) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`;
