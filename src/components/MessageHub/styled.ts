import { animated } from 'react-spring';
import styled from 'styled-components';

import CommonIcon from '@common/CommonIcon';
import { media } from '@utils/media';

export const Main = styled('div')`
  position: relative;
  width: 100%;
  height: 100%;
  overflow: hidden;
  cursor: pointer;
  display: flex;
  justify-content: center;
  align-items: center;
  color: #676767;
  z-index: 3;
`;

export const Container = styled('div')`
  position: fixed;
  z-index: 2;
  right: 5px;
  bottom: 0;
  display: flex;
  flex-direction: column;
  pointer-events: none;
  align-items: center;
`;

export const Message = styled(animated.div)`
  box-sizing: border-box;
  position: relative;
  overflow: hidden;
  width: 320px;
  height: auto;
  margin-top: 12px;
`;

export const Content = styled('div')`
  position: relative;
  color: white;
  background: #47426f;
  font-size: 1em;
  display: flex;
  align-self: stretch;
  box-sizing: border-box;
  overflow: hidden;
  height: auto;
  border-radius: 3px;
  margin-bottom: 0;
  font-family: 'Montserrat';
  font-size: 13px;
  font-weight: 600;
`;

export const Button = styled('button')`
  position: absolute;
  right: 0;
  top: 0;
  height: auto;
  padding: 5px 10px;
  cursor: pointer;
  pointer-events: all;
  outline: 0;
  border: none;
  background: transparent;
  display: flex;
  align-self: flex-end;
  margin: 0;
  padding: 0;
`;

export const Life = styled(animated.div)`
  position: absolute;
  bottom: 0;
  left: 0px;
  width: auto;
  background-image: linear-gradient(130deg, #47426f, #00f0e0);
  height: 5px;
`;

export const Description = styled.p`
  display: flex;
  align-self: stretch;
  width: 100%;
  height: 100%;
  justify-content: center;
  align-items: center;
  margin: 12px 22px;
  box-sizing: border-box;
`;

export const StyledIcon = styled(CommonIcon)`
  fill: #17173f !important;
  margin: 7px;
`;
