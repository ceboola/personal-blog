---
lang: en
title: Renovate with Gitlab CI/CD
date: '2021-05-03T01:00:00.284Z'
featuredImage: ../images/renovate-with-gitlab-ci/renovate.jpeg
tags: ['gitlab', 'renovate', 'devops']
category: 'DevOps'
---

Hello guys, today I just wanna share with you something that will change yours minds. Did you had such a situation when you comes to work and somebody told you to update dependencies? Probably most of us had similiar situation and basically its a pain in the ass... Recently I have found on the internet package with doing that stuff for us completely automated! Its called <b>Renovate</b>

### What is Renovate?

<b>Renovate</b> its basically runner which automatically updates dependencies (scanning git for new packages) for us in some specified intervals of time. It is fully customizable so you dont need to worry about some limitations, for me game changer about it was I dont really need track my dependencies manually and finally I can focus on my code, and of course closer the newer version you are the less time-consuming this could be (fixes after upgrading 2-3 majors in a row).

#### Gitlab with Renovate

Configuring this two things its a piece of cake! It requires less than 5 steps.

### Create Gitlab project

First create new Gitlab project for Renovate runner, this is the place where you will configure runner.

![Create Project](../images/renovate-with-gitlab-ci/renovategitlab.png)

### Create .gitlab-ci.yml

```yml
include:
  - project: 'renovate-bot/renovate-runner'
    file: '/templates/renovate-dind.gitlab-ci.yml'
```

### Create schedule pipeline

![Create schedule pipeline](../images/renovate-with-gitlab-ci/renovategitlab2.png)

Now inside you can change how often this pipeline will be triggered, my routine looks like this (each hour check for new dependencies):

Click new schedule
![Pipeline routines](../images/renovate-with-gitlab-ci/renovategitlab3.png)

### Create gitlab token

Now lets create gitlab token for renovate runner with permissions to <b>api, read_user, write_repository</b>

![Access Tokens](../images/renovate-with-gitlab-ci/renovategitlab4.png)

![Token Permission](../images/renovate-with-gitlab-ci/renovategitlab5.png)

### Create github token

This step is not neccessary but basically abolishes the limit of scanning git repositories by renovate runner, so its better to have it :), this token needs only minimal scopes means just generate it without any checkboxes. Go to <b>Settings > Developer settings > Personal access tokens > generate new token</b>

![Github token](../images/renovate-with-gitlab-ci/renovategitlab6.png)

Copy this tokens (Gitlab & Github) and go to <b>Settings > CI/CD > Variables</b>, now you need to fill 3 fields with <b>GITHUB_COM_TOKEN RENOVATE_TOKEN RENOVATE_EXTRA_FLAGS</b>

![CI/CD variables](../images/renovate-with-gitlab-ci/renovategitlab7.png)

And basically thats it! Your runner is up and ready to listen for internal projects, just one small detail separates you from success, now lets go back to the project where you want to use it.

### Go back to target project

Here in your target project where you want to auto-update dependencies create new folder called <b>.gitlab</b> in the root of your project. Inside create another file called <b>renovate.json</b> -> this is our config to tell renovate how it should behaves

```json
{
  "extends": ["config:base"],
  "prHourlyLimit": 0, // limit of pull request by hour, 0 unlimited
  "prConcurrentLimit": 0, // limit of opened merge request at the moment, 0 unlimited
  "ignoreDeps": ["graphql"], // dependencies with we dont wanna auto updated by runner
  "packageRules": [{ "languages": ["js"], "addLabels": ["javascript"] }] // adds labels
}
```

This is the simpliest one, for more config options check [documentation](https://docs.renovatebot.com/configuration-options/)

After all these steps your Renovate runner is ready for action!
