import { createContext, useMemo, useState } from 'react';

import firebase from 'gatsby-plugin-firebase';

import { useIntl } from 'gatsby-plugin-react-intl';

import { SitePageContext } from '@typings/graphql-types';

type PostCategory = AllMarkdownRemarkEdge['node']['frontmatter']['category'];
interface IPostsContext {
  userStore: {
    greetingUser: {
      greeting: boolean;
      setGreeting: (state: boolean) => void;
    };
    firebase: {
      dbData: firebase.firestore.Firestore | undefined;
      setDbData: (state: firebase.firestore.Firestore) => void;
    };
  };
  posts: AllMarkdownRemarkEdge[];
  setPostsCategory: (category: PostCategory) => void;
  data: QueryProps;
  postCategory: PostCategory;
  pageContext: SitePageContext;
}

const Provider: React.FC<{
  posts: IPostsContext['posts'];
  data: IPostsContext['data'];
  pageContext: IPostsContext['pageContext'];
}> = ({ children, data, pageContext }) => {
  const intl = useIntl();
  const defaultCategory = intl.formatMessage({ id: 'all_posts' });
  const [postCategory, setPostsCategory] =
    useState<PostCategory>(defaultCategory);
  const [greeting, setGreeting] = useState(true);
  const [dbData, setDbData] = useState<
    firebase.firestore.Firestore | undefined
  >();
  const filteredPosts = useMemo(() => {
    return postCategory === defaultCategory
      ? data.allMarkdownRemark?.edges
      : data.allMarkdownRemark?.edges?.filter(
          ({ node }) => node.frontmatter.category === postCategory,
        );
  }, [postCategory, defaultCategory, data.allMarkdownRemark]);

  const userStore = useMemo(
    () => ({
      greetingUser: {
        greeting,
        setGreeting,
      },
      firebase: {
        dbData,
        setDbData,
      },
    }),
    [dbData, greeting],
  );

  const value = useMemo(
    () => ({
      userStore,
      posts: filteredPosts,
      setPostsCategory,
      data,
      postCategory,
      pageContext,
    }),
    [data, filteredPosts, pageContext, postCategory, userStore],
  );

  return (
    <PostsContext.Provider value={value}>{children}</PostsContext.Provider>
  );
};

const projectStatsDefaultVal = {
  blank: 0,
  code: 0,
  comment: 0,
  nFiles: 0,
};

const defaultValue: IPostsContext = {
  userStore: {
    greetingUser: {
      greeting: true,
      // eslint-disable-next-line @typescript-eslint/no-empty-function
      setGreeting() {},
    },
    firebase: {
      dbData: undefined,
      // eslint-disable-next-line @typescript-eslint/no-empty-function
      setDbData() {},
    },
  },
  postCategory: '',
  posts: [],
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  setPostsCategory() {},
  data: {
    markdownRemark: {
      excerpt: '',
      html: '',
      timeToRead: 0,
      frontmatter: {
        title: '',
        description: '',
        rawDate: '',
        date: '',
        tags: [],
        lang: '',
        featuredImage: {
          childImageSharp: {
            fluid: {
              aspectRatio: 0,
              src: '',
              srcSet: '',
              sizes: '',
            },
          },
        },
      },
      fields: {
        slug: '',
      },
    },
    site: {
      buildTime: '',
      siteMetadata: {
        title: '',
      },
    },
    tags: {
      group: [],
    },
    totalPosts: {
      totalCount: 0,
    },
    allMarkdownRemark: {
      totalCount: 0,
      edges: [],
      group: [],
    },
    pageContext: {
      tag: '',
    },
    GAData: {
      edges: [
        {
          node: {
            id: '',
            totalCount: 0,
          },
        },
      ],
    },
    currentPageViews: {
      totalCount: 0,
      totalSessions: 0,
    },
    Placeholder: {
      childImageSharp: {
        fluid: {
          aspectRatio: 0,
          src: '',
          srcSet: '',
          sizes: '',
        },
      },
    },
    SummarizedStats: {
      articles: 0,
      articlesEn: 0,
      articlesPl: 0,
      commits: 0,
      projects: 0,
      comments: 0,
      totalTimeToRead: 0,
      totalTimeToReadEn: 0,
      totalTimeToReadPl: 0,
      mostPopularTagEn: {
        fieldValue: '',
        totalCount: 0,
      },
      mostPopularTagPl: {
        fieldValue: '',
        totalCount: 0,
      },
      totalTags: 0,
      totalUniqueTags: 0,
      projectSize: 0,
      programmingLanguages: {
        CSS: 0,
        Dockerfile: 0,
        HTML: 0,
        JavaScript: 0,
        Pug: 0,
        Shell: 0,
        TypeScript: 0,
      },
      pages: [
        {
          path: '',
          totalCount: 0,
          totalSessions: 0,
        },
      ],
    },
    gitlab: {
      web_url: '',
      name: '',
      statistics: {
        commit_count: 0,
      },
      created_at: '',
      usedLanguages: [],
    },
    allPages: {
      totalCount: 0,
    },
    totalPageViewsSite: {
      sum: 0,
    },
    totalSessionsSite: {
      sum: 0,
    },
    projectStatsCloc: {
      Bourne_Shell: projectStatsDefaultVal,
      Dockerfile: projectStatsDefaultVal,
      JSON: projectStatsDefaultVal,
      JavaScript: projectStatsDefaultVal,
      Markdown: projectStatsDefaultVal,
      SVG: projectStatsDefaultVal,
      TypeScript: projectStatsDefaultVal,
      YAML: projectStatsDefaultVal,
    },
    totalProjectStatsCloc: {
      SUM: projectStatsDefaultVal,
    },
  },
  pageContext: {
    posts: [],
    slug: '',
    previous: {},
    next: {},
    lang: '',
    plPostsFiltered: [],
    previousPl: {},
    nextPl: {},
    previousEn: {},
    nextEn: {},
    language: '',
    intl: {},
    tag: '',
  },
};

export const PostsContext = createContext(defaultValue);

export default Provider;
