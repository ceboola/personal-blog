import {
  Wrapper,
  PrevItem,
  NextItem,
  Teaser,
  Container,
  Info,
  Title,
  Description,
  StyledIcon,
} from './styled';
import type { IArticleNavProps, Props } from './types';

const Prev: React.FC<IArticleNavProps> = ({ title, desc, exc, link }) => (
  <PrevItem to={`${link}`} rel="prev">
    <Teaser reverse={false}>
      <Container>
        <StyledIcon rotate={true} name="next" />
      </Container>
      <Info>
        <Title>{title}</Title>
        <Description>{desc ? desc : exc}</Description>
      </Info>
    </Teaser>
  </PrevItem>
);

const Next: React.FC<IArticleNavProps> = ({ title, desc, exc, link }) => (
  <NextItem to={`${link}`} rel="next">
    <Teaser reverse={true}>
      <Container>
        <StyledIcon rotate={false} name="next" />
      </Container>
      <Info>
        <Title>{title}</Title>
        <Description>{desc ? desc : exc}</Description>
      </Info>
    </Teaser>
  </NextItem>
);
const ArticleNav: React.FC<Props> = ({ pageContext }) => {
  const { previousPl, nextPl, previousEn, nextEn } = pageContext;

  return (
    <Wrapper>
      {previousPl && pageContext.lang === 'pl' && (
        <Prev
          title={previousPl.node?.frontmatter?.title ?? ''}
          desc={previousPl.node?.frontmatter?.description ?? ''}
          exc={previousPl.node?.excerpt ?? ''}
          link={previousPl?.node?.fields?.slug ?? ''}
        />
      )}
      {previousEn && pageContext.lang === 'en' && (
        <Prev
          title={previousEn.node?.frontmatter?.title ?? ''}
          desc={previousEn.node?.frontmatter?.description ?? ''}
          exc={previousEn.node?.excerpt ?? ''}
          link={previousEn?.node?.fields?.slug ?? ''}
        />
      )}
      {nextPl && pageContext.lang === 'pl' && (
        <Next
          title={nextPl.node?.frontmatter?.title ?? ''}
          desc={nextPl.node?.frontmatter?.description ?? ''}
          exc={nextPl.node?.excerpt ?? ''}
          link={nextPl?.node?.fields?.slug ?? ''}
        />
      )}
      {nextEn && pageContext.lang === 'en' && (
        <Next
          title={nextEn.node?.frontmatter?.title ?? ''}
          desc={nextEn.node?.frontmatter?.description ?? ''}
          exc={nextEn.node?.excerpt ?? ''}
          link={nextEn?.node?.fields?.slug ?? ''}
        />
      )}
    </Wrapper>
  );
};

export default ArticleNav;
