export interface SocialMediaDataProps {
  icon: string;
  url: string;
}
