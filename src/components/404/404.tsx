import { useContext } from 'react';

import { PageRendererProps } from 'gatsby';
import { useIntl } from 'gatsby-plugin-react-intl';

import SEO from '@components/SEO';
import SiteHeader from '@components/SiteHeader';
import { PostsContext } from '@providers/PostsProvider';

import { Description, Title } from './styled';

const Page404: React.FC<PageRendererProps> = ({ location }) => {
  const { data } = useContext(PostsContext);
  const { title } = data.site.siteMetadata;
  const intl = useIntl();

  return (
    <>
      <SiteHeader title={title} location={location} />
      <SEO title={`404: ${intl.formatMessage({ id: 'notFound.title' })}`} />
      <Title>{intl.formatMessage({ id: 'notFound.title' })}</Title>
      <Description>
        {intl.formatMessage({ id: 'notFound.description' })}
      </Description>
    </>
  );
};

export default Page404;
