# stage1 as builder
FROM ceboola/blog-image:1.2 as builder

# Copy source files
WORKDIR /app 
COPY . ./

# Install dependencies
RUN yarn install
RUN yarn total_count

# Build prod
RUN yarn build

# ----------------------------------
# Prepare production environment
FROM nginx:alpine
# ----------------------------------

# Clean nginx
RUN rm -rf /usr/share/nginx/html/*

# Copy dist
COPY --from=builder /app/public /usr/share/nginx/html
COPY nginx.conf /etc/nginx/nginx.conf

WORKDIR /usr/share/nginx/html

# Permission
RUN chown root /usr/share/nginx/html/*
RUN chmod 755 /usr/share/nginx/html/*

# Expose port
EXPOSE 5000

# Start
CMD ["nginx", "-g", "daemon off;"]
