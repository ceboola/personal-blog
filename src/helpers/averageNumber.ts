export const averageNumber = (arr: number[]): number =>
  arr.reduce((acc, v) => acc + v) / arr.length;
