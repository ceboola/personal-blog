import { useContext, useEffect, useState } from 'react';

import { useIntl } from 'gatsby-plugin-react-intl';
import { Helmet } from 'react-helmet';

import CommonIcon from '@common/CommonIcon';
import Loader from '@common/Loader';
import { getSearchParamsObj } from '@helpers/getSearchParamsObj';

import { PostsContext } from '@providers/PostsProvider';

import { Wrapper, Unsub } from './styled';

const Unsubscribe: React.FC = () => {
  const { data } = useContext(PostsContext);
  const { title } = data.site.siteMetadata;
  const intl = useIntl();
  const [unsub, setUnsub] = useState({
    state: false,
    text: intl.formatMessage({
      id: 'newsletter.unsubscribe_email_pending',
    }),
    imageName: '',
  });

  useEffect(() => {
    const { email } = getSearchParamsObj();

    fetch('https://api.codesigh.com/unsubscribe', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: email,
      }),
    })
      .then((res) => res.json())
      .then((res) => {
        if (res.message.delete) {
          setUnsub({
            state: true,
            text: intl.formatMessage({
              id: 'newsletter.unsubscribe_email_deleted',
            }),
            imageName: 'checkbox',
          });
        } else {
          setUnsub({
            state: true,
            text: intl.formatMessage({
              id: 'newsletter.unsubscribe_email_already_deleted',
            }),
            imageName: 'close',
          });
        }
      })
      .catch((err) => {
        console.error('here is the error: ', err);
        setUnsub({
          state: true,
          text: intl.formatMessage({
            id: 'newsletter.unsubscribe_email_not_exists',
          }),
          imageName: 'close',
        });
      });
  }, [intl]);

  return (
    <Wrapper>
      <Helmet
        title={`${intl.formatMessage({
          id: 'newsletter.unsubscribe',
        })} | ${title}`}
      />
      <h1>{intl.formatMessage({ id: 'newsletter.unsubscribe' })}</h1>
      <div>
        <Unsub>
          {!unsub.state ? (
            <>
              <Loader width={70} height={70} /> {unsub.text}
            </>
          ) : (
            <>
              <CommonIcon name={unsub.imageName} height={70} width={70} />
              {unsub.text}
            </>
          )}
        </Unsub>
      </div>
    </Wrapper>
  );
};

export default Unsubscribe;
