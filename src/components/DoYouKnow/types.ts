export interface DoYouKnowProps {
  children: JSX.Element;
}
