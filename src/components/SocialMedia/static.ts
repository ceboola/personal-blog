import { SocialMediaDataProps } from './types';

export const socialMediaData: SocialMediaDataProps[] = [
  {
    icon: 'twitter',
    url: '/twitter',
  },
  {
    icon: 'gitlab',
    url: 'https://gitlab.com/ceboola',
  },
  {
    icon: 'github',
    url: 'https://github.com/ceboola',
  },
];
