import styled, { css } from 'styled-components';

export const TimeToRead = styled.div<{ show?: boolean }>`
  opacity: 0;
  margin-top: auto;
  font-size: 10px;
  transition: all 0.25s ease-in-out;
  display: flex;
  align-items: flex-end;
  margin: 5px 0;
  font-family: 'Montserrat';
  width: 100%;
  ${(props) =>
    props.show &&
    css`
      opacity: 1;
    `};
`;

export const Wrapper = styled.div`
  display: flex;
  align-items: center;
  width: 100%;
  & > svg {
    fill: #00c899;
    margin-right: 4px;
  }
`;
