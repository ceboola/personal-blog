import loadable from '@loadable/component';
import styled, { css } from 'styled-components';

const Textfit = loadable(
  () => import(/* webpackChunkName: "Textfit" */ 'react-textfit'),
);

import CommonIcon from '@common/CommonIcon';

export const Container = styled.div<{
  show: boolean | number | undefined;
  fade?: boolean;
}>`
  display: ${(props) => (props.show ? 'unset' : 'none')};
  transition: all 0.25s ease-in-out;
  opacity: 0;
  ${(props) =>
    props.fade &&
    css`
      opacity: 1;
    `};
`;

export const CommentsWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  padding: 4px 8px;
  background-color: #47426f;
  font-weight: 900;
  color: #fff;
  min-width: 40px;
  min-height: 40px;
`;

export const StyledIcon = styled(CommonIcon)`
  transform: none;
  fill: #fff;
`;

export const Count = styled.div<{
  width: number;
}>`
  position: absolute;
  font-weight: 700;
  font-family: 'Montserrat';
  width: ${(props) => (props.width > 400 ? '33px' : '26px')};
  height: ${(props) => (props.width > 400 ? '33px' : '26px')};
  padding: 0 4px;
`;

export const ViewsWrapper = styled(CommentsWrapper)`
  right: 70px;
  background-color: #00c899;
`;

export const StatsWrapper = styled.div`
  position: absolute;
  bottom: 0;
  right: 15px;
  display: flex;
  flex-direction: row-reverse;
  z-index: 1;
  min-width: 40px;
  min-height: 40px;
`;

export const Description = styled.span`
  position: relative;
  font-size: 7px;
  padding: 0;
  margin: 0;
  font-weight: 900;
  display: flex;
`;

export const Comments = styled.div`
  padding: 0 10px;
  background-color: #47426f;
  font-weight: 900;
  color: #fff;
  z-index: 2;
  min-width: 30px;
  text-align: center;
`;

export const Views = styled(Comments)`
  right: 30px;
  background-color: #00c899;
`;

export const StatsWrapperSmall = styled.div`
  display: flex;
  flex-direction: row-reverse;
  position: absolute;
  bottom: 0;
  right: 0;
  font-size: 15px;
`;

export const StyledTextFit = styled(Textfit)`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 3px;
`;
