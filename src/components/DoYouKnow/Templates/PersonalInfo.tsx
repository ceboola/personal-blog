import { useContext } from 'react';

import { useIntl } from 'gatsby-plugin-react-intl';

import CommonIcon from '@common/CommonIcon';
import { PostsContext } from '@providers/PostsProvider';

import { Underline, Languages } from './styled';

export const PersonalInfo = (): JSX.Element => {
  const intl = useIntl();
  const {
    data: {
      SummarizedStats: { articles, articlesEn, articlesPl, commits, projects },
    },
  } = useContext(PostsContext);
  return (
    <div>
      {intl.formatMessage(
        {
          id: 'doYouKnow.description',
        },
        {
          commitsSum: (
            <Underline
              to="https://gitlab.com/ceboola"
              target="_blank"
              rel="noopener norefferer noreferrer"
            >
              {commits}
            </Underline>
          ),
          projectsSum: (
            <Underline
              to="https://gitlab.com/users/ceboola/projects"
              target="_blank"
              rel="noopener norefferer noreferrer"
            >
              {projects}
            </Underline>
          ),
          articlesSum: <Underline to="/">{articles}</Underline>,
        },
      )}
      <Languages>
        <CommonIcon name="united-states" height={32} width={32} />
        <Underline to="/">{articlesEn}</Underline>

        <CommonIcon name="poland" height={32} width={32} />
        <Underline to="/pl/">{articlesPl}</Underline>
      </Languages>
    </div>
  );
};
