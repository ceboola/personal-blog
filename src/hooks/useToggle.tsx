import { useState, useCallback, useEffect } from 'react';

export const useToggle = (
  initialValue: boolean,
): readonly [boolean, () => void] => {
  const [value, setValue] = useState(initialValue);
  useEffect(() => {
    setValue(initialValue);
  }, [initialValue]);

  const toggle = useCallback(() => {
    setValue((v) => !v);
  }, []);
  return [value, toggle] as const;
};
