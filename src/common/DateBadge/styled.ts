import styled from 'styled-components';

export const Date = styled.div`
  display: flex;
  flex-direction: column;
  background-color: #47426f;
  color: #fff;
  font-size: 18px;
  font-weight: 900;
  padding: 0 16px;
  position: absolute;
  right: 15px;
  text-align: center;
  text-transform: uppercase;
  top: 0;
  z-index: 1;
  transition: all 0.25s ease;
  overflow: hidden;
  font-family: 'Montserrat';
`;

export const Day = styled.span`
  font-size: 16px;
`;

export const Month = styled.span`
  font-size: 11px;
`;

export const Year = styled.span`
  font-size: 13px;
`;
