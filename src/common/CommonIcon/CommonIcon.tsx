import { memo } from 'react';

import { StyledSvg } from './styled';
import type { CommonIconProps } from './types';

const reqSvgs = require.context(
  '!!svg-sprite-loader!../../../static/assets',
  false,
  /\.svg$/,
);

// clean all up to obtain an object who looks like:
// {"./logo.svg": "/static/media/logo.83ae646d.svg"}
const svgs = reqSvgs
  .keys()
  .reduce((images: Record<string, { id: string; viewBox: string }>, path) => {
    const svg = reqSvgs(path).default;
    images[svg.id] = svg;
    return images;
  }, {});

const Icon: React.FC<CommonIconProps> = ({
  name,
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  viewBox: _,
  width,
  height,
  ...otherProps
}) => {
  const { id, viewBox } = svgs[name];

  return (
    <StyledSvg width={width} height={height} viewBox={viewBox} {...otherProps}>
      <use xlinkHref={`#${id}`} />
    </StyledSvg>
  );
};

export default memo(Icon);
