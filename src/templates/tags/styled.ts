import { Link } from 'gatsby';
import styled from 'styled-components';

import { media } from '@utils/media';

export const Title = styled.h1`
  font-size: 32px;
  margin-left: 20px;
`;

export const StyledLink = styled(Link)`
  background-image: linear-gradient(#00c899, #00c899);
  background-size: 100% 3px;
  background-position: 0% 100%;
  background-repeat: no-repeat;
  box-shadow: none;
  color: unset;
  padding-bottom: 4px;
  &:hover {
    color: #00c899;
  }
  ${media.lg`
    margin-left: 20px;
`}
`;

export const Phrase = styled.span`
  background-image: linear-gradient(#00c899, #00c899);
  background-size: 100% 7px;
  background-position: 0% 100%;
  background-repeat: no-repeat;
`;
