import { FC } from 'react';

import { useInView } from 'react-intersection-observer';
import { useSpring } from 'react-spring';

import { roundNumber } from '@helpers/roundNumber';

import {
  Vertical,
  ProgressBar,
  Percent,
  Language,
  Wrapper,
  StyledIcon,
} from './styled';
import { VerticalProgressProps } from './types';

const VerticalProgress: FC<VerticalProgressProps> = ({
  progress,
  name,
  iconName,
}): JSX.Element => {
  const { ref, inView } = useInView({
    threshold: [1],
    triggerOnce: true,
  });

  const styles = useSpring({
    to: {
      height: inView ? `${progress}%` : '0%',
      progress: inView ? progress : 0,
    },
    from: { height: '0%', progress: 0 },
    config: { duration: 1000, mass: 100, tension: 280, friction: 60 },
  });

  return (
    <>
      {progress !== null && (
        <Wrapper ref={ref}>
          <Vertical>
            <ProgressBar style={styles}>
              <Percent>
                {styles.progress.to(
                  (progress: number) => `${roundNumber(progress, 2)}%`,
                )}
              </Percent>
            </ProgressBar>
          </Vertical>
          <Language>{name}</Language>
          <StyledIcon
            name={iconName.toLocaleLowerCase()}
            width={18}
            height={18}
          />
        </Wrapper>
      )}
    </>
  );
};

export default VerticalProgress;
