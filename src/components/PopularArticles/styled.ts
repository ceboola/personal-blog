import styled from 'styled-components';

import CommonIcon from '@common/CommonIcon';
import { media } from '@utils/media';
import { rhythm } from '@utils/typography';

export const Wrapper = styled.div`
  color: #fff;
  position: relative;
  display: flex;
  flex-wrap: wrap;
  margin-left: ${`calc(-100vw / 2 + ${rhythm(36)} / 2)`};
  margin-right: ${`calc(-100vw / 2 + ${rhythm(36)} / 2)`};
  height: auto;
  background-color: #17173f;
  padding: 10px 20px 78px 20px;
  margin-bottom: ${rhythm(1.5)};
  ${media.lg`
      margin-left: 0;
      margin-right: 0;
      padding: 10px;
  `}
  ${media.sm`
      padding: 5px;
  `}
`;

export const Container = styled.div`
  display: inherit;
  width: 100%;
  height: 100%;
  flex-flow: row wrap;
  align-items: stretch;
  justify-content: flex-start;
`;

export const StyledIcon = styled(CommonIcon)<{
  width?: string;
  height?: string;
}>`
  position: absolute;
  top: 50%;
  left: 50%;
  border-radius: 50%;
  width: ${(props) => (props.width ? props.width : '50%')};
  height: ${(props) => (props.height ? props.height : '50%')};
  line-height: ${(props) => (props.height ? props.height : '50%')};
  background: #f3ecff;
  box-shadow: 0 0 5px rgba(0, 0, 0, 0.15);
  transform: translate(-50%, -50%) scale(0);
  transition: all 300ms 0ms cubic-bezier(0.6, -0.28, 0.735, 0.045);
  z-index: 1;
  ${media.sm`
      width: 20vw;
      height: 20vw;
  `}
`;
