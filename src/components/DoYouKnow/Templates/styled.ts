import { Link } from 'gatsby';
import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

export const Stats = styled.div`
  display: flex;
  flex-direction: row;
`;

export const Languages = styled.div`
  display: flex;
  align-items: center;
  svg {
    margin: 0 15px;
    &:first-of-type {
      margin-left: 0;
    }
  }
`;

export const Underline = styled(Link)`
  display: inline-flex;
  color: #00c899;
  text-decoration: none;
  box-shadow: none;
  &:hover {
    text-decoration: underline;
  }
`;
