import { Link } from 'gatsby';
import styled from 'styled-components';

import { media } from '@utils/media';

export const Wrapper = styled.div`
  display: flex;
  ${media.lg`
    margin-left: 5px;
  `}
`;

export const List = styled.ul`
  list-style: none;
  margin: 0;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
`;

export const Item = styled.li`
  margin-right: 25px;
`;

export const StyledLink = styled(Link)`
  background-image: linear-gradient(#00c899, #00c899);
  background-size: 100% 3px;
  background-position: 0% 100%;
  background-repeat: no-repeat;
  box-shadow: none;
  color: unset;
  padding-bottom: 4px;
`;

export const Title = styled.h1``;
