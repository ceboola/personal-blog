import { useContext, memo } from 'react';

import loadable from '@loadable/component';
import { Link } from 'gatsby';

const PageStats = loadable(
  () =>
    import(
      /* webpackChunkName: "PageStatsFeaturedArticle" */ '@common/PageStats'
    ),
);
import { PopularnFeatureProps } from '@components/PopularArticles/types';
import { useDimensions } from '@hooks/useDimensions';
import { PostsContext } from '@providers/PostsProvider';

import {
  FeaturedDesc,
  FeaturedImage,
  Icon,
  StyledDateBadge,
  Wrapper,
} from './styled';

const FeaturedArticle: React.FC<PopularnFeatureProps> = ({ top5 }) => {
  const { data } = useContext(PostsContext);
  const firstEl = top5[0];
  const [{ width }, ref] = useDimensions();

  const currentPageViews =
    top5.length > 0 &&
    data.GAData?.edges.find((obj) => obj.node.id === firstEl.fields.slug)?.node
      .totalCount;

  return (
    <Wrapper ref={ref}>
      <Link to={firstEl && firstEl.fields.slug}>
        <FeaturedImage
          loading="eager"
          fluid={
            firstEl
              ? firstEl.frontmatter.featuredImage.childImageSharp.fluid
              : data.Placeholder.childImageSharp.fluid
          }
        />
        {top5.length > 0 ? (
          <>
            <StyledDateBadge date={firstEl.frontmatter.date} />
            <Icon name="reading-book" width="200px" height="200px" />
            <PageStats
              slug={firstEl.fields.slug}
              pageViews={currentPageViews}
              divWidth={width}
            />
            <FeaturedDesc>{firstEl.frontmatter.title}</FeaturedDesc>
          </>
        ) : (
          <FeaturedDesc>No data</FeaturedDesc>
        )}
      </Link>
    </Wrapper>
  );
};

export default memo(FeaturedArticle);
