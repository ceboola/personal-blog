import firebase from 'gatsby-plugin-firebase';

interface firebaseLogoutProps {
  auth: () => firebase.auth.Auth;
}

interface UserInfo extends firebase.UserInfo {
  stsTokenManager: {
    accessToken: string;
  };
}

export const isBrowser = (): boolean => typeof window !== 'undefined';

export const getUser = (): UserInfo =>
  isBrowser() && window.localStorage.getItem('user')
    ? JSON.parse(window.localStorage.getItem('user') as string)
    : {};

export const getCurrentUser = (): UserInfo =>
  (isBrowser() && getUser()) as UserInfo;

export const setUser = (
  user: string | Record<string, string[]>,
): boolean | void =>
  isBrowser() && window.localStorage.setItem('user', JSON.stringify(user));

export const isLoggedIn = (): boolean => {
  const user = getUser();
  return !!user.email;
};

export const getData = async (): Promise<void> => {
  const tokenData = await new Promise<void>((resolve) => {
    firebase
      .auth()
      .currentUser?.getIdToken()
      .then((token) => {
        resolve();
        return token;
      })
      .catch((err) => {
        console.error('Error refreshing id token', err);
      });
  });
  return tokenData;
};

export const getUserToken = (): Promise<void> => {
  return new Promise<void>((resolve) => {
    firebase
      .auth()
      .currentUser?.getIdToken()
      .then((token) => {
        resolve();
        return token;
      })
      .catch((err) => {
        console.error('Error refreshing id token', err);
      });
  });
};

export const logout = (firebase: firebaseLogoutProps): Promise<void> => {
  return new Promise<void>((resolve) => {
    firebase
      .auth()
      .signOut()
      .then(function () {
        setUser({});
        resolve();
      });
  });
};
