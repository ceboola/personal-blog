import { useEffect, useMemo, useRef, useState, memo, useContext } from 'react';

import loadable from '@loadable/component';
import { useInView } from 'react-intersection-observer';

const Navigation = loadable(
  () => import(/* webpackChunkName: "Navigation" */ '@components/Navigation'),
);
const Footer = loadable(
  () => import(/* webpackChunkName: "Footer" */ '@components/Footer'),
);
const LoginStatus = loadable(
  () => import(/* webpackChunkName: "LoginStatus" */ '@components/LoginStatus'),
);
const Bio = loadable(
  () => import(/* webpackChunkName: "Bio" */ '@components/Bio'),
);
const DoYouKnow = loadable(
  () => import(/* webpackChunkName: "DoYouKnow" */ '@components/DoYouKnow'),
);
const RandomTemplate = loadable(
  () =>
    import(
      /* webpackChunkName: "RandomTemplate" */ '@components/DoYouKnow/RandomTemplate'
    ),
  {
    resolveComponent: (components) => components.RandomTemplate,
  },
);

import { PostsContext } from '@providers/PostsProvider';

import {
  Container,
  ContentWrapper,
  GlobalStyle,
  NavigationObserver,
} from './styled';

const Layout: React.FC = ({ children }) => {
  const { ref, inView } = useInView({
    threshold: [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1],
    rootMargin: '28px',
  });
  const headerRef = useRef<HTMLDivElement>(null);
  const [sticky, setSticky] = useState({ isSticky: false, offset: 0 });
  const { isSticky, offset } = sticky;
  const {
    pageContext: { clearSlug },
  } = useContext(PostsContext);

  useEffect(() => {
    if (!headerRef.current) return;
    const { height } = headerRef.current.getBoundingClientRect();
    if (!inView) {
      !isSticky && setSticky({ isSticky: true, offset: height });
    } else if (isSticky) {
      setSticky({ isSticky: false, offset: 0 });
    }
  }, [inView, isSticky]);

  return useMemo(
    () => (
      <>
        <GlobalStyle />
        <NavigationObserver ref={ref} />
        <Container style={{ marginTop: offset }}>
          <Navigation refs={headerRef} isSticky={isSticky} />
          <LoginStatus />
          <ContentWrapper isHome={clearSlug !== '/'}>{children}</ContentWrapper>
          <DoYouKnow>
            <RandomTemplate />
          </DoYouKnow>
          <Bio />
          <Footer />
        </Container>
      </>
    ),
    [children, clearSlug, isSticky, offset, ref],
  );
};

export default memo(Layout);
