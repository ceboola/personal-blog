import { graphql } from 'gatsby';

import About from '@components/About';

const AboutPage: React.FC = () => {
  return <About />;
};

export const pageQuery = graphql`
  query($slug: String!) {
    site {
      siteMetadata {
        title
      }
    }
    SummarizedStats {
      ...SummarizedStats
    }
    gitlab(name: { eq: "personal-blog" }) {
      ...CurrentProject
    }
    currentPageViews: pageViews(path: { eq: $slug }) {
      ...SitePageViews
    }
  }
`;

export default AboutPage;
