/* eslint-disable import/no-duplicates */
import { IntlProvider } from 'gatsby-plugin-react-intl';
import { ThemeProvider } from 'styled-components';
import { DOMParser } from 'xmldom';

import 'prismjs/themes/prism-tomorrow.css';

import 'firebase/auth';
import 'firebase/firestore';
// custom typeface's
require('typeface-ubuntu');
require('typeface-montserrat');

import Layout from '@components/Layout';
import PageTransition from '@components/PageTransition';
import PostsProvider from '@providers/PostsProvider';

global.DOMParser = DOMParser;

export const onRenderBody = ({ setBodyAttributes }) => {
  setBodyAttributes({
    className: 'no-js',
  });
};

export const wrapPageElement = ({ element, props }) => {
  const { locale, messages } = element.props;
  return (
    <IntlProvider locale={locale} messages={messages}>
      <ThemeProvider theme={{}}>
        <PostsProvider
          posts={
            props.data.allMarkdownRemark && props.data.allMarkdownRemark.edges
          }
          data={props.data}
          pageContext={props.pageContext}
        >
          <PageTransition props={props}>
            <Layout>{element}</Layout>
          </PageTransition>
        </PostsProvider>
      </ThemeProvider>
    </IntlProvider>
  );
};
