---
lang: en
title: VPS with K3S and Gitlab integration
date: '2020-06-14T21:22:00.284Z'
featuredImage: ../images/vps-with-k3s-and-gitlab-integration/k3s-feature.jpg
tags: ['autodevops', 'gitlab', 'kubernetes', 'k3s']
category: 'DevOps'
---

#### What is Kubernetes?

Have you ever heard of buzzword called orchestration as a frontend? If yes then you should be interested in this topic. Kubernetes is responsible for allocating and scheduling containers, providing them with abstracted functionality like internal networking and file storage, and then monitoring the health of all of these elements and stepping in to repair or adjust them as necessary, also provides a unified deployment model that allows DevOps teams to quickly and automatically test and deploy new services in quick-short summarization, but hey! topic says something about K3S:

#### K3S huh?

K3S is a lightweight certified Kubernetes distribution. I have choosed this distribution for simple reason, my vps resources fails to meet the requirements of K8S(first + last letters of kubernetes and 8 truncated characters in between). Right now I'm hosting 1 node with K3S on my VPS(1vCore, 2GB RAM) and used resources looks like this(actually running 3 projects: 2 frontend, 1 backend):

![Resources on VPS after installing K3S](../images/vps-with-k3s-and-gitlab-integration/resources.jpg)

OK! so after this introduction to K3S lets start the right part, its pretty straight forward:

## 1. Install K3S on your VPS

First check list of supported kubernetes clusters on gitlab:
https://docs.gitlab.com/ee/user/project/clusters/#supported-cluster-versions

in my example I will go with <u>v1.19.9-rc1+k3s1</u>

```bash
curl -sfL https://get.k3s.io | INSTALL_K3S_VERSION="v1.19.9-rc1+k3s1" --no-deploy traefik sh -
```

As you can see we used "--no-deploy traefik" flag, normally traefik is deployed by default when starting the server. We choosed not to start with traefik, gitlab integration comes with pre-defined packages such as ingress-nginx controller, so without this flag its leading to a conflict because those resources are responsible for the same thing and you are trying to install same part on top of it, dont worry we will do that later inside Gitlab UI :).

Note: it also have firewall protection
https://docs.gitlab.com/ee/user/project/clusters/protect/web_application_firewall/index.html

## 2. Create ServiceAccount with cluster-admin role

```bash
cat <<EOF | kubectl apply -f -
apiVersion: v1
kind: ServiceAccount
metadata:
  name: gitlab-admin
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: gitlab-admin
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: gitlab-admin
  namespace: kube-system
EOF
```

## 3. Extract secret and token for Gitlab

```bash
K3S_DEPLOY_SECRET_NAME=`kubectl get serviceaccount default -o jsonpath='{.secrets[0].name}'`
K3S_TOKEN=`kubectl get secret $KUBE_DEPLOY_SECRET_NAME -o jsonpath='{.data.token}'|base64 --decode`
K3S_CERT=`kubectl get secret $KUBE_DEPLOY_SECRET_NAME -o jsonpath='{.data.ca\.crt}'|base64 --decode`
```

then

```bash
echo $K3S_TOKEN
echo $K3S_CERT
```

And thats it, we ended part of installing k3s, lets move on to gitlab part (save this TOKEN and CERT, it will be needed in next steps).

## 4. Gitlab integration

### Start with creating new project

![New project](../images/vps-with-k3s-and-gitlab-integration/new_project.jpg)

### On the left-side menu click Operations > Kubernetes

![Operations](../images/vps-with-k3s-and-gitlab-integration/operations.jpg)

### Click "connect cluster with certificate" -> "connect existing cluster"

then you should see exactly what is on screen, fill form, just replace my placehoders with your real data from VPS. If you're not sure with port(but by default it should be 6443) your kubernetes using, check it:

```bash
cat /etc/rancher/k3s/k3s.yaml
```

and search for server index ex:

```
server: https://yourIp:yourPort
```

If you intend in the future to have more projects than one using same node then you should pick unique namespace prefix for current project, if not you can just leave it empty.

![New cluster](../images/vps-with-k3s-and-gitlab-integration/new_cluster.jpg)

After filling form click "add kubernetes cluster". Almost there! only few steps to enjoy yours automated deploy features!

Now go back again to <u><b>Operations</b> > <b>Kubernetes</b></u>.

Click your newly created cluster. In "<u><b>details</b></u>" tab you need only to fill base domain input with your current domain, rest is already filled when we were created cluster, save it.

Next jump into <u><b>Applications</b></u> tab.

What you really need in this section to have working kubernetes is just two first options installed: <b>Ingress</b> and <b>Cert-Manager</b>, rest is up to you.

![New applications](../images/vps-with-k3s-and-gitlab-integration/new_applications.jpg)

After this you should be ready to go! You can enable manually your autodevops CI/CD features directly in <u><b>Settings</b> > <b>CI/CD</b></u>

![New autodevops](../images/vps-with-k3s-and-gitlab-integration/new_autodevops.jpg)

But in my opinion it is better to have from the beginning our gitlab-ci file (its more customizable than using template directly from settings) for the project, so lets create it.

### Create .gitlab-ci.yml file in your project root

Basically it is up to you what youre gonna add here, you can include full template and leave it as it is(includes all pipelines).

```yaml
include:
  - template: Auto-DevOps.gitlab-ci.yml
```

or you can modify it to fit your expectations(disable/modify jobs),
full documentation: https://docs.gitlab.com/ee/topics/autodevops/customize.html

example:

```yaml
include:
  - template: Auto-DevOps.gitlab-ci.yml

variables:
  POSTGRES_ENABLED: 'false'
  SAST_DISABLED: 'true'
  LICENSE_MANAGEMENT_DISABLED: 'true'
  DEPENDENCY_SCANNING_DISABLED: 'true'
  CONTAINER_SCANNING_DISABLED: 'true'
  CODE_QUALITY_DISABLED: 'true'
  PERFORMANCE_DISABLED: 'true'

production:
  environment:
    url: https://blog.$KUBE_INGRESS_BASE_DOMAIN
  rules:
    - if: '$CI_COMMIT_REF_NAME == "master"'
```

### Can I deploy more than one app on my K3S!?

Yes you can, steps are the same as previous points, just make sure in new projects youre not installing any packages from <b>Applications</b> tab. Pass same TOKEN and CERT for each project, one simple different thing will be project namespace prefix while integrating new project.

### Last but not least!

As I remember from the past there was some errors from API while deploying app to k3s, there was missmatch between them.

Add <b><u>.gitlab</u></b> folder inside your root project directory, then inside .gitlab folder make new file called <b><u>auto-deploy-values.yaml</u></b>.

```yaml
deploymentApiVersion: apps/v1
```

In this file you can override your helm chart values, sometimes you need to add some annotations to ingress or change the path for url etc.

Full list here:

https://gitlab.com/gitlab-org/cluster-integration/auto-deploy-image/-/blob/master/assets/auto-deploy-app/values.yaml

And that's it, youre ready to go! 😎😎😎
