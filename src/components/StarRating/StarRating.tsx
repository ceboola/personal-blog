import { memo, useContext, useEffect, useState } from 'react';

import firebase from 'gatsby-plugin-firebase';
import { useIntl } from 'gatsby-plugin-react-intl';

import CommonButton from '@common/CommonButton';
import Loader from '@common/Loader';
import Stars from '@components/StarRating/Stars';
import { averageNumber } from '@helpers/averageNumber';
import { PostsContext } from '@providers/PostsProvider';
import { getCurrentUser } from '@utils/auth';

import { StyledLink, Wrapper, Row } from './styled';
import { PostRatingProps } from './types';

const StarRating: React.FC = () => {
  const intl = useIntl();
  const {
    userStore: {
      firebase: { dbData },
    },
    pageContext,
  } = useContext(PostsContext);
  const { uid, email } = getCurrentUser();
  const slugWithoutSlashes = pageContext.slug?.replace(/\//g, '');

  const [postRating, setPostRating] = useState<PostRatingProps>({
    exist: false,
    loader: true,
    avgRating: 0,
    numRatings: 0,
    personalRating: 0,
  });
  const [generateRating, setGenerateRating] = useState(false);
  const [hoverRating, setHoverRating] = useState(0);
  const onMouseEnter = (index: number): void => {
    setHoverRating(index);
  };
  const onMouseLeave = (): void => {
    setHoverRating(0);
  };
  const onSaveRating = (index: number): void => {
    ratePost(index);
  };

  const generatePageRating = (): void => {
    const {
      stsTokenManager: { accessToken },
    } = getCurrentUser();
    fetch('https://api.codesigh.com/pageRating', {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${accessToken}`,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        url: slugWithoutSlashes,
      }),
    })
      .then((res) => res.json())
      .then((res) => {
        setGenerateRating(true);
        console.log('here is the response: ', res);
      })
      .catch((err) => {
        console.error('here is the error: ', err);
      });
  };

  const ratePost = async (index: number): Promise<void> => {
    setPostRating((s) => ({
      ...s,
      personalRating: index,
    }));
    const postRatingRef = dbData
      ?.collection('postRating')
      .doc(slugWithoutSlashes);
    const document = await postRatingRef?.get();
    if (!document?.exists) {
      console.log('No such document!');
    } else {
      await postRatingRef
        ?.collection('ratings')
        .doc(email as string)
        .set({ rating: index });
    }
  };

  useEffect(() => {
    const fetchData = async (): Promise<void> => {
      try {
        const postRatingRef = dbData
          ?.collection('postRating')
          .doc(slugWithoutSlashes);
        const doc = await postRatingRef?.get();
        if (!doc?.exists) {
          setPostRating((s) => ({
            ...s,
            exist: false,
            loader: false,
            avgRating: 0,
            numRatings: 0,
            personalRating: 0,
          }));
          console.log('No such document!');
        } else {
          const events = await firebase
            .firestore()
            .collection('postRating')
            .doc(slugWithoutSlashes)
            .collection('ratings');
          const collection = await events.limit(1).get();
          if (collection.empty) {
            setPostRating((s) => ({
              ...s,
              exist: true,
              avgRating: 0,
              numRatings: 0,
              personalRating: 0,
            }));
          } else {
            events.get().then((querySnapshot) => {
              const usersRating: { id: string; rating: number }[] =
                querySnapshot.docs.map((doc) => {
                  return {
                    id: doc.id,
                    rating: doc.data().rating,
                    ...doc.data(),
                  };
                });

              const array: number[] = usersRating.map((val) => val.rating);
              const personalRating = (): number => {
                if (uid) {
                  const user: { id: string; rating: number } | undefined =
                    usersRating?.find((user) => user.id === email || 0);
                  return user?.rating as number;
                }
                return 0;
              };

              setPostRating((s) => ({
                ...s,
                exist: true,
                avgRating: Math.round(averageNumber(array)),
                numRatings: array.length,
                personalRating: personalRating(),
              }));
            });
          }
        }
      } catch (err) {
        console.log('err', err);
      }
    };
    fetchData();
  }, [
    dbData,
    email,
    slugWithoutSlashes,
    uid,
    postRating.personalRating,
    generateRating,
  ]);

  return postRating?.exist ? (
    <Wrapper>
      {!uid && (
        <div>
          {intl.formatMessage({ id: 'starRating.rate_article' })}{' '}
          <StyledLink to={`${intl.locale === 'pl' ? '/pl' : ''}/dashboard/`}>
            {intl.formatMessage({ id: 'loginStatus.login' })}
          </StyledLink>
        </div>
      )}
      {postRating.personalRating > 0 && (
        <div>
          {intl.formatMessage({ id: 'starRating.personal_rate' })}:{' '}
          {postRating.personalRating}
        </div>
      )}
      <Row>
        {[1, 2, 3, 4, 5].map((index) => {
          return (
            <Stars
              key={`star_${index}`}
              index={index}
              rating={postRating.avgRating}
              hoverRating={hoverRating}
              onMouseEnter={onMouseEnter}
              onMouseLeave={onMouseLeave}
              onSaveRating={onSaveRating}
            />
          );
        })}
      </Row>
      <div>
        ({intl.formatMessage({ id: 'starRating.average_rating' })}:{' '}
        {postRating.avgRating}/5 |{' '}
        {intl.formatMessage({ id: 'starRating.number_of_ratings' })}:{' '}
        {postRating.numRatings})
      </div>
    </Wrapper>
  ) : process.env.GATSBY_FIREBASE_ADMIN_UID === uid ? (
    <CommonButton
      onClick={(e) => {
        e.preventDefault();
        generatePageRating();
      }}
    >
      {intl.formatMessage({ id: 'starRating.generate_rating' })}
    </CommonButton>
  ) : !postRating.exist && !postRating.loader ? null : (
    <Loader width={48} height={48} />
  );
};

export default memo(StarRating);
