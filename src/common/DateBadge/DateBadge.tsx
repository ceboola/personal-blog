import { memo } from 'react';

import { FormattedDateParts, useIntl } from 'gatsby-plugin-react-intl';

import { Date, Day, Month, Year } from './styled';
import type { DateBadgeProps } from './types';

const DateBadge: React.FC<DateBadgeProps> = ({ date, ...otherProps }) => {
  const intl = useIntl();

  return (
    <Date {...otherProps}>
      <FormattedDateParts
        value={date}
        year="numeric"
        month="short"
        day="2-digit"
      >
        {(parts): JSX.Element => (
          <>
            {intl.locale === 'pl' && (
              <>
                <Day>{parts[0].value}</Day>
                <Month>{parts[2].value}</Month>
                <Year>{parts[4].value}</Year>
              </>
            )}
            {intl.locale === 'en' && (
              <>
                <Day>{parts[2].value}</Day>
                <Month>{parts[0].value}</Month>
                <Year>{parts[4].value}</Year>
              </>
            )}
          </>
        )}
      </FormattedDateParts>
    </Date>
  );
};

export default memo(DateBadge);
