import { useContext, memo } from 'react';

import loadable from '@loadable/component';
import { useIntl } from 'gatsby-plugin-react-intl';
import dompurify from 'isomorphic-dompurify';
import kebabCase from 'lodash/kebabCase';

const PageStats = loadable(
  () =>
    import(/* webpackChunkName: "PageStatsArticleStart" */ '@common/PageStats'),
);
import { useDimensions } from '@hooks/useDimensions';
import { PostsContext } from '@providers/PostsProvider';

import {
  Article,
  ArticleContainer,
  Description,
  HoverWrapper,
  ReadMore,
  StyledDateBadge,
  StyledIcon,
  StyledIconHover,
  StyledImg,
  StyledLink,
  Title,
  Tag,
  TagList,
  Wrapper,
  StyledTimeToRead,
} from './styled';
import type { Props } from './types';

const ArticleStart: React.FC<Props> = ({ activeTag }) => {
  const sanitizer = dompurify.sanitize;
  const { posts, data } = useContext(PostsContext);
  const GAData = data.GAData?.edges;
  const [{ width }, ref] = useDimensions();
  const intl = useIntl();
  const locale = intl.locale !== 'en' ? `/${intl.locale}` : '';
  const filteredPosts = posts.filter((edge) =>
    edge.node.frontmatter.lang.includes(intl.locale),
  );

  return (
    <ArticleContainer>
      {filteredPosts.map(({ node }) => {
        const {
          timeToRead,
          excerpt,
          frontmatter: { title, date, description, featuredImage, tags },
          fields: { slug },
        } = node;
        const currentPageViews = GAData.find(
          (filteredPageViews) => filteredPageViews.node.id === slug,
        )?.node.totalCount;

        return (
          <Wrapper ref={ref} key={title}>
            <StyledDateBadge date={date} />
            <Article>
              <header>
                <StyledLink to={slug}>
                  <HoverWrapper>
                    <PageStats
                      slug={slug}
                      pageViews={currentPageViews ?? 0}
                      divWidth={width}
                    />
                    <StyledIconHover
                      viewBox="0 0 24 24"
                      name="reading-book"
                      width="35%"
                    />
                    <StyledImg fluid={featuredImage.childImageSharp.fluid} />
                  </HoverWrapper>
                </StyledLink>
                {tags && (
                  <TagList>
                    {tags.map((tag: string, index: number) => {
                      return (
                        <Tag
                          isActive={tag === activeTag}
                          key={`${tag}_${index}`}
                        >
                          <StyledLink to={`${locale}/tags/${kebabCase(tag)}`}>
                            {tag}
                          </StyledLink>
                        </Tag>
                      );
                    })}
                  </TagList>
                )}
                <Title>
                  <StyledLink to={slug}>{title}</StyledLink>
                </Title>
              </header>
              <Description>
                <p
                  dangerouslySetInnerHTML={{
                    __html: sanitizer(description ?? excerpt),
                  }}
                />
              </Description>
              <StyledTimeToRead measurment={false} time={timeToRead} />
              <ReadMore>
                <StyledLink to={slug}>
                  {intl.formatMessage({ id: 'read_article' })}{' '}
                  <StyledIcon viewBox="0 0 24 24" name="arrow-right" />
                </StyledLink>
              </ReadMore>
            </Article>
          </Wrapper>
        );
      })}
    </ArticleContainer>
  );
};

export default memo(ArticleStart);
