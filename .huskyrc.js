module.exports = {
  hooks: {
    'pre-commit': 'yarn lint-staged',
    'pre-push': "concurrently 'yarn lint-staged' 'yarn type-check'",
  },
};
