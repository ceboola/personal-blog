import { PageProps } from 'gatsby';
import { FluidObject } from 'gatsby-image';

export interface PopularArticlesProps {
  data: {
    allMarkdownRemark: {
      edges: {
        node: {
          frontmatter: {
            title: string;
            date: string;
            category: string;
            featuredImage: {
              childImageSharp: {
                fluid: FluidObject;
              };
            };
          };
        };
      }[];
    };
  };
}

export type PagePropsData = Pick<PageProps, 'data'>;

export interface PopularnFeatureProps {
  top5: Top5Markdown[];
}
