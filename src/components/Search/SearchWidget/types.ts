export interface SearchWidgetProps {
  excerpt: string;
  slug: string;
  title: string;
  lang: string;
}
