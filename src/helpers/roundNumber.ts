export const roundNumber = (number: number, places: number): number => {
  return +(
    Math.round(((number + 'e+') as unknown as number) + places) +
    'e-' +
    places
  );
};
