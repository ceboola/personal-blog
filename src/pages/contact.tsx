import { graphql } from 'gatsby';

import Contact from '@components/Contact';

const ContactPage: React.FC = () => {
  return <Contact />;
};

export const pageQuery = graphql`
  query($slug: String!) {
    site {
      siteMetadata {
        title
      }
    }
    SummarizedStats {
      ...SummarizedStats
    }
    gitlab(name: { eq: "personal-blog" }) {
      ...CurrentProject
    }
    currentPageViews: pageViews(path: { eq: $slug }) {
      ...SitePageViews
    }
  }
`;

export default ContactPage;
