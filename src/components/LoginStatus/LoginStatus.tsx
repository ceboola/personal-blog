import { useContext, memo } from 'react';

import { navigate } from 'gatsby';
import firebase from 'gatsby-plugin-firebase';
import { useIntl } from 'gatsby-plugin-react-intl';

import { PostsContext } from '@providers/PostsProvider';
import { getCurrentUser, isLoggedIn, logout } from '@utils/auth';

import { Wrapper, StyledLink } from './styled';

const LoginStatus = (): JSX.Element => {
  const intl = useIntl();
  const lang = intl.locale === 'en' ? '' : '/pl';

  const {
    pageContext: { clearSlug },
    userStore: {
      greetingUser: { setGreeting },
    },
  } = useContext(PostsContext);
  const isDashboardPage = clearSlug?.indexOf('/dashboard/');

  let details;
  if (!isLoggedIn()) {
    details = isDashboardPage ? (
      <Wrapper>
        <p>
          App <u>v{process.env.version}</u>
        </p>
        <p>
          Dashboard /{` `}
          <StyledLink to={`${lang}/dashboard/`}>
            {intl.formatMessage({ id: 'loginStatus.login' })}
          </StyledLink>
          .
        </p>
      </Wrapper>
    ) : null;
  } else {
    const { displayName, email } = getCurrentUser();

    details = (
      <Wrapper>
        <p>
          {intl.formatMessage({ id: 'loginStatus.logged_in' })}{' '}
          <b>{displayName}</b> ({email}
          )! /{` `}
          <StyledLink
            to="/"
            onClick={(event) => {
              event.preventDefault();
              logout(firebase).then(() => {
                navigate(`${lang}/dashboard/`);
                setGreeting(true);
              });
            }}
          >
            {intl.formatMessage({ id: 'loginStatus.logout' })}
          </StyledLink>
        </p>
      </Wrapper>
    );
  }

  return <>{details}</>;
};

export default memo(LoginStatus);
