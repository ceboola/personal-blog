const SI_SYMBOL = ['', 'k', 'M', 'G', 'T', 'P', 'E'];

export const formatNumber = (
  number: number | JSX.Element,
): number | JSX.Element | string => {
  const tier = (Math.log10(number as number) / 3) | 0;
  if (tier == 0) return number;
  const suffix = SI_SYMBOL[tier];
  const scale = Math.pow(10, tier * 3);
  const scaled = (number as number) / scale;
  return scaled.toFixed(1) + suffix;
};
