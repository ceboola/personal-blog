import { PageRendererProps } from 'gatsby';

import {
  SitePageContext,
  MarkdownRemarkGroupConnection,
  MarkdownRemarkConnection,
  MarkdownRemarkFrontmatter,
} from '@typings/graphql-types';

export interface Props extends PageRendererProps {
  data: {
    site: {
      siteMetadata: {
        title: string;
      };
    };
    tags: {
      group: MarkdownRemarkGroupConnection[];
    };
    totalPosts: MarkdownRemarkConnection;
    markdownRemark: {
      frontmatter: MarkdownRemarkFrontmatter;
    };
  };
  pageContext: SitePageContext;
}
