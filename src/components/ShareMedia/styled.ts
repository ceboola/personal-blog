import styled from 'styled-components';

import CommonIcon from '@common/CommonIcon';
import { media } from '@utils/media';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: max-content;
  width: 40px;
  position: absolute;
  left: -70px;
  top: 0;
  ${media.sm`
    left: -40px;
  `}
  & a {
    box-shadow: none;
    display: flex;
  }
  ${media.xxs`
      flex-direction: row;
      position: initial;
  `}
`;

export const Container = styled.div`
  border-radius: 50%;
  width: 30px;
  height: 30px;
  background-color: #fff;
  position: relative;
  box-shadow: 0 6px 10px rgb(0 0 0 / 8%);
  display: flex;
  align-items: center;
  justify-content: center;
  margin-top: 26px;
  ${media.xxs`
      margin-left: 20px;
      &:nth-child(2) {
        margin-left: 0px;
      }
  `}
`;

export const StyledIcon = styled(CommonIcon)`
  fill: #47426f;
  ${media.xxs`
      width: 30px;
  `}
`;

export const Title = styled.div`
  font-size: 9px;
  font-weight: 900;
  background-image: linear-gradient(#00c899, #00c899);
  background-size: 100% 3px;
  background-position: 0% 100%;
  background-repeat: no-repeat;
  font-family: 'Montserrat';
  width: max-content;
  text-transform: uppercase;
  ${media.xxs`
      position: absolute;
      top: 0;
      left: 0;
  `}
`;
