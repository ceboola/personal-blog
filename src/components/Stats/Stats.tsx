import { useContext } from 'react';

import { useIntl, FormattedPlural } from 'gatsby-plugin-react-intl';
import { Helmet } from 'react-helmet';

import { PostsContext } from '@providers/PostsProvider';

import {
  Wrapper,
  Container,
  Statistics,
  Row,
  Full,
  Title,
  StatsWrapper,
  Paths,
  StyledLink,
  Info,
  StyledIcon,
} from './styled';

const Stats: React.FC = () => {
  const { data } = useContext(PostsContext);
  const { title } = data.site.siteMetadata;
  const intl = useIntl();

  const sortedByLinesOfCode = Object.entries(data.projectStatsCloc).sort(
    (a, b) => b[1].code - a[1].code,
  );
  const sortedByFiles = Object.entries(data.projectStatsCloc).sort(
    (a, b) => b[1].nFiles - a[1].nFiles,
  );

  return (
    <Wrapper>
      <Helmet
        title={`${intl.formatMessage({ id: 'statistics.title' })} | ${title}`}
      />
      <h1>{intl.formatMessage({ id: 'statistics.title' })}</h1>
      <Statistics>
        <Container>
          <Title>
            {intl.formatMessage({ id: 'statistics.pageStats.title' })}
          </Title>
          <Full>
            <span>{data.allPages.totalCount}</span>
            <Title>
              {intl.formatMessage({ id: 'statistics.pageStats.total_pages' })}
            </Title>
          </Full>
          <Row>
            <StatsWrapper>
              <span>{data.totalPageViewsSite.sum}</span>
              <Title>
                {intl.formatMessage({
                  id: 'statistics.pageStats.total_page_views',
                })}
              </Title>
              <Info>
                *({intl.formatMessage({ id: 'statistics.pageStats.hint' })})
              </Info>
            </StatsWrapper>
            <StatsWrapper>
              <span>{data.totalSessionsSite.sum}</span>
              <Title>
                {intl.formatMessage({
                  id: 'statistics.pageStats.total_unique_sessions',
                })}
              </Title>
            </StatsWrapper>
          </Row>
          <Full>
            <Title>
              <span>
                {intl.formatMessage({ id: 'statistics.pageStats.page' })}
              </span>
              <span>
                {intl.formatMessage({ id: 'statistics.pageStats.views' })}
              </span>
              <span>
                {intl.formatMessage({ id: 'statistics.pageStats.sessions' })}
              </span>
            </Title>
            {data.SummarizedStats.pages.map((value, index) => {
              return (
                <Paths key={`${value.path}_${index}`} order={index}>
                  <div>
                    <StyledLink to={value.path}>{value.path}</StyledLink>
                  </div>
                  <div>{value.totalCount}</div>
                  <div>
                    {value.totalSessions}{' '}
                    {index === 0 ? (
                      <StyledIcon name="medal-first" />
                    ) : index === 1 ? (
                      <StyledIcon name="medal-second" />
                    ) : index === 2 ? (
                      <StyledIcon name="medal-third" />
                    ) : null}
                  </div>
                </Paths>
              );
            })}
          </Full>
        </Container>
        <Container>
          <Title>
            {intl.formatMessage({ id: 'statistics.articleStats.title' })}
          </Title>
          <Full>
            <span>
              {data.SummarizedStats.articles}
              <StyledIcon name="article" />
            </span>
            <Title>
              {intl.formatMessage({
                id: 'statistics.articleStats.total_articles',
              })}
            </Title>
          </Full>
          <Row>
            <StatsWrapper>
              <span>
                {data.SummarizedStats.articlesEn}
                <StyledIcon name="united-states" />
              </span>
              <Title>
                {intl.formatMessage({
                  id: 'statistics.articleStats.total_articles_in_english',
                })}
              </Title>
            </StatsWrapper>
            <StatsWrapper>
              <span>
                {data.SummarizedStats.articlesPl}
                <StyledIcon name="poland" />
              </span>
              <Title>
                {intl.formatMessage({
                  id: 'statistics.articleStats.total_articles_in_polish',
                })}
              </Title>
            </StatsWrapper>
          </Row>
          <Full>
            <span>{data.SummarizedStats.comments}</span>
            <Title>
              {intl.formatMessage({
                id: 'statistics.articleStats.total_comments',
              })}
            </Title>
          </Full>
          <Full>
            <span>
              {data.SummarizedStats.totalTimeToRead} min
              <StyledIcon name="stopwatch" />
            </span>
            <Title>
              {intl.formatMessage({
                id: 'statistics.articleStats.total_time_to_read_articles',
              })}
            </Title>
          </Full>
          <Row>
            <StatsWrapper>
              <span>
                {data.SummarizedStats.totalTimeToReadEn} min
                <StyledIcon name="united-states" />
              </span>
              <Title>
                {intl.formatMessage({
                  id: 'statistics.articleStats.total_time_to_read_in_english',
                })}
              </Title>
            </StatsWrapper>
            <StatsWrapper>
              <span>
                {data.SummarizedStats.totalTimeToReadPl} min
                <StyledIcon name="poland" />
              </span>
              <Title>
                {intl.formatMessage({
                  id: 'statistics.articleStats.total_time_to_read_in_polish',
                })}
              </Title>
            </StatsWrapper>
          </Row>
          <Full>
            <span>
              {data.SummarizedStats.totalTags}
              <StyledIcon name="hash" />
            </span>
            <Title>
              {intl.formatMessage({ id: 'statistics.articleStats.total_tags' })}
            </Title>
          </Full>
          <Row>
            <StatsWrapper>
              <StyledLink
                to={`/tags/${data.SummarizedStats.mostPopularTagEn.fieldValue}/`}
              >
                <span>
                  # {data.SummarizedStats.mostPopularTagEn.fieldValue} (
                  {data.SummarizedStats.mostPopularTagEn.totalCount})
                  <StyledIcon name="united-states" />
                </span>
              </StyledLink>
              <Title>
                {intl.formatMessage({
                  id: 'statistics.articleStats.most_popular_tag_in_english',
                })}
              </Title>
            </StatsWrapper>
            <StatsWrapper>
              <StyledLink
                to={`/pl/tags/${data.SummarizedStats.mostPopularTagPl.fieldValue}/`}
              >
                <span>
                  # {data.SummarizedStats.mostPopularTagPl.fieldValue} (
                  {data.SummarizedStats.mostPopularTagPl.totalCount})
                  <StyledIcon name="poland" />
                </span>
              </StyledLink>
              <Title>
                {intl.formatMessage({
                  id: 'statistics.articleStats.most_popular_tag_in_polish',
                })}
              </Title>
            </StatsWrapper>
          </Row>
          <Full>
            <span>
              {data.SummarizedStats.totalUniqueTags}
              <StyledIcon name="hash" />
            </span>
            <Title>
              {intl.formatMessage({
                id: 'statistics.articleStats.total_unique_tags',
              })}
            </Title>
          </Full>
          <Title>
            {intl.formatMessage({ id: 'statistics.projectStats.title' })}
          </Title>
          <Full>
            <a
              href={data.gitlab.web_url}
              target="_blank"
              rel="noopener norefferer noreferrer"
            >
              <span>{data.gitlab.name}</span>
            </a>
            <Title>
              {intl.formatMessage({
                id: 'statistics.projectStats.project_name',
              })}
            </Title>
          </Full>
          <Title>
            {intl.formatMessage({ id: 'statistics.gitlabApiStats.title' })}
          </Title>
          <Row>
            {Object.entries(data.gitlab.usedLanguages).map(([key, val], i) => (
              <>
                {val && (
                  <StatsWrapper key={i}>
                    <span>
                      {val}%
                      <StyledIcon name={key.toLowerCase()} />
                    </span>
                    <Title>{key}</Title>
                  </StatsWrapper>
                )}
              </>
            ))}
          </Row>
          <Full>
            <span>
              {data.gitlab.statistics.commit_count}
              <StyledIcon name="gitlab" />
            </span>
            <Title>
              {intl.formatMessage({
                id: 'statistics.gitlabApiStats.total_commits',
              })}
            </Title>
          </Full>
          <Row>
            <StatsWrapper>
              <span>{data.gitlab.created_at}</span>
              <Title>
                {intl.formatMessage({
                  id: 'statistics.gitlabApiStats.created_at',
                })}
              </Title>
            </StatsWrapper>
            <StatsWrapper>
              <span>{data.SummarizedStats.projectSize} MB</span>
              <Title>
                {intl.formatMessage({
                  id: 'statistics.gitlabApiStats.total_project_size',
                })}
              </Title>
            </StatsWrapper>
          </Row>
          <Title>
            {intl.formatMessage({ id: 'statistics.clocStats.title' })}
          </Title>
          <Full>
            <span>{data.totalProjectStatsCloc.SUM.code}</span>
            <Title>
              {intl.formatMessage({
                id: 'statistics.clocStats.total_lines_of_code',
              })}
            </Title>
          </Full>
          <Row>
            {sortedByLinesOfCode.map((val, i) => (
              <>
                {val && (
                  <StatsWrapper key={i}>
                    <span>
                      {val[1].code}{' '}
                      <FormattedPlural
                        value={val[1].code}
                        one={intl.formatMessage({
                          id: 'statistics.clocStats.lines.singular.one',
                        })}
                        other={intl.formatMessage({
                          id: 'statistics.clocStats.lines.plural.other',
                        })}
                        {...(intl.locale === 'pl' && {
                          few: intl.formatMessage({
                            id: 'statistics.clocStats.lines.plural.few',
                          }),
                          many: intl.formatMessage({
                            id: 'statistics.clocStats.lines.plural.many',
                          }),
                        })}
                      />
                      <StyledIcon
                        name={
                          val[0] === 'Bourne_Shell'
                            ? 'shell'
                            : val[0].toLowerCase()
                        }
                      />
                    </span>
                    <Title>
                      {val[0] === 'Bourne_Shell' ? 'Shell' : val[0]}
                    </Title>
                  </StatsWrapper>
                )}
              </>
            ))}
          </Row>
          <Full>
            <span>{data.totalProjectStatsCloc.SUM.nFiles}</span>
            <Title>
              {intl.formatMessage({ id: 'statistics.clocStats.total_files' })}
            </Title>
          </Full>
          <Row>
            {sortedByFiles.map((val, i) => (
              <>
                {val && (
                  <StatsWrapper key={i}>
                    <span>
                      {val[1].nFiles}{' '}
                      <FormattedPlural
                        value={val[1].nFiles}
                        one={intl.formatMessage({
                          id: 'statistics.clocStats.files.singular.one',
                        })}
                        other={intl.formatMessage({
                          id: 'statistics.clocStats.files.plural.other',
                        })}
                        {...(intl.locale === 'pl' && {
                          few: intl.formatMessage({
                            id: 'statistics.clocStats.files.plural.few',
                          }),
                          many: intl.formatMessage({
                            id: 'statistics.clocStats.files.plural.many',
                          }),
                        })}
                      />
                      <StyledIcon
                        name={
                          val[0] === 'Bourne_Shell'
                            ? 'shell'
                            : val[0].toLowerCase()
                        }
                      />
                    </span>
                    <Title>
                      {val[0] === 'Bourne_Shell' ? 'Shell' : val[0]}
                    </Title>
                  </StatsWrapper>
                )}
              </>
            ))}
          </Row>
        </Container>
      </Statistics>
    </Wrapper>
  );
};

export default Stats;
