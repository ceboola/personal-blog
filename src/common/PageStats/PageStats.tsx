import { memo } from 'react';

import { useIntl } from 'gatsby-plugin-react-intl';

import { CommentsCount } from '@components/Comments';
import { formatNumber } from '@helpers/formatNumber';

import {
  StatsWrapper,
  CommentsWrapper,
  Description,
  Count,
  StyledIcon,
  ViewsWrapper,
  StatsWrapperSmall,
  Comments,
  Views,
  Container,
  StyledTextFit,
} from './styled';

interface PageStatsProps {
  slug: string | null | undefined;
  pageViews: number | false | undefined;
  divWidth: number | false | undefined;
  show?: boolean;
}

const PageStats: React.FC<PageStatsProps> = ({
  slug,
  pageViews,
  divWidth,
  show = true,
  ...otherProps
}) => {
  const iconSize = divWidth && divWidth > 0 && divWidth > 400 ? 48 : 36;
  const intl = useIntl();

  return (
    <Container fade={show} show={divWidth && divWidth > 1} {...otherProps}>
      {divWidth && divWidth < 150 && show && (
        <StatsWrapperSmall>
          <Comments>
            <CommentsCount websiteId={877} id={slug} mode="number" />
          </Comments>
          <Views>{pageViews}</Views>
        </StatsWrapperSmall>
      )}
      {divWidth && divWidth > 150 && show && (
        <StatsWrapper>
          <CommentsWrapper>
            <Description>{intl.formatMessage({ id: 'comments' })}</Description>
            <Count width={divWidth}>
              <StyledTextFit
                mode="single"
                throttle={300}
                forceSingleModeWidth={false}
                max={divWidth > 400 ? 17 : 13}
              >
                {formatNumber(
                  <CommentsCount websiteId={877} id={slug} mode="number" />,
                )}
              </StyledTextFit>
            </Count>
            <StyledIcon name="comments" width={iconSize} height={iconSize} />
          </CommentsWrapper>
          <ViewsWrapper>
            <Description>
              {intl.formatMessage({ id: 'page_views' })}
            </Description>
            <Count width={divWidth}>
              <StyledTextFit
                mode="single"
                throttle={300}
                forceSingleModeWidth={true}
                max={divWidth > 400 ? 17 : 13}
              >
                {formatNumber(pageViews as number)}
              </StyledTextFit>
            </Count>
            <StyledIcon name="views" width={iconSize} height={iconSize} />
          </ViewsWrapper>
        </StatsWrapper>
      )}
    </Container>
  );
};

export default memo(PageStats);
