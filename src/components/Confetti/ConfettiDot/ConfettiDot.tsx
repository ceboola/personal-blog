import React from 'react';

import { config, to, useSpring } from 'react-spring';

import { flipCoin, randomFromArray, randomInRange } from '@helpers/randoms';

import { AnimatedConfettiDot } from './styled';
import { ConfettiProps, ConfettiDotProps } from './types';

const alignWithAnchor = (
  anchorRef: React.RefObject<HTMLElement>,
): Record<string, number> => {
  if (anchorRef.current == null) {
    return {
      initialX: 0,
      initialY: 0,
    };
  }

  const { height, width } = anchorRef.current.getBoundingClientRect();

  return {
    initialX: width / 2,
    initialY: height / 2,
  };
};

const Circle = ({ color, size }: ConfettiProps): JSX.Element => (
  <circle
    cx={`${size / 2}`}
    cy={`${size / 2}`}
    r={`${(size / 2) * 0.6}`}
    fill={color}
  />
);

const Triangle = ({ color, size }: ConfettiProps): JSX.Element => {
  const flipped = flipCoin();
  return (
    <polygon
      points={`${size / 2},0 ${size},${randomInRange(
        flipped ? size / 2 : 0,
        size,
      )} 0,${randomInRange(flipped ? 0 : size / 2, size)}`}
      fill={color}
    />
  );
};

const Square = ({ color, size }: ConfettiProps): JSX.Element => {
  const flipped = flipCoin();
  return (
    <rect
      height={`${randomInRange(0, flipped ? size : size / 2)}`}
      width={`${randomInRange(0, flipped ? size / 2 : size)}`}
      fill={color}
    />
  );
};

const getRandomShape = (color: string, size: number): JSX.Element => {
  const Shape = randomFromArray(([
    Circle,
    Square,
    Triangle,
  ] as unknown) as number[]);
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  return <Shape color={color} size={size} />;
};

const ConfettiDot = ({
  anchorRef,
  color,
  initialHorizontal,
  initialUpwards,
  rotate,
  size,
}: ConfettiDotProps): JSX.Element => {
  const { initialX, initialY } = alignWithAnchor(anchorRef);
  const { horizontal, opacity, upwards } = useSpring({
    config: config.default,
    from: {
      horizontal: initialHorizontal,
      opacity: 90,
      upwards: initialUpwards,
    },
    to: {
      horizontal: 0,
      opacity: 0,
      upwards: 0,
    },
  });

  let totalUpwards = 0;
  let totalHorizontal = 0;
  const startTime = new Date().getTime() / 1000;
  let lastTime = startTime;
  const gravityPerSecond = 30;

  return (
    <AnimatedConfettiDot
      style={{
        opacity,
        transform: to([upwards, horizontal], (v, h) => {
          const currentTime = new Date().getTime() / 1000;
          const duration = currentTime - lastTime;
          const totalDuration = currentTime - startTime;
          const verticalTraveled = v * duration;
          const horizontalTraveled = h * duration;
          totalUpwards += verticalTraveled;
          totalHorizontal += horizontalTraveled;
          lastTime = currentTime;

          const totalGravity = gravityPerSecond * totalDuration;
          const finalX = initialX + totalHorizontal;
          const finalY = initialY - totalUpwards + totalGravity;

          return `translate3d(${finalX}px, ${finalY}px, 0) rotate(${rotate}deg)`;
        }),
      }}
    >
      {getRandomShape(color, size)}
    </AnimatedConfettiDot>
  );
};

export default ConfettiDot;
