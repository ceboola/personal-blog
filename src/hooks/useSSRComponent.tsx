import dompurify from 'isomorphic-dompurify';

export const useSSRComponent = (Component: React.FC<unknown>): JSX.Element => {
  const sanitizer = dompurify.sanitize;
  if (process.env.NODE_ENV === 'development') return <Component />;
  if (typeof window === 'undefined')
    return (
      <div
        suppressHydrationWarning
        dangerouslySetInnerHTML={{ __html: sanitizer('') }}
      />
    );

  return (
    <div>
      <Component />
    </div>
  );
};

export default useSSRComponent;
