import { useContext, useRef, memo } from 'react';

import { useIntl } from 'gatsby-plugin-react-intl';

import { getCategories } from '@helpers/getCategories';
import useScrollPosition from '@hooks/useScrollPosition';
import { PostsContext } from '@providers/PostsProvider';

import { Wrapper, Category, StyledButton, Shadow, Container } from './styled';

const Categories: React.FC = () => {
  const { data, setPostsCategory, postCategory } = useContext(PostsContext);
  const intl = useIntl();
  const defaultCategory = intl.formatMessage({ id: 'all_posts' });
  const categories = getCategories(
    data.allMarkdownRemark.edges,
    defaultCategory,
  );
  const handleCategoryClick = (category: string): void => {
    setPostsCategory(category);
  };
  const ref = useRef<HTMLUListElement | null>(null);
  const {
    state: { leftEdge, rightEdge },
    isScrollable,
  } = useScrollPosition(ref);

  return (
    <Container>
      <Shadow direction="left" show={leftEdge} />
      <Wrapper ref={ref}>
        {categories.map((category, index) => {
          return (
            <Category key={`${category}_${index}`}>
              <StyledButton
                active={postCategory === category}
                key={index}
                onClick={() => handleCategoryClick(category)}
                title={category}
                aria-label="filter category"
              >
                {category}
              </StyledButton>
            </Category>
          );
        })}
      </Wrapper>
      <Shadow direction="right" show={isScrollable && rightEdge} />
    </Container>
  );
};

export default memo(Categories);
