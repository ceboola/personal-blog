---
lang: pl
title: Renovate z Gitlab CI/CD
date: '2021-05-03T01:00:00.284Z'
featuredImage: ../../images/renovate-with-gitlab-ci/renovate.jpeg
tags: ['gitlab', 'renovate', 'devops']
category: 'DevOps'
---

Witajcie, dzisiaj na szybko chciałbym się z Wami podzielić czymś dzięki czemu życie stało się łatwiejsze. Pewnie wielu z Was dotkneła przykra sprawa podbijania paczek? Prawdopodobnie większość, więc każdy wie jaki to jest wrzód na tyłku... Przeszukując ostatnio internet trafiłem na genialne narzędzie które podbijanie paczek robi za nas automatycznie! Nazywa się <b>Renovate</b>

### Co to jest Renovate?

<b>Renovate</b> to "bot" który nasłuchuje zmiany wewnątrz poszczegółnych paczek (skanuje repozytoria git w poszukiwaniu zmian) o określonych interwałach czasowych. Jest w pełni konfigurowalny do naszych potrzeb więc nie musisz się bać o jego limitacje. Dla mnie osobiście to coś czego mi osobiście brakowało, nie muszę teraz dbać o ręczne podbijanie paczek ponieważ runner robi to za mnie, teraz mogę skupić się na swoim kodzie. Oczywiście czym mamy aktualniejsze paczki tym łatwiej rozwiązać nam potencjalne problemy. W drugą stronę już nie byłoby tak kolorowo (łatanie 2-3 majorów do góry czasami jest awykonalne).

#### Gitlab z Renovate

Konfiguracja tych dwóch rzeczy to bułka z masłem! Potrzebujesz do tego mniej niż 5 kroków.

### Stwórz nowy projekt Gitlab

Po pierwsze stwórz nowy projekt dla Renovate, to miejsce gdzie wykonasz większość konfiguracji.

![Stwórz projekt](../../images/renovate-with-gitlab-ci/renovategitlab.png)

### Stwórz plik .gitlab-ci.yml

```yml
include:
  - project: 'renovate-bot/renovate-runner'
    file: '/templates/renovate-dind.gitlab-ci.yml'
```

### Stwórz schedule pipeline

![Stworz schedule pipeline](../../images/renovate-with-gitlab-ci/renovategitlab2.png)

Teraz możesz tam zmienić jak często ten pipeline odpali się w poszukiwaniu nowych dependency, moja rutyna wygląda tak (co każdą godzinę sprawdzam paczki):

Kliknij new schedule
![Pipeline rutyna](../../images/renovate-with-gitlab-ci/renovategitlab3.png)

### Stwórz gitlab token

Teraz stwórzmy token do gitlaba który będzie odpowiedzialny za uprawnienia do naszego Renovate, będzie potrzebował dostępu do <b>api, read_user, write_repository</b>

![Token dostepu](../../images/renovate-with-gitlab-ci/renovategitlab4.png)

![Token pozwolenia](../../images/renovate-with-gitlab-ci/renovategitlab5.png)

### Stwórz github token

Ten krok nie jest konieczny ale lepiej go wykonać bo znosi nam limity z naszego runnera, dzięki niemu bez przeszkód Renovate będzie mógł skanować wewnątrz repozytoriów zmiany paczek. Ten token potrzebuje minimalnych uprawnień. W zasadzie możemy go wygenerować bez żadnych zaznaczonych checkboxów. Przejdź do <b>Settings > Developer settings > Personal access tokens > generate new token</b>

![Github token](../../images/renovate-with-gitlab-ci/renovategitlab6.png)

Skopiuj te tokeny (Gitlab & Github) i przejdź do <b>Settings > CI/CD > Variables</b>, teraz musisz wypełnić te trzy zmienne <b>GITHUB_COM_TOKEN RENOVATE_TOKEN RENOVATE_EXTRA_FLAGS</b> swoimi prawdziwymi danymi.

![CI/CD variables](../../images/renovate-with-gitlab-ci/renovategitlab7.png)

I to właściwie tyle jeżeli chodzi o część naszego runnera. Teraz powinien być gotowy do pracy i przeszukiwania paczek w celu podbijania nowych wersji, teraz dzieli Cię już tylko jeden mały krok żeby w pełni cieszyć się jego funkcjonalnością, teraz wróćmy do projektu w którym chcemy żeby bot nam podbijał paczki.

### Wróć do projektu docelowego

Teraz w tym projekcie stwórz folder nazwany <b>.gitlab</b>, a wewnątrz tego folderu stwórz nowy plik <b>renovate.json</b> -> to jest nasz config który przekazuje wszystkie polecenia do naszego runnera.

```json
{
  "extends": ["config:base"],
  "prHourlyLimit": 0, // limit of pull request by hour, 0 unlimited
  "prConcurrentLimit": 0, // limit of opened merge request at the moment, 0 unlimited
  "ignoreDeps": ["graphql"], // dependencies with we dont wanna auto updated by runner
  "packageRules": [{ "languages": ["js"], "addLabels": ["javascript"] }] // adds labels
}
```

To jest najprostrzy przykład który wystarcza do w pełni funkcjonalnego Renovate, jeżeli chcesz go bardziej skonfigurować zapraszam do oficjalnej [dokumentacji](https://docs.renovatebot.com/configuration-options/)

Po tych wszystkich krokach Twój Renovate jest gotowy do pracy!
