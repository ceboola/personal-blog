interface AllMarkdownRemarkEdge {
  node: {
    timeToRead: number;
    excerpt: string;
    frontmatter: {
      title: string;
      date: string;
      category: string;
      description: string;
      lang: string;
      featuredImage: {
        childImageSharp: {
          fluid: import('gatsby-image').FluidObject;
        };
      };
      tags: string[];
    };
    fields: {
      slug: string;
    };
  };
}

interface ProjectStats {
  blank: number;
  code: number;
  comment: number;
  nFiles: number;
}

interface Top5Markdown {
  excerpt: string;
  frontmatter: {
    date: string;
    title: string;
    featuredImage: {
      childImageSharp: {
        fluid: FluidObject;
      };
    };
  };
  fields: {
    slug: string;
  };
  pageViews: number;
  timeToRead: number;
}

interface QueryProps {
  markdownRemark: {
    frontmatter: {
      title: string;
      description: string;
      rawDate: string;
      date: string;
      tags: string[];
      lang: string;
      featuredImage: {
        childImageSharp: {
          fluid: import('gatsby-image').FluidObject;
        };
      };
    };
    fields: {
      slug: string;
    };
    excerpt: string;
    html: string;
    timeToRead: number;
  };
  site: {
    buildTime: string;
    siteMetadata: {
      title: string;
    };
  };
  tags: {
    group: import('./graphql-types').MarkdownRemarkGroupConnection[];
  };
  totalPosts: {
    totalCount: number;
  };
  allMarkdownRemark: {
    totalCount: number;
    group: Array<{
      fieldValue: string;
      totalCount: number;
    }>;
    edges: import('./graphql-types').AllMarkdownRemarkEdge[];
  };
  pageContext: {
    tag?: string; // only set into `templates/tags-pages.tsx`
    language?: string;
  };
  GAData: {
    edges: Array<{
      node: { id: string; totalCount: number };
    }>;
  };
  currentPageViews: {
    totalCount: number;
    totalSessions: number;
  };
  Placeholder: {
    childImageSharp: {
      fluid: import('gatsby-image').FluidObject;
    };
  };
  SummarizedStats: {
    articles: number;
    articlesEn: number;
    articlesPl: number;
    commits: number;
    projects: number;
    comments: number;
    totalTimeToRead: number;
    totalTimeToReadEn: number;
    totalTimeToReadPl: number;
    mostPopularTagEn: {
      fieldValue: string;
      totalCount: number;
    };
    mostPopularTagPl: {
      fieldValue: string;
      totalCount: number;
    };
    totalTags: number;
    totalUniqueTags: number;
    projectSize: number;
    programmingLanguages: {
      CSS: number;
      Dockerfile: number;
      HTML: number;
      JavaScript: number;
      Pug: number;
      Shell: number;
      TypeScript: number;
    };
    pages: Array<{
      path: string;
      totalCount: number;
      totalSessions: number;
    }>;
  };
  gitlab: {
    web_url: string;
    name: string;
    statistics: {
      commit_count: number;
    };
    created_at: string;
    usedLanguages: Array<number>;
  };
  allPages: {
    totalCount: number;
  };
  totalPageViewsSite: {
    sum: number;
  };
  totalSessionsSite: {
    sum: number;
  };
  projectStatsCloc: {
    Bourne_Shell: ProjectStats;
    Dockerfile: ProjectStats;
    JSON: ProjectStats;
    JavaScript: ProjectStats;
    Markdown: ProjectStats;
    SVG: ProjectStats;
    TypeScript: ProjectStats;
    YAML: ProjectStats;
  };
  totalProjectStatsCloc: {
    SUM: ProjectStats;
  };
}
