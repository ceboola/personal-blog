import { Link } from 'gatsby';
import styled, { css } from 'styled-components';

import CommonIcon from '@common/CommonIcon';
import { media } from '@utils/media';

export const StyledLink = styled(Link)`
  span {
    color: #00c899;
  }
`;

export const StyledIcon = styled(CommonIcon)`
  position: absolute;
  right: 0;
  top: 5px;
  height: 26px;
  width: 26px;
`;

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  a {
    box-shadow: none;
    color: #00c899;
    &:hover {
      text-decoration: underline;
    }
  }
`;

export const Statistics = styled.div`
  display: flex;
  flex-direction: row;
  ${media.sm`
      flex-direction: column;
  `}
`;

export const Title = styled.h2`
  display: flex;
  font-size: 12px;
  margin: 10px 0;
  width: 100%;
  text-align: center;
  span {
    display: flex;
    flex: 0 1 33%;
    justify-content: center;
    background-image: linear-gradient(#00c899, #00c899);
    background-size: 30% 3px;
    background-position: 50% 100%;
    background-repeat: no-repeat;
    padding-bottom: 6px;
    &:first-of-type {
      justify-content: flex-start;
      margin-left: 10px;
      background-position: -10% 100%;
    }
  }
`;

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  flex: 0 1 50%;
  justify-content: flex-start;
  margin-right: 20px;
  ${media.sm`
      flex: 0 1 100%;
      margin: 15px 0;
  `}
  &:nth-child(2) {
    ${StyledIcon} {
      position: relative;
      height: 18px;
      width: 18px;
      margin-left: 10px;
      top: 0;
      fill: #00c899;
    }
  }
`;

export const Full = styled.div`
  width: 100%;
  min-height: 100px;
  height: max-content;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin: 0 0 10px 0;
  padding: 0 10px;
  background-color: #17173f;
  color: #fff;
  border-radius: 6px;
  ${Title} {
    justify-content: center;
    line-height: 1.3;
  }
  > span {
    color: #fff;
    font-weight: 500;
    font-size: 22px;
    background-image: linear-gradient(#00c899, #00c899);
    background-size: 100% 3px;
    background-position: 0% 100%;
    background-repeat: no-repeat;
    line-height: 22px;
    padding: 0 8px 7px 8px;
  }
`;

export const Info = styled.span`
  position: absolute;
  margin: 0;
  font-size: 9px;
  height: auto;
  bottom: -5px;
`;

export const Row = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  div {
    display: inherit;
    height: 100px;
    justify-content: center;
    align-items: center;
    width: 100%;
    background-color: #17173f;
    color: #fff;
    &:nth-child(odd) {
      margin-right: 10px;
    }
  }
  span {
    display: flex;
    color: #fff;
    font-weight: 500;
    font-size: 17px;
    background-image: linear-gradient(#00c899, #00c899);
    background-size: 100% 3px;
    background-position: 0% 100%;
    background-repeat: no-repeat;
    line-height: 17px;
    padding: 0 6px 8px 6px;
  }
  ${Info} {
    margin: 0;
    background-image: none;
    font-size: 9px;
    line-height: 10px;
    ${media.md`
        font-size: 8px;
    `}
  }
`;

export const StatsWrapper = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  border-radius: 6px;
  margin: 0 0 10px 0;
  padding: 0 10px;
  flex: 0 1 calc(50% - 5px);
  ${Title} {
    justify-content: center;
    line-height: 1.3;
  }
`;

export const Paths = styled.div<{ order: number }>`
  display: flex;
  flex-direction: row;
  background-color: #17173f;
  width: 100%;
  margin-bottom: 10px;
  align-items: center;
  div {
    position: relative;
    font-size: 13px;
    display: flex;
    flex: 0 1 33%;
    justify-content: center;
    &:first-of-type {
      justify-content: flex-start;
      margin-left: 10px;
      max-width: 200px;
      ${media.md`
          max-width: 32%;
          font-size: 12px;
      `}
    }
    &:nth-child(n + 2) {
      font-size: 15px;
      font-weight: 500;
      ${(props) =>
        props.order === 0 &&
        css`
          color: #e0ae0a;
          font-weight: 900;
        `};
      ${(props) =>
        props.order === 1 &&
        css`
          color: #a0a0a0;
          font-weight: 900;
        `};
      ${(props) =>
        props.order === 2 &&
        css`
          color: #a85d2b;
          font-weight: 900;
        `};
    }
  }
  a {
    width: 100%;
  }
`;
