import { useIntl } from 'gatsby-plugin-react-intl';

import CommonIcon from '@common/CommonIcon';
import SEO from '@components/SEO';

import { Wrapper, Title, List, Underline, Description } from './styled';

const About: React.FC = () => {
  const intl = useIntl();

  return (
    <Wrapper>
      <SEO title={`${intl.formatMessage({ id: 'about_me' })}`} />
      <Title>
        <h1>{intl.formatMessage({ id: 'about_me' })}</h1>
        <CommonIcon name="hello" width={60} height={60} />
      </Title>
      <Description>
        {intl.formatMessage(
          {
            id: 'aboutMe.firstDescription',
          },
          {
            line: (chunks: string) => <Underline>{chunks}</Underline>,
          },
        )}
        <List>
          <li>
            <div>
              {intl.formatMessage({ id: 'aboutMe.list.li-1' })}
              <CommonIcon name="love" width={28} height={28} />
            </div>
          </li>
          <li>{intl.formatMessage({ id: 'aboutMe.list.li-2' })}</li>
          <li>{intl.formatMessage({ id: 'aboutMe.list.li-3' })}</li>
          <li>{intl.formatMessage({ id: 'aboutMe.list.li-4' })}</li>
          <li>{intl.formatMessage({ id: 'aboutMe.list.li-5' })}</li>
        </List>
        {intl.formatMessage(
          {
            id: 'aboutMe.secondDescription',
          },
          {
            line: (chunks: string) => <Underline>{chunks}</Underline>,
          },
        )}
      </Description>
    </Wrapper>
  );
};

export default About;
