import styled from 'styled-components';

import { media } from '@utils/media';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

export const Unsub = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  svg {
    margin-bottom: 26px;
  }
`;
