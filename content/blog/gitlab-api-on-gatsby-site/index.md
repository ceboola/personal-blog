---
lang: en
title: Gitlab API on Gatsby site
date: '2020-06-11T13:46:41.169Z'
tags: ['gitlab', 'api', 'gatsby']
featuredImage: ../images/gitlab-api-on-gatsby-site/gitlabgatsby.jpeg
category: 'Front-end'
---

Hello there! In this article I will show you how to integrate Gitlab API with Gatsby site, pretty straight forward so lets go.

## Gitlab API + Gatsby

Theres many ways to receive data from Gitlab API from simply GET/POST request to additional libraries but in our case lets say we wanna use benefits of static site generation (SSG) which Gatsby provide for us. In this example I will use library called [Gitbreaker](https://github.com/jdalrymple/gitbeaker), lets start by installing dependency

```bash
yarn add @gitbeaker/node
```

### gatsby-node

Gatsby-node it is a file (source) with includes inside Gatsby APIs. Giving you possibility to run the code once in the build phase. The full list of APIs with you can consume inside that file is in official gatsby [documentation](https://www.gatsbyjs.com/docs/reference/config-files/gatsby-node/). Today we will focus on <b>sourceNode</b>, <b>createSchemaCustomization</b> and <b>createResolver</b> API, lets start with first word and this is how it is described:

#### sourceNodes

> Extension point to tell plugins to source nodes. This API is called during the Gatsby bootstrap sequence. Source plugins use this hook to create nodes. This API is called exactly once per plugin (and once for your site’s gatsby-config.js file). If you define this hook in gatsby-node.js it will be called exactly once after all of your source plugins have finished creating nodes.

OK! After some quick presentation on what we will operate lets do some code, firstly import our gitbreaker into gatsby-node.js file

```javascript
const { Gitlab } = require('@gitbeaker/node');
```

"Why its using ES5 import?" you may ask, there is some limitation on Node side, if you want to use ES6 instead of ES5 check [this](https://github.com/gatsbyjs/gatsby/issues/7810)

After importing dependency lets create a call to Gitlab API, lets say we wanna get information about our personal projects.

```javascript
exports.sourceNodes = async ({
  actions,
  createContentDigest,
  createNodeId,
}) => {
  const { createNode } = actions;
  //gitlab
  const GITLAB_NODE_TYPE = `Gitlab`;
  const api = new Gitlab({
    token: process.env.GITLAB_TOKEN,
  });
  const user = await api.Users.current();
  const projects = await api.Users.projects(user.id, { statistics: true });
};
```

Basically what we are doing here is:

- creating node type for graphql schema
- connecting to api with our credentials (you can get your gitlab token from Preferences > Access Tokens on gitlab.com)
- using our current user id from Users API to connect directly and get data about project, we are passing here <b>user.id</b> and statistics paremeter set to <b>true</b> to get extra stats from projects like commits, size etc.

If youre not familiar with process.env variables you should look here [dotenv](https://www.npmjs.com/package/dotenv) in quick short these variables are sensitive data and thats why we are masking them.

### Save fetched data to GraphQL

After connecting to Gitlab API we should be able now to get the data, lets say we want to get our projects:

```javascript
exports.sourceNodes = async ({
  actions,
  createContentDigest,
  createNodeId,
}) => {
  const { createNode } = actions;
  //gitlab
  const GITLAB_NODE_TYPE = `Gitlab`;
  const api = new Gitlab({
    token: process.env.GITLAB_TOKEN,
  });
  const user = await api.Users.current();
  const projects = await api.Users.projects(user.id, { statistics: true });

  await Promise.all(
    projects.map(async (project) => {
      createNode({
        ...project,
        id: createNodeId(`${GITLAB_NODE_TYPE}-${project.id}`),
        parent: null,
        children: [],
        internal: {
          type: GITLAB_NODE_TYPE,
          content: JSON.stringify(project),
          contentDigest: createContentDigest(project),
        },
      });

};
```

For each project we are mapping and getting data, saving it to type <b>Gitlab</b>. Now after starting gatsby application we can see now our fetched data from Gitlab API on GraphiQL!

![GraphQL data](../images/gitlab-api-on-gatsby-site/graphql.png)

## Extra

But hey, lets add some custom field because right now we are missing used languages field with is on another Gitlab API endpoint, lets do it!

```javascript
exports.sourceNodes = async ({
  actions,
  createContentDigest,
  createNodeId,
}) => {
  const { createNode } = actions;
  //gitlab
  const GITLAB_NODE_TYPE = `Gitlab`;
  const api = new Gitlab({
    token: process.env.GITLAB_TOKEN,
  });
  const user = await api.Users.current();
  const projects = await api.Users.projects(user.id, { statistics: true });

  await Promise.all(
    projects.map(async (project) => {
      createNode({
        ...project,
        usedLanguages: await api.Projects.languages(project.id),
        id: createNodeId(`${GITLAB_NODE_TYPE}-${project.id}`),
        parent: null,
        children: [],
        internal: {
          type: GITLAB_NODE_TYPE,
          content: JSON.stringify(project),
          contentDigest: createContentDigest(project),
        },
      });

};
```

Whats the difference? We added new field called <b>usedLanguages</b>, now we are fetching for each <b>project.id</b> a set of programming languages used in specific project!

![GraphQL languages](../images/gitlab-api-on-gatsby-site/graphql2.png)

Cool isnt it? But we're demanding fellas and wanna get summarized stats of all our projects! Lets start from drawing the problem on sheet and try to understand mathematics.

![Mathematics problem](../images/gitlab-api-on-gatsby-site/problem.jpg)

The problem is we are getting various numbers for each language for example JS: 20%, 50%, 50%, 20%, 0%, 10% as you can see its much more than 100% just to summarize them. First idea:

- sum all and divide by amount of them (150/6=25%)
  That was bad, even on first sight it looks legit, further will be lacking amount of percent to 100%

####Second idea

![Mathematics problem](../images/gitlab-api-on-gatsby-site/problem2.jpg)

First divide each percent by 10, summarize them and each divided value divide again by summarized number, so in our example:

- 2 + 5 + 5 + 2 + 1 = 15
- each value divide by 15
- 0.13 + 0.33 + 0.33 + 0.13 + 0.06
- now each value multiply by 100
- 13 + 33 + 33 + 13 + 6 ~ 100%

Now looks promising! Lets implement this right now :)

First we need create types for our schema, lets use new API called <b>createSchemaCustomization</b>

> Customize Gatsby’s GraphQL schema by creating type definitions, field extensions or adding third-party schemas.

```javascript
exports.createSchemaCustomization = ({ actions }) => {
  const { createTypes } = actions;
  const typeDefs = `
  type SummarizedStats @nodeInterface {
    programmingLanguages: GitlabUsedLanguages
  }
  `;
  createTypes(typeDefs);
};
```

So here we defined type <b>SummarizedStats</b> for schema and inside it declared field <b>programmingLanguages</b> where we will be saving summarized data, so now lets fill <b>programmingLanguages</b> with some data!

### From paper to code

First lets make some helper to round our numbers:

```javascript
const round = (number, places) => {
  return +(Math.round(number + 'e+' + places) + 'e-' + places);
};
```

Now we need to fill our schema with real data. How can we do that? For the rescue we will use another gatsby API called <b>createResolver</b>

> Add custom field resolvers to the GraphQL schema.
> Allows adding new fields to types by providing field configs, or adding resolver functions to existing fields.

#### HoF 🔥🔥

```javascript
exports.createResolvers = async ({ createResolvers }) => {
  createResolvers({
    Query: {
      SummarizedStats: {
        type: 'SummarizedStats',
        resolve(source, args, context) {
          const round = (number, places) => {
            return +(Math.round(number + 'e+' + places) + 'e-' + places);
          };
          const getAllGitlab = context.nodeModel.getAllNodes({
            type: 'Gitlab',
          });
          const mapField = (resource, field) =>
            resource.map((val) => val[field]);

          const allLang = mapField(getAllGitlab, 'usedLanguages');
          const programmingLanguages = allLang.reduce(
            (acc, val) => {
              for (const key in acc) {
                const checkValue = val[key] ?? 0;
                acc[key] = round(
                  acc[key] +
                    checkValue /
                      allLang.filter((val) => {
                        return val[key];
                      }).length,
                  2,
                );
              }
              return acc;
            },
            {
              CSS: 0,
              Dockerfile: 0,
              HTML: 0,
              JavaScript: 0,
              Pug: 0,
              Shell: 0,
              TypeScript: 0,
            },
          );
          for (const property in programmingLanguages) {
            programmingLanguages[property] =
              programmingLanguages[property] / 10;
          }
          const sumValues = Object.values(programmingLanguages).reduce(
            (a, b) => a + b,
          );
          const sumProgrammingLanguages = Object.fromEntries(
            Object.entries(programmingLanguages).map(([k, v]) => [
              k,
              round((v / sumValues) * 100, 2),
            ]),
          );

          return {
            programmingLanguages: sumProgrammingLanguages,
          };
        },
      },
    },
  });
};
```

Sooo what we are doing here?

- We are making field for <b>SummarizedStats</b> from <b>Gitlab</b> type all resources (context.nodeModel.getAllNodes)
- Then we are maping to get all <b>usedLanguages</b> (even with 0 or null)
- Then reduce them to accumulate only correct values and divide them by amount of existing values (zero/null not counting), and you need to pass object initial values, otherwise you will get in some of them <b>NaN</b>
- After when you get object of summarized languages from each project <b>divide</b> it by 10
- Lastly multiply it by 100% to get correct percent values and round it to 2 decimals and return it to our field <b>programmingLanguages</b>

After everything works as it should you can check if it working on <b>GraphiQL</b>

#### Voila!

![Voila](../images/gitlab-api-on-gatsby-site/voila.png)

Now its up to you! Use your imagination what you can do with this data but if you dont have any idea I will be showing 'how to' show this data on simple chart so stay tuned!
