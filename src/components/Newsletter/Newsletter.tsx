import {
  useState,
  memo,
  useEffect,
  useRef,
  useCallback,
  useContext,
} from 'react';

import firebase from 'gatsby-plugin-firebase';
import { useIntl, FormattedPlural } from 'gatsby-plugin-react-intl';

import Loader from '@common/Loader';
import { colors } from '@common/static';
import ConfettiCannon from '@components/Confetti/ConfettiCanon';
import MessageHub from '@components/MessageHub';
import { usePrevious } from '@hooks/usePrevious';
import { PostsContext } from '@providers/PostsProvider';

import {
  Form,
  StyledButton,
  StyledIcon,
  Wrapper,
  Title,
  InfoStatus,
  Description,
  Count,
} from './styled';
import { Response } from './types';

const rgx =
  /^(([^<>()\\[\]\\.,;:\s@"]+(\.[^<>()\\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const isValidEmail = (emailValue: string): boolean =>
  rgx.test(String(emailValue).toLowerCase());

const Newsletter = (): JSX.Element => {
  const intl = useIntl();
  const pluralSuffix = intl.locale === 'en' ? 's' : 'ów';

  const [isMyInputFocused, setIsMyInputFocused] = useState(false);
  const [emailValue, setEmailValue] = useState('');
  const [isSame, setIsSame] = useState(false);
  const [loader, setLoader] = useState(false);
  const [errorMessage, setErrorMessage] = useState<{
    newsletterInfo: string | null;
    messageHubInfo: string | null;
    error: boolean | null;
  }>({ newsletterInfo: null, messageHubInfo: null, error: null });
  const [newsletterCount, setNewsletterCount] = useState(0);

  const messageHubControllerRef =
    useRef<{
      displayMessage: (msg: string) => void;
    }>();
  const confettiAnchorRef = useRef<HTMLDivElement>(null);
  const prevEmail = usePrevious(emailValue);
  const prevNewsletterCount = usePrevious(newsletterCount);
  const {
    userStore: {
      firebase: { dbData, setDbData },
    },
  } = useContext(PostsContext);

  const notSameInputValue = useCallback(() => {
    if (!isSame && prevEmail !== emailValue) {
      setIsSame(true);
    }
    if (isSame && prevEmail === emailValue) {
      setLoader(false);
      setIsSame(false);
      setErrorMessage({
        newsletterInfo: intl.formatMessage({ id: 'newsletter.email_exists' }),
        messageHubInfo: intl.formatMessage({ id: 'messageHub.email_exists' }),
        error: true,
      });
    }
    return prevEmail !== emailValue;
  }, [emailValue, intl, isSame, prevEmail]);

  useEffect(() => {
    setDbData(firebase.firestore());
    const fetchData = async (): Promise<void> => {
      try {
        await dbData?.collection('newsletterData').onSnapshot((snapshot) => {
          if (snapshot.size) {
            setNewsletterCount(snapshot.size);
          } else {
            console.log('snapshot empty');
          }
        });
      } catch (err) {
        console.error(err);
      }
    };

    fetchData();
    return;
  }, [dbData, setDbData]);

  useEffect(() => {
    if (errorMessage.messageHubInfo) {
      messageHubControllerRef.current?.displayMessage(
        errorMessage.messageHubInfo,
      );
    }
  }, [errorMessage]);

  useEffect(() => {
    setErrorMessage({
      newsletterInfo: null,
      messageHubInfo: null,
      error: null,
    });
    setEmailValue('');
  }, [intl]);

  const sendData = async (): Promise<void> => {
    if (isMyInputFocused && isValidEmail(emailValue)) {
      fetch('https://api.codesigh.com/subscribe', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          email: emailValue,
        }),
      })
        .then((res) => res.json())
        .then((res: Response) => {
          if (res.message.newEmail) {
            setLoader(false);
            setErrorMessage({
              newsletterInfo: intl.formatMessage({
                id: 'newsletter.email_new',
              }),
              messageHubInfo: intl.formatMessage({
                id: 'messageHub.email_new',
              }),
              error: false,
            });
          } else {
            setLoader(false);
            setErrorMessage({
              newsletterInfo: intl.formatMessage({
                id: 'newsletter.email_already_signed',
              }),
              messageHubInfo: intl.formatMessage({
                id: 'messageHub.email_already_signed',
              }),
              error: true,
            });
          }
        })
        .catch((err) => {
          console.error('here is the error: ', err);
        });
    }
  };

  return (
    <>
      <Form isValid={isValidEmail(emailValue)}>
        <Title>Newsletter</Title>
        <Wrapper>
          <label htmlFor="newsletter">
            <input
              id="newsletter"
              type="email"
              placeholder="Email address"
              autoComplete="off"
              autoCorrect="off"
              onFocus={() => setIsMyInputFocused(true)}
              value={emailValue}
              onChange={(event) => setEmailValue(event.target.value)}
            />
          </label>
          <StyledButton
            name="send email"
            onClick={(event) => {
              event.preventDefault();
              notSameInputValue() && sendData() && setLoader(true);
            }}
            disabled={isMyInputFocused && !isValidEmail(emailValue)}
          >
            {!loader ? (
              <StyledIcon name="emailto" width={20} height={20} />
            ) : (
              <Loader width={22} height={22} strokeColor="#00c899" />
            )}
          </StyledButton>
        </Wrapper>
        <InfoStatus error={errorMessage.error}>
          {errorMessage.newsletterInfo}
        </InfoStatus>
        <Description ref={confettiAnchorRef}>
          {typeof prevNewsletterCount !== 'undefined' &&
            newsletterCount > prevNewsletterCount &&
            prevNewsletterCount > 0 && (
              <ConfettiCannon
                anchorRef={confettiAnchorRef}
                colors={colors}
                dotCount={80}
                initialHorizontal={{ min: -2000, max: 2000 }}
                initialUpwards={{ min: -800, max: 1000 }}
                size={{ min: 1, max: 14 }}
              />
            )}
          {intl.formatMessage({ id: 'newsletter.join_other' })}{' '}
          <Count>{newsletterCount}</Count>{' '}
          <FormattedPlural
            value={newsletterCount}
            one={intl.formatMessage({ id: 'user' })}
            other={`${intl.formatMessage({ id: 'user' })}${pluralSuffix}`}
          />{' '}
          {intl.formatMessage({ id: 'newsletter.already_subscribed' })}
        </Description>
      </Form>
      <MessageHub ref={messageHubControllerRef} timeout={2500} />
    </>
  );
};

export default memo(Newsletter);
