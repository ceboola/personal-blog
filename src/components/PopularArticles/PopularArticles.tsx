import { useContext, memo } from 'react';

import loadable from '@loadable/component';
import { useIntl } from 'gatsby-plugin-react-intl';

import { getTopFiveArticles } from '@helpers/getTopFiveArticles';
import { PostsContext } from '@providers/PostsProvider';

const Categories = loadable(
  () => import(/* webpackChunkName: "Categories" */ './Categories'),
);
const FeaturedArticle = loadable(
  () => import(/* webpackChunkName: "FeaturedArticle" */ './FeaturedArticle'),
);
const Popular = loadable(
  () => import(/* webpackChunkName: "Popular" */ './Popular'),
);

import { Container, Wrapper } from './styled';

const PopularArticles: React.FC = () => {
  const { posts, data } = useContext(PostsContext);
  const intl = useIntl();
  const filteredPosts = posts.filter((edge) =>
    edge.node.frontmatter.lang.includes(intl.locale),
  );

  const popularData = getTopFiveArticles(filteredPosts, data.GAData);
  return (
    <Wrapper>
      <Categories />
      <Container>
        <FeaturedArticle top5={popularData} />
        <Popular top5={popularData} />
      </Container>
    </Wrapper>
  );
};

export default memo(PopularArticles);
