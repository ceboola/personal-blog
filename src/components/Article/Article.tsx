import { useEffect, useState, memo, useRef } from 'react';

import { FormattedDate } from 'gatsby-plugin-react-intl';
import dompurify from 'isomorphic-dompurify';
import { useInView } from 'react-intersection-observer';
import { useSpring, animated } from 'react-spring';

import ShareMedia from '@components/ShareMedia';
import StarRating from '@components/StarRating';
import { useDimensions } from '@hooks/useDimensions';
import { useWindowSize } from '@hooks/useWindowSize';

import {
  Description,
  ImageWrapper,
  Line,
  StyledImg,
  StyledPageStats,
  Title,
  Wrapper,
  Container,
  Date,
  StyledTimeToRead,
  ParallaxObserver,
  StyledSticky,
} from './styled';
import { Props } from './types';

const Article: React.FC<Props> = ({
  title,
  post,
  date,
  featuredImage,
  slug,
  currentPageViews,
  timeToRead,
}) => {
  const sanitizer = dompurify.sanitize;
  const [{ width }, ref] = useDimensions();
  const [{ width: windowWidth }] = useWindowSize();
  const [show, setShow] = useState(false);
  const [mediaHeight, setMediaHeight] = useState<number | null>(0);
  const [imageHeight, setImageHeight] = useState<number | null | undefined>(0);
  const mediaRef = useRef<HTMLDivElement | null>(null);
  const imageRef = useRef<HTMLDivElement | null>(null);

  const calc = (o: number): string =>
    `translateY(${o < 1 ? Math.abs(o) * 0.5 : 0}px)`;
  const divide = (o: number): number => (o < 1 ? 1 - Math.abs(o) / 250 : 1);
  const [{ offset }, api] = useSpring(() => ({
    offset: 0,
  }));
  const { ref: observerRef, entry } = useInView({
    threshold: [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1],
    rootMargin: '-20px',
  });
  const { ref: sectionRef, inView } = useInView({
    triggerOnce: true,
    rootMargin: `0px 0px ${windowWidth > 480 ? '-300px' : '0px'} 0px`,
  });

  useEffect(() => {
    setMediaHeight(mediaRef.current && mediaRef.current?.clientHeight);
    setImageHeight(
      imageRef.current && imageRef.current.clientHeight > 250
        ? imageRef.current.clientHeight
        : (imageRef.current?.clientHeight as number) * 2,
    );
    setShow(true);
    const offset = entry?.boundingClientRect.top;
    api.start({ offset });
    typeof offset !== 'undefined' && Math.abs(offset) > 400 && api.stop();
  }, [
    offset,
    entry?.boundingClientRect.top,
    mediaRef.current?.clientHeight,
    imageRef.current?.clientHeight,
    api,
  ]);

  return (
    <>
      <ParallaxObserver ref={observerRef} height={imageHeight} />
      {featuredImage && (
        <animated.div
          style={{
            transform: offset.to(calc),
            // https://github.com/pmndrs/react-spring/issues/1102
            opacity: offset.to(divide) as never,
          }}
        >
          <ImageWrapper ref={imageRef}>
            <StyledImg fluid={featuredImage.childImageSharp.fluid} />
          </ImageWrapper>
        </animated.div>
      )}
      <Wrapper ref={ref} marginTop={featuredImage != null}>
        <StyledSticky
          enabled={windowWidth > 480}
          top={50}
          bottomBoundary={
            ref.current
              ? ref.current?.clientHeight +
                ref.current?.offsetTop -
                (mediaHeight !== null ? mediaHeight : 300)
              : 0
          }
        >
          {inView && <ShareMedia ref={mediaRef} />}
        </StyledSticky>

        <header>
          <Container>
            <Title>{title}</Title>
            <StyledPageStats
              slug={slug}
              pageViews={currentPageViews}
              divWidth={width}
              show={show}
            />
          </Container>
          <Description>
            <Date>
              <FormattedDate
                value={date}
                year="numeric"
                month="long"
                day="2-digit"
              />
            </Date>
            <StyledTimeToRead measurment={false} time={timeToRead} />
            <StarRating />
          </Description>
        </header>
        <section
          style={{
            height: 'max-content',
          }}
        >
          <div
            style={{
              height: 'max-content',
            }}
            ref={sectionRef}
            dangerouslySetInnerHTML={{ __html: sanitizer(post) }}
          ></div>
        </section>
      </Wrapper>
      <Line />
    </>
  );
};

export default memo(Article);
