require('dotenv').config({
  path: `.env`,
});

const feedQuery = (lang) => {
  return `
{
  allMarkdownRemark(
    filter: { frontmatter: {lang: {eq: "${lang}"}} },
    sort: { order: DESC, fields: [frontmatter___date] },
  ) {
    edges {
      node {
        excerpt
        html
        fields { slug }
        frontmatter {
          title
          date
          lang
        }
      }
    }
  }
}
`;
};

const feedSerialize = (site, allMarkdownRemark) => {
  return allMarkdownRemark.edges.map((edge) => {
    return Object.assign({}, edge.node.frontmatter, {
      description: edge.node.excerpt,
      date: edge.node.frontmatter.date,
      url: site.siteMetadata.siteUrl + edge.node.fields.slug,
      guid: site.siteMetadata.siteUrl + edge.node.fields.slug,
      custom_elements: [{ 'content:encoded': edge.node.html }],
    });
  });
};

const config = {
  siteMetadata: {
    title: `codesigh.com blog`,
    author: {
      name: `Pawel`,
      summary: `Frontend enthusiast`,
    },
    description: `Frontend fancy stuff`,
    siteUrl: `https://blog.codesigh.com/`,
    keywords: 'JavaScript, React, Gatsby, TypeScript',
    social: {
      twitter: `none`,
    },
  },
  flags: {
    DEV_SSR: true,
    FAST_DEV: true,
    FAST_REFRESH: true,
    PARALLEL_SOURCING: true,
  },
  plugins: [
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/content/blog`,
        name: `blog`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/content/blog/images`,
        name: `images`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/static/assets`,
        name: `assets`,
      },
    },
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        plugins: [
          {
            resolve: `gatsby-remark-images`,
            options: {
              maxWidth: 990,
            },
          },
          {
            resolve: `gatsby-remark-responsive-iframe`,
            options: {
              wrapperStyle: `margin-bottom: 1.0725rem`,
            },
          },
          `gatsby-remark-prismjs`,
          `gatsby-remark-copy-linked-files`,
          `gatsby-remark-smartypants`,
        ],
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: `UA-181076912-1`,
      },
    },
    {
      resolve: `gatsby-plugin-feed`,
      options: {
        query: `
          {
            site {
              siteMetadata {
                title
                description
                siteUrl
                site_url: siteUrl
              }
            }
          }
        `,
        feeds: [
          {
            serialize: ({ query: { site, allMarkdownRemark } }) =>
              feedSerialize(site, allMarkdownRemark),
            query: feedQuery('en'),
            output: '/rss.xml',
            title: 'blog.codesigh.com RSS Feed in EN',
          },
          {
            serialize: ({ query: { site, allMarkdownRemark } }) =>
              feedSerialize(site, allMarkdownRemark),
            query: feedQuery('pl'),
            output: '/pl/rss.xml',
            title: 'blog.codesigh.com RSS Feed in PL',
          },
        ],
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `codesigh.com blog`,
        short_name: `blog`,
        start_url: `/`,
        background_color: `#ffffff`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `static/assets/gatsby-icon.png`,
      },
    },
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-typescript`,
    {
      resolve: `gatsby-plugin-typography`,
      options: {
        pathToConfigModule: `src/utils/typography`,
        omitGoogleFont: `true`,
      },
    },
    {
      resolve: `gatsby-plugin-styled-components`,
      options: {
        displayName: false,
      },
    },
    {
      resolve: 'gatsby-background-image-es5',
      options: {
        // add your own characters to escape, replacing the default ':/'
        specialChars: '/:',
      },
    },
    {
      resolve: `gatsby-plugin-google-fonts-with-attributes`,
      options: {
        fonts: [
          `montserrat:400,700,900`,
          `ubuntu:400,700,900`, // you can also specify font weights and styles
        ],
        display: 'swap',
        attributes: {
          rel: 'stylesheet preload prefetch',
        },
      },
    },
    {
      resolve: `gatsby-plugin-svg-sprite-loader`,
      options: {
        esModule: false,
        publicPath: '/',
        // spriteFilename: 'sprites.svg',
        extract: false,
        symbolId: '[name]',
        /* SVG sprite loader options */
        pluginOptions: {
          /* SVG sprite loader plugin options */
          // plainSprite: true,
        },
      },
    },
    {
      resolve: `gatsby-plugin-react-intl`,
      options: {
        path: `${__dirname}/i18n`,
        languages: [`en`, `pl`],
        defaultLanguage: `en`,
        redirect: false,
      },
    },
    {
      resolve: 'gatsby-plugin-preconnect',
      options: {
        domains: [
          'https://talk.hyvor.com',
          'https://fonts.gstatic.com',
          'http://talk.hyvor.com',
        ],
      },
    },
    {
      resolve: 'gatsby-plugin-webpack-bundle-analyser-v2',
      options: {
        devMode: false,
      },
    },
    {
      resolve: `gatsby-plugin-lodash`,
      options: {
        disabledFeatures: [`shorthands`, `cloning`],
      },
    },
    {
      resolve: `gatsby-plugin-create-client-paths`,
      options: { prefixes: [`/dashboard/*`, `/pl/dashboard/*`] },
    },
    {
      resolve: 'gatsby-plugin-firebase',
      options: {
        credentials: {
          apiKey: process.env.GATSBY_FIREBASE_API_KEY,
          authDomain: process.env.GATSBY_FIREBASE_AUTH_DOMAIN,
          databaseURL: process.env.GATSBY_FIREBASE_DATABASE_URL,
          projectId: process.env.GATSBY_FIREBASE_PROJECT_ID,
          storageBucket: process.env.GATSBY_FIREBASE_STORAGE_BUCKET,
          messagingSenderId: process.env.GATSBY_FIREBASE_MESSAGING_SENDER_ID,
          appId: process.env.GATSBY_FIREBASE_APP_ID,
        },
      },
    },
    {
      resolve: `gatsby-transformer-json`,
      options: {
        typeName: `ProjectStatsCloc`, // a fixed string
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/static/count_total.json`,
      },
    },
    {
      resolve: `gatsby-plugin-advanced-sitemap`,
      options: {
        query: `
        {
        allPages: allMarkdownRemark {
          edges {
            node {
              id
              fields {
                slug
              }
              frontmatter {
                featuredImage {
                  childImageSharp {
                    fluid {
                      src
                    }
                  }
                }
              }
            }
          }
        }
        allTags: allSitePage(filter: {component: {regex: "/\\/templates\\/tags\\//"}}) {
          edges {
            node {
              id
              path
            }
          }
        }
      }`,
        output: '/sitemap_index.xml',
        mapping: {
          allPages: {
            sitemap: `posts`,
            serializer: (edges) => {
              return edges.map(({ node }) => {
                return {
                  node: {
                    id: node.id,
                    slug: node.fields.slug,
                    url: node.fields.slug,
                    feature_image:
                      process.env.GATSBY_SITE_URL +
                      node.frontmatter.featuredImage.childImageSharp.fluid.src,
                  },
                };
              });
            },
          },
          allTags: {
            sitemap: `tags`,
            serializer: (edges) => {
              return edges.map(({ node }) => {
                return {
                  node: {
                    id: node.id,
                    slug: node.path,
                    url: node.path,
                  },
                };
              });
            },
          },
        },
        exclude: [
          `/dev-404-page`,
          `/404`,
          `/404.html`,
          `/offline-plugin-app-shell-fallback`,
          `/my-excluded-page`,
          /(\/)?hash-\S*/, // you can also pass valid RegExp to exclude internal tags for example
        ],
        createLinkInHead: true, // optional: create a link in the `<head>` of your site
        addUncaughtPages: true, // optional: will fill up pages that are not caught by queries and mapping and list them under `sitemap-pages.xml`
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
};

process.env.NODE_ENV === 'production' &&
  config.plugins.push({
    resolve: `gatsby-plugin-loadable-components-ssr`,
    options: {
      // Whether replaceHydrateFunction should call ReactDOM.hydrate or ReactDOM.render
      // Defaults to ReactDOM.render on develop and ReactDOM.hydrate on build
      useHydrate: false,
    },
  });

module.exports = config;
