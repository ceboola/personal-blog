import { useState, useEffect } from 'react';

import debounce from 'lodash.debounce';

import useIsomorphicLayoutEffect from '@hooks/useIsoMorphicLayoutEffect';
/**
 * A hook used to measure the height of the page.
 * Assign the return ref to a div and the height value will be stored in height.
 * @returns {Array} [{height, width}, ref]
 */
export function useWindowSize(): readonly [
  { readonly height: number | false; readonly width: number | false },
] {
  const [height, setHeight] = useState(
    typeof window !== 'undefined' && window.innerHeight,
  );
  const [width, setWidth] = useState(
    typeof window !== 'undefined' && window.innerWidth,
  );

  const RESET_TIMEOUT = 300;

  const windowDimensions = (): void => {
    if (typeof window === 'undefined') return;
    if (typeof window !== 'undefined') {
      setHeight(window.innerHeight);
      setWidth(window.innerWidth);
    }
  };

  useIsomorphicLayoutEffect(() => {
    windowDimensions();
  }, [windowDimensions]);

  const debouncedDimensions = debounce(windowDimensions, RESET_TIMEOUT);

  useEffect(() => {
    window.addEventListener('resize', debouncedDimensions, {
      capture: false,
      passive: true,
    });
    return () => {
      window.removeEventListener('resize', debouncedDimensions);
    };
  });

  return [{ height, width }] as const;
}
