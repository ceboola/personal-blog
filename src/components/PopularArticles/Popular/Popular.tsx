import { useContext, useEffect, useState, memo } from 'react';

import loadable from '@loadable/component';
import { Link } from 'gatsby';
import { useIntl } from 'gatsby-plugin-react-intl';

const PageStats = loadable(
  () => import(/* webpackChunkName: "PageStatsPopular" */ '@common/PageStats'),
);
import { PopularnFeatureProps } from '@components/PopularArticles/types';
import { useDimensions } from '@hooks/useDimensions';
import { PostsContext } from '@providers/PostsProvider';

import {
  Clamp,
  Heading,
  Wrapper,
  PopularContainer,
  HoverWrapper,
  PopularImage,
  InfoWrapper,
  StyledDateBadge,
  Title,
  StyledIcon,
  StyledTimeToRead,
} from './styled';

const Popular: React.FC<PopularnFeatureProps> = ({ top5 }) => {
  const { data } = useContext(PostsContext);
  const [{ width }, ref] = useDimensions();
  const intl = useIntl();

  const [show, setShow] = useState(false);
  useEffect(() => {
    setShow(true);
  }, []);

  return (
    <Wrapper>
      <Heading>{intl.formatMessage({ id: 'popular_articles' })}</Heading>
      {top5.length <= 1 && (
        <div>{intl.formatMessage({ id: 'data_not_found' })}</div>
      )}
      {top5.length > 0 &&
        top5.slice(1).map((result, index) => {
          const {
            timeToRead,
            frontmatter: { title, featuredImage, date },
            fields: { slug },
          } = result;
          const currentPageViews = data.GAData.edges.find(
            (filteredPageViews) => filteredPageViews.node.id === slug,
          )?.node.totalCount;
          return (
            <PopularContainer key={`${title}_${index}`}>
              <Link to={slug}>
                <HoverWrapper ref={ref}>
                  <StyledIcon name="reading-book" />
                  <PageStats
                    slug={slug}
                    divWidth={width}
                    pageViews={currentPageViews}
                  />
                  <StyledDateBadge date={date} />
                  <PopularImage fluid={featuredImage.childImageSharp.fluid} />
                </HoverWrapper>
                <InfoWrapper>
                  <Clamp>
                    <Title>{title}</Title>
                  </Clamp>
                  <StyledTimeToRead time={timeToRead} show={show} />
                </InfoWrapper>
              </Link>
            </PopularContainer>
          );
        })}
    </Wrapper>
  );
};

export default memo(Popular);
