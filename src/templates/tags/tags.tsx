import { useContext, useEffect } from 'react';

import { graphql, Link } from 'gatsby';
import { FormattedPlural, useIntl } from 'gatsby-plugin-react-intl';
import { Helmet } from 'react-helmet';

import ArticleStart from '@components/ArticleStart';
import { ArticleContainer } from '@components/ArticleStart/styled';
import TagsList from '@components/TagsList';
import { PostsContext } from '@providers/PostsProvider';

import { Title, StyledLink, Phrase } from './styled';
import { Props } from './types';

const Tags: React.FC<Props> = ({ pageContext }) => {
  const { data, setPostsCategory } = useContext(PostsContext);
  const intl = useIntl();
  const tagsx = data.tags.group;
  const totalPosts = data.totalPosts.totalCount;
  const { title } = data.site.siteMetadata;
  const { tag } = pageContext;
  const { totalCount } = data.allMarkdownRemark;

  useEffect(() => {
    setPostsCategory(intl.formatMessage({ id: 'all_posts' }));
  }, [intl, setPostsCategory]);

  return (
    <div>
      <Helmet
        title={`${intl.formatMessage({ id: 'tags' })} ${tag} | ${title}`}
      />
      <Title>
        {`${totalCount} `}
        <FormattedPlural
          value={totalCount}
          one={intl.formatMessage({ id: 'postTag.singular.one' })}
          other={intl.formatMessage({ id: 'postTag.plural.other' })}
          {...(intl.locale === 'pl' && {
            few: intl.formatMessage({ id: 'postTag.plural.few' }),
            many: intl.formatMessage({ id: 'postTag.plural.many' }),
          })}
        />{' '}
        &quot;<Phrase>{tag}</Phrase>&quot;
      </Title>
      <ArticleContainer>
        <ArticleStart activeTag={tag} />
        <TagsList link={Link} tags={tagsx} tag={tag} totalPosts={totalPosts} />
      </ArticleContainer>
      <StyledLink to={`${intl.locale === 'pl' ? '/pl' : ''}/tags`}>
        {intl.formatMessage({ id: 'all_tags' })}
      </StyledLink>
    </div>
  );
};

export default Tags;

export const pageQuery = graphql`
  query ($tag: String, $language: String!, $slug: String!) {
    site {
      siteMetadata {
        title
      }
    }
    SummarizedStats {
      ...SummarizedStats
    }
    gitlab(name: { eq: "personal-blog" }) {
      ...CurrentProject
    }
    currentPageViews: pageViews(path: { eq: $slug }) {
      ...SitePageViews
    }
    tags: allMarkdownRemark(
      filter: { frontmatter: { lang: { eq: $language } } }
    ) {
      group(field: frontmatter___tags) {
        fieldValue
        totalCount
      }
    }
    totalPosts: allMarkdownRemark(
      filter: { frontmatter: { lang: { eq: $language } } }
    ) {
      totalCount
    }
    allMarkdownRemark(
      limit: 2000
      sort: { fields: [frontmatter___date], order: DESC }
      filter: { frontmatter: { tags: { in: [$tag] }, lang: { eq: $language } } }
    ) {
      totalCount
      edges {
        node {
          timeToRead
          excerpt
          fields {
            slug
          }
          frontmatter {
            date(formatString: "MMMM DD, YYYY")
            title
            description
            tags
            lang
            featuredImage {
              childImageSharp {
                fluid(quality: 50) {
                  ...GatsbyImageSharpFluid_withWebp
                }
              }
            }
          }
        }
      }
    }
    GAData: allPageViews {
      edges {
        node {
          id
          totalCount
        }
      }
    }
  }
`;
