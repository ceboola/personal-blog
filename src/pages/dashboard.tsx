import { useContext, useEffect } from 'react';

import { Router } from '@reach/router';
import { graphql } from 'gatsby';
import { useIntl } from 'gatsby-plugin-react-intl';

import Dashboard from '@components/Dashboard';
import Login from '@components/Login';
import PrivateRoute from '@components/PrivateRoute';
import { PostsContext } from '@providers/PostsProvider';
import { isLoggedIn } from '@utils/auth';

const LoginPage = (): JSX.Element => {
  const intl = useIntl();
  const lang = intl.locale === 'en' ? '' : '/pl';
  const {
    userStore: {
      greetingUser: { greeting, setGreeting },
    },
  } = useContext(PostsContext);

  useEffect(() => {
    if (!isLoggedIn() && !greeting) {
      setGreeting(true);
    }
  }, [greeting, setGreeting]);

  return (
    <>
      <Router>
        <Login path={`${lang}/dashboard/`} />
        <PrivateRoute path={`${lang}/dashboard/user/`} component={Dashboard} />
      </Router>
    </>
  );
};

export const pageQuery = graphql`
  query ($slug: String!) {
    site {
      siteMetadata {
        title
        siteUrl
      }
    }
    SummarizedStats {
      ...SummarizedStats
    }
    gitlab(name: { eq: "personal-blog" }) {
      ...CurrentProject
    }
    currentPageViews: pageViews(path: { eq: $slug }) {
      ...SitePageViews
    }
    sitePage {
      path
    }
  }
`;

export default LoginPage;
