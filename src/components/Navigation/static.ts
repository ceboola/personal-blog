import { NavData } from './types';

export const navData: NavData[] = [
  {
    name: 'home',
    url: '/',
  },
  {
    name: 'about_me',
    url: '/about/',
  },
  {
    name: 'contact.title',
    url: '/contact/',
  },
  {
    name: 'tags',
    url: '/tags/',
  },
];
