export interface ShareMediaDataProps {
  icon: string;
  url: string;
}

export const shareMediaData: ShareMediaDataProps[] = [
  {
    icon: 'twitter',
    url: '//twitter.com/intent/tweet?url=',
  },
  {
    icon: 'facebook',
    url: '//facebook.com/sharer.php?u=',
  },
  {
    icon: 'linkedin',
    url: '//linkedin.com/shareArticle?url=',
  },
  {
    icon: 'reddit',
    url: '//reddit.com/submit?url=',
  },
  {
    icon: 'wykop',
    url: '//wykop.pl/dodaj/link/?url=',
  },
];
