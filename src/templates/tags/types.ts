import { PageRendererProps } from 'gatsby';
import { FluidObject } from 'gatsby-image';

import {
  MarkdownRemarkConnection,
  MarkdownRemarkGroupConnection,
} from '@typings/graphql-types';

interface AllMarkdownRemark extends Omit<MarkdownRemarkConnection, 'edges'> {
  edges: Array<{
    node: {
      excerpt: string;
      frontmatter: {
        title: string;
        date: string;
        description: string;
        featuredImage: {
          childImageSharp: {
            fluid: FluidObject;
          };
        };
        tags: string[];
      };
      fields: { slug: string };
    };
  }>;
}

export interface Props extends PageRendererProps {
  data: {
    allMarkdownRemark: AllMarkdownRemark;
    site: {
      siteMetadata: {
        title: string;
      };
    };
    tags: {
      group: MarkdownRemarkGroupConnection[];
    };
    totalPosts: MarkdownRemarkConnection;
  };
  pageContext: {
    tag?: string[];
  };
}

// type Props2 = Record<'data', Record<'allMarkdownRemark', AllMarkdownRemark>> &
//   Record<'pageContext', Partial<Record<'tag', string>>>;
