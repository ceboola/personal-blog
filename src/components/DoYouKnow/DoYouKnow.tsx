import { FC, memo } from 'react';

import { Link } from 'gatsby';
import { useIntl } from 'gatsby-plugin-react-intl';

import {
  Wrapper,
  Content,
  StyledButton,
  StyledIcon,
  Title,
  CTATitle,
  Description,
} from './styled';
import { DoYouKnowProps } from './types';

const DoYouKnow: FC<DoYouKnowProps> = ({ children }): JSX.Element => {
  const intl = useIntl();

  return (
    <Wrapper>
      <Content>
        <StyledIcon name="question-mark" height={60} width={60} />
        <Title>{`${intl.formatMessage({ id: 'doYouKnow.title' })}`}</Title>
        <Description>
          {children}
          <CTATitle>{`${intl.formatMessage({
            id: 'doYouKnow.cta_title',
          })}`}</CTATitle>
          <Link to="/stats/">
            <StyledButton>{`${intl.formatMessage({
              id: 'doYouKnow.buttonText',
            })}`}</StyledButton>
          </Link>
        </Description>
      </Content>
    </Wrapper>
  );
};

export default memo(DoYouKnow);
