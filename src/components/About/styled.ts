import styled from 'styled-components';

import { media } from '@utils/media';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  span {
    &:nth-child(0) {
      background-image: linear-gradient(#00c899, #00c899);
      background-size: 100% 5px;
      background-position: 0% 100%;
      background-repeat: no-repeat;
      padding-bottom: 0px;
    }
  }
`;

export const Title = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
`;

export const List = styled.ul`
  margin: 24px 0;
  li {
    &::marker {
      color: #17173f;
      font-size: 20px;
    }
  }
  div {
    display: inline-flex;
    align-items: center;
  }
  svg {
    margin-left: 5px;
  }
  ${media.lg`
      list-style: inside;
  `}
`;

export const Underline = styled.div`
  background-image: linear-gradient(#00c899, #00c899);
  background-size: 100% 5px;
  background-position: 0% 100%;
  background-repeat: no-repeat;
  padding-bottom: 0px;
  width: max-content;
  display: inline;
`;

export const Description = styled.div`
  margin-bottom: 40px;
`;
