import { graphql } from 'gatsby';

import Unsubscribe from '@components/Unsubscribe';

const UnsubscribePage: React.FC = () => {
  return <Unsubscribe />;
};

export const pageQuery = graphql`
  query($slug: String!) {
    site {
      siteMetadata {
        title
      }
    }
    SummarizedStats {
      ...SummarizedStats
    }
    gitlab(name: { eq: "personal-blog" }) {
      ...CurrentProject
    }
    currentPageViews: pageViews(path: { eq: $slug }) {
      ...SitePageViews
    }
  }
`;

export default UnsubscribePage;
