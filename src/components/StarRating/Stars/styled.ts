import styled from 'styled-components';

import CommonIcon from '@common/CommonIcon';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
`;

export const StyledIcon = styled(CommonIcon)<{ fill: string }>`
  fill: ${(props) => props.fill};
  cursor: pointer;
`;
