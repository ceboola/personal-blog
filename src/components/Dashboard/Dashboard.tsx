import { useContext, useEffect, useRef, useState, memo } from 'react';

import { useIntl } from 'gatsby-plugin-react-intl';

import { colors } from '@common/static';
import AdminPanel from '@components/AdminPanel';
import ConfettiCannon from '@components/Confetti/ConfettiCanon';
import MessageHub from '@components/MessageHub';
import UserPanel from '@components/UserPanel';
import { PostsContext } from '@providers/PostsProvider';
import { getCurrentUser } from '@utils/auth';

import { Wrapper } from './styled';

const Dashboard = (): JSX.Element => {
  const intl = useIntl();
  const { displayName, uid } = getCurrentUser();
  const [msg, setMsg] = useState('');
  const [isMounted, setIsMounted] = useState(false);
  const messageHubControllerRef =
    useRef<{
      displayMessage: (msg?: string) => void;
    }>(null);
  const divRef = useRef<HTMLDivElement>(null);
  const {
    userStore: {
      greetingUser: { greeting, setGreeting },
    },
  } = useContext(PostsContext);

  useEffect(() => {
    setMsg(
      `${intl.formatMessage({
        id: 'dashboard.greetings',
      })} ${displayName}`,
    );
    if (greeting && msg) {
      messageHubControllerRef.current?.displayMessage(msg);
      setGreeting(false);
      setTimeout(() => {
        setIsMounted(true);
      }, 500);
    }
  }, [displayName, msg, setGreeting, greeting, setIsMounted, intl]);

  return (
    <Wrapper ref={divRef}>
      <p>Welcome back to your profile, {displayName}!</p>
      {isMounted && (
        <ConfettiCannon
          anchorRef={divRef}
          colors={colors}
          dotCount={200}
          initialHorizontal={{ min: -3000, max: 3000 }}
          initialUpwards={{ min: -1500, max: 2000 }}
          size={{ min: 14, max: 22 }}
        />
      )}
      {process.env.GATSBY_FIREBASE_ADMIN_UID === uid ? (
        <AdminPanel />
      ) : (
        <UserPanel />
      )}
      <MessageHub ref={messageHubControllerRef} timeout={3000} />
    </Wrapper>
  );
};

export default memo(Dashboard);
