import { FC, FunctionComponent } from 'react';

import { RouteComponentProps } from '@reach/router';
import { navigate } from 'gatsby';
import { useIntl } from 'gatsby-plugin-react-intl';

import { isLoggedIn } from '@utils/auth';

type PrivateRouteProps = { component: FunctionComponent } & RouteComponentProps;
const PrivateRoute: FC<PrivateRouteProps> = ({
  component: Component,
  location,
  ...rest
}) => {
  const intl = useIntl();
  const lang = intl.locale === 'en' ? '' : '/pl';

  if (!isLoggedIn() && location?.pathname !== `/`) {
    // If we’re not logged in, redirect to the home page.
    navigate(`${lang}/dashboard/`);
    return null;
  }

  return <Component {...rest} />;
};

export default PrivateRoute;
