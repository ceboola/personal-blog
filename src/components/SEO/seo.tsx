import { useContext } from 'react';

import { useStaticQuery, graphql } from 'gatsby';
import { useIntl } from 'gatsby-plugin-react-intl';
import { Helmet } from 'react-helmet';

import { PostsContext } from '@providers/PostsProvider';

import { Props } from './types';

const SEO: React.FC<Props> = ({ description, meta, title, article }) => {
  const { site, file } = useStaticQuery(
    graphql`
      query {
        site {
          siteMetadata {
            title
            description
            social {
              twitter
            }
          }
        }
        file(relativePath: { eq: "gatsby-icon.png" }) {
          childImageSharp {
            fixed(width: 128, height: 128) {
              ...GatsbyImageSharpFixed
            }
          }
        }
      }
    `,
  );
  const {
    pageContext: { slug },
  } = useContext(PostsContext);

  const metaDescription = description ?? site.siteMetadata.description;
  const intl = useIntl();

  return (
    <Helmet
      htmlAttributes={{
        lang: intl.locale === 'pl' ? 'pl' : 'en',
      }}
      title={title}
      titleTemplate={`%s | ${site.siteMetadata.title}`}
      meta={[
        {
          name: `description`,
          content: metaDescription,
        },
        {
          property: `og:title`,
          content: title,
        },
        {
          property: `og:description`,
          content: metaDescription,
        },
        {
          property: `og:type`,
          content: `website`,
        },
        {
          name: `twitter:card`,
          content: `summary`,
        },
        {
          name: `twitter:creator`,
          content: site.siteMetadata.social.twitter,
        },
        {
          name: `twitter:title`,
          content: title,
        },
        {
          name: `twitter:description`,
          content: metaDescription,
        },
      ].concat(meta ?? [])}
    >
      {article && (
        <script type="application/ld+json">
          {JSON.stringify({
            '@context': 'https://schema.org',
            '@type': 'BlogPosting',
            headline: article.markdownRemark.frontmatter.title,
            articleBody:
              article.markdownRemark.excerpt ??
              article.markdownRemark.frontmatter.description,
            image: `${process.env.GATSBY_SITE_URL}${article.markdownRemark.frontmatter.featuredImage.childImageSharp.fluid.src}`,
            datePublished: article.markdownRemark.frontmatter.rawDate,
            publisher: {
              '@type': 'Organization',
              name: 'codesigh blog',
              logo: {
                '@type': 'ImageObject',
                height: '128',
                width: '128',
                url: `${process.env.GATSBY_SITE_URL}${file.childImageSharp.fixed.src}`,
              },
            },
            mainEntityOfPage: {
              '@type': 'WebPage',
              '@id': `${process.env.GATSBY_SITE_URL}${slug}`,
            },
            dateModified: article.markdownRemark.frontmatter.rawDate,
            author: {
              '@type': 'Person',
              name: 'ceboola',
            },
            inLanguage:
              article.markdownRemark.frontmatter.lang === 'en' ? 'en' : 'pl',
          })}
        </script>
      )}
    </Helmet>
  );
};

export default SEO;
