import { useEffect, useState, useContext } from 'react';

import { PostsContext } from '@providers/PostsProvider';

import { PersonalInfo, ProjectsStats, CurrentProjectStats } from './Templates';

export const RandomTemplate = (): JSX.Element => {
  const {
    pageContext: { clearSlug },
  } = useContext(PostsContext);
  const randomize = (myArray: Array<JSX.Element>): JSX.Element => {
    return myArray[Math.floor(Math.random() * myArray.length)];
  };

  const [random, setRandom] = useState(<></>);
  useEffect(() => {
    const arr = [
      <PersonalInfo key="personalInfo" />,
      <ProjectsStats key="projectStats" />,
      <CurrentProjectStats key="currentprojectstats" />,
    ];
    setRandom(randomize(arr));
  }, [clearSlug]);
  return <>{random}</>;
};
