import { PageProps } from 'gatsby';

export interface PageTransitionProps {
  props: PageProps;
}
