import { ReactNode } from 'react';

import { GatsbyLinkProps } from 'gatsby';

export interface Props {
  to: GatsbyLinkProps<string>['to'];
  children: ReactNode;
}
