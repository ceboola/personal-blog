export type CommonButtonProps = {
  children?: HTMLCollection | string | React.ReactNode;
  onClick?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
  ref?: React.RefObject<HTMLButtonElement>;
} & React.ButtonHTMLAttributes<HTMLButtonElement>;
