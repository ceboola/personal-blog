import styled, { css } from 'styled-components';

import CommonButton from '@common/CommonButton';
import CommonIcon from '@common/CommonIcon';
import { media } from '@utils/media';

export const StyledButton = styled(CommonButton)`
  position: absolute;
  right: 0;
  color: #000;
  height: 100%;
  padding: 7px 14px;
  width: 50px;
  box-sizing: border-box;
  &:disabled {
    background-color: #5a5858;
    color: #9c9c9c;
  }
`;

export const Form = styled.form<{ isValid: boolean }>`
  position: relative;
  margin: 0;
  width: max-content;
  max-width: 233px;
  input {
    padding-right: 50px;
    width: 233px;
    &:focus {
      outline-style: solid;
      outline-color: transparent;
      border: 2px solid
        ${(props) => (props.isValid ? '#128723' : 'transparent')};
      box-shadow: 0 0 5px
        ${(props) => (props.isValid ? '#128723' : 'transparent')};
      border-radius: 3px;
    }
  }
  ${StyledButton} {
    ${(props) =>
      props.isValid &&
      css`
        &:focus {
          border: 1px solid #128723;
          border-left: none;
          padding: 6px 13px;
        }
      `};
  }
  ${media.sm`
    margin-bottom: 32px;
  `}
`;

export const Wrapper = styled.div`
  position: relative;
  margin-bottom: 2px;
  width: max-content;
  max-width: 233px;
`;

export const Title = styled.h3`
  margin: 0 0 12px 0;
`;

export const StyledIcon = styled(CommonIcon)``;

export const InfoStatus = styled.div<{ error: boolean | null }>`
  width: 100%;
  font-size: 11px;
  background-color: transparent;
  height: 18px;
  display: flex;
  align-items: center;
  justify-content: center;
  ${(props) =>
    props.error &&
    css`
      background-color: red;
    `};
  ${(props) =>
    props.error === false &&
    css`
      background-color: #128723;
    `};
`;

export const Description = styled.div`
  font-size: 10px;
  font-family: 'Montserrat';
  display: flex;
  justify-content: flex-start;
  margin-top: -5px;
`;

export const Count = styled.span`
  display: contents;
  color: #00c899;
  font-size: 17px;
`;
