import { FC, useEffect, useMemo, useRef } from 'react';

import { Link } from 'gatsby';
import { FormattedPlural, useIntl } from 'gatsby-plugin-react-intl';
import { useSpring } from 'react-spring';

import { useDeviceDetect } from '@hooks/useDeviceDetect';
import useScrollPosition from '@hooks/useScrollPosition';

import {
  Wrapper,
  SearchList,
  Title,
  Description,
  AnimatedDiv,
  StyledIcon,
} from './styled';
import type { SearchResultsProps } from './types';

const SearchResults: FC<SearchResultsProps> = ({
  results,
  input,
}): JSX.Element => {
  const intl = useIntl();
  const ref = useRef<HTMLDivElement | null>(null);
  const { isMobile } = useDeviceDetect();
  const {
    state: { y, topEdge, bottomEdge },
  } = useScrollPosition(ref);

  const filteredResults = useMemo(
    () => results.filter((r) => r.lang === intl.locale),
    [intl.locale, results],
  );
  const { height } = useSpring({
    to: [
      {
        height:
          filteredResults.length === 0
            ? 80
            : Math.min(filteredResults.length * 80 + 150, 1000),
      },
    ],
    from: { height: 0 },
    config: { duration: 300, tension: 2000, friction: 1000, precision: 1 },
  });

  const { top } = useSpring({
    loop: true,
    from: { top: 0 },
    to: { top: 20 },
    config: { duration: 1000 },
  });

  const { bottom } = useSpring({
    loop: true,
    from: { bottom: 0 },
    to: { bottom: 20 },
    config: { duration: 1000 },
  });

  const isScrolled = y > 0;
  useEffect(() => {
    isScrolled && isMobile && input.current?.blur();
  }, [input, isMobile, isScrolled]);

  return useMemo(() => {
    return (
      <Wrapper ref={ref} style={{ height }}>
        {filteredResults.length >= 3 && (
          <AnimatedDiv style={{ top }} show={topEdge} position="top">
            <StyledIcon
              rotate={false}
              name="arrow-right"
              width={20}
              height={20}
            />
          </AnimatedDiv>
        )}
        {filteredResults.length ? (
          <SearchList>
            <h2>
              {filteredResults.length}{' '}
              <FormattedPlural
                value={filteredResults.length}
                one={intl.formatMessage({ id: 'searchResults.singular.one' })}
                other={intl.formatMessage({
                  id: 'searchResults.plural.other',
                })}
                {...(intl.locale === 'pl' && {
                  few: intl.formatMessage({ id: 'searchResults.plural.few' }),
                  many: intl.formatMessage({
                    id: 'searchResults.plural.many',
                  }),
                })}
              />{' '}
              <FormattedPlural
                value={filteredResults.length}
                one={intl.formatMessage({ id: 'matchedQuery.singular.one' })}
                other={intl.formatMessage({
                  id: 'matchedQuery.plural.other',
                })}
                {...(intl.locale === 'pl' && {
                  few: intl.formatMessage({ id: 'matchedQuery.plural.few' }),
                  many: intl.formatMessage({
                    id: 'matchedQuery.plural.many',
                  }),
                })}
              />
            </h2>
            <ul>
              {filteredResults.map((result) => (
                <li key={result.slug}>
                  <Link to={`${result.slug}`}>
                    <Title>{result.title}</Title>
                    <Description>{result.excerpt}</Description>
                  </Link>
                </li>
              ))}
            </ul>
          </SearchList>
        ) : (
          <p>{intl.formatMessage({ id: 'searchNotFound' })}</p>
        )}
        {filteredResults.length >= 3 && (
          <AnimatedDiv style={{ bottom }} show={bottomEdge} position="bottom">
            <StyledIcon
              rotate={true}
              name="arrow-right"
              width={20}
              height={20}
            />
          </AnimatedDiv>
        )}
      </Wrapper>
    );
  }, [height, filteredResults, top, topEdge, intl, bottom, bottomEdge]);
};

export default SearchResults;
