import loadable from '@loadable/component';
import Img, { FluidObject } from 'gatsby-image';
import styled from 'styled-components';

const Sticky = loadable(
  () => import(/* webpackChunkName: "Sticky" */ 'react-stickynode'),
);

import PageStats from '@common/PageStats';
import { StatsWrapper } from '@common/PageStats/styled';
import TimeToRead from '@common/TimeToRead';
import { media } from '@utils/media';
import { rhythm, styledScale } from '@utils/typography';

export const Wrapper = styled.article<{ marginTop: boolean }>`
  position: relative;
  display: flex;
  flex-direction: column;
  background-color: white;
  padding: 14px 70px;
  margin-top: ${(props) => (props.marginTop ? '-7rem' : '0')};
  ${media.md`
      margin-top: 0;
  `}
  ${media.sm`
      padding: 14px 40px; 
  `}
  ${media.xxs`
      padding: 14px 0px; 
  `}
`;

export const Title = styled.h1`
  margin: ${rhythm(0.3)} 0;
  width: 100%;
`;

export const Description = styled.div`
  ${styledScale(-1 / 5)};
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  margin-bottom: ${rhythm(1)};
  font-weight: 900;
  align-items: center;
  font-family: 'Montserrat';
  & > div {
    margin-right: 20px;
    font-size: 10px;
    width: max-content;
  }
`;

export const Date = styled.div`
  background-color: #47426f;
  color: #fff;
  border-radius: 7px;
  padding: 5px 10px;
  order: 0;
  ${media.md`
    order: 1;
  `}
`;

export const StyledTimeToRead = styled(TimeToRead)`
  background-color: #47426f;
  color: #fff;
  border-radius: 7px;
  padding: 5px 10px;
  flex-grow: 0;
  order: 0;
  & svg {
    margin-bottom: 4px;
  }
  ${media.md`
    order: 1;
  `}
`;

export const Line = styled.hr`
  margin-bottom: ${rhythm(1)};
`;

export const StyledImg = styled(Img)<{ fluid: FluidObject }>`
  position: absolute;
  top: 0;
  left: 0;
`;

export const ImageWrapper = styled.div`
  overflow: hidden;
  width: 100vw;
  position: relative;
  left: 50%;
  margin-left: -50vw;
  max-height: 500px;
  ${media.md`
      height: auto;
      width: 100%;
      margin: 0;
      left: 0;
  `}
`;

export const StyledPageStats = styled(PageStats)`
  display: flex;
  height: 70px;
  ${StatsWrapper} {
    display: flex;
    position: initial;
  }
  ${media.md`
      height: 60px;
  `}
  ${media.xxs`
      position: absolute;
      right: 0;
      top: -60px;
  `}
`;

export const Container = styled.div`
  display: flex;
  flex-direction: row;
`;

export const ParallaxObserver = styled.div<{
  height: number | null | undefined;
}>`
  position: absolute;
  top: 0;
  height: ${(props) => `${props.height ? props.height : '20'}px`};
  width: calc(100% - 20px);
  z-index: -1;
`;

export const StyledSticky = styled(Sticky)`
  ${media.xxs`
      height: 60px;
`}
`;
