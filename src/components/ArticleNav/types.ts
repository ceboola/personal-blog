import type { SitePageContext } from '@typings/graphql-types';

export interface Props {
  pageContext: SitePageContext;
}
export interface IArticleNavProps {
  title: string;
  desc: string;
  exc: string;
  link: string;
}
