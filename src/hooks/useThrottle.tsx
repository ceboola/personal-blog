import { useEffect, useMemo } from 'react';

import throttle from 'lodash.throttle';

interface DebouncedFunc<T extends (...args: number[]) => void> {
  (...args: Parameters<T>): ReturnType<T> | undefined;
  cancel(): void;
  flush(): ReturnType<T> | undefined;
}

export const useThrottle = <T extends (...args: number[]) => void>(
  fn: T,
  ms = 75,
): DebouncedFunc<T> => {
  const throttledFn = useMemo(() => {
    return throttle(fn, ms);
  }, [fn, ms]);
  // This is must because fn can call setState.
  useEffect(() => {
    return () => {
      throttledFn.cancel();
    };
  }, [throttledFn]);
  return throttledFn;
};
