module.exports = {
  '*.{js,jsx,ts,tsx}': [
    'eslint --ignore-path .gitignore --fix',
    'prettier --write',
  ],
  '*.+(json|yml|yaml|css|less|scss|md|graphql|mdx)': ['prettier --write'],
  '*.{png,jpeg,jpg,gif,svg}': ['imagemin-lint-staged', 'git add'],
};
