// returns a new object with the values at each key mapped using mapFn(value)
export const mapObject = (
  obj: Record<string, unknown>,
  fn: (v: unknown, k: string, i: number) => void,
): Record<string, unknown> =>
  Object.fromEntries(Object.entries(obj).map(([k, v], i) => [k, fn(v, k, i)]));

export default mapObject;
