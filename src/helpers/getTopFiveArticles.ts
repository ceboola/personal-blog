interface Props {
  allPosts: AllMarkdownRemarkEdge[];
  popularPosts: {
    edges: Array<{
      node: {
        id: string;
        totalCount: number;
      };
    }>;
  };
}

export const getTopFiveArticles = (
  allPosts: Props['allPosts'],
  popularPosts: Props['popularPosts'],
): Top5Markdown[] => {
  const results = [];
  for (const a of popularPosts.edges) {
    const popularPost = allPosts.find((b) => b.node.fields.slug === a.node.id);
    if (popularPost == undefined) {
      continue;
    } else {
      results.push({
        pageViews: a.node.totalCount,
        ...popularPost.node,
      });
    }
    if (results.length >= 5) {
      break;
    }
  }
  return results;
};
