import { SitePageContext } from '@typings/graphql-types';

import { animations } from './animations';
import { parsePathname } from './parsePathname';

interface getAnimationsProps {
  previousPathname: string;
  pathname: string;
  pageContext: SitePageContext;
}

interface returnPropsType {
  [key: string]: unknown;
}

const excludeRoutes = new RegExp(
  '/(?:dev-404-page|login|dashboard|contact|about|tags|404)/',
);
export const getAnimations = ({
  previousPathname,
  pathname,
  pageContext,
}: getAnimationsProps): returnPropsType => {
  const { lang, clearSlug, previousPl, previousEn } = pageContext;

  const [previousSlashes] = parsePathname(previousPathname);
  const [currentSlashes] = parsePathname(pathname);

  // if (previousSlashes < currentSlashes) return animations.slideUp;

  // if (previousSlashes > currentSlashes) return animations.slideUp;

  if (previousSlashes === currentSlashes) {
    if (lang === 'pl' && previousPl?.node?.fields?.slug === previousPathname) {
      return animations.slideLeft;
    }
    if (lang === 'en' && previousEn?.node?.fields?.slug === previousPathname) {
      return animations.slideLeft;
    }
    if (!clearSlug?.search(excludeRoutes)) {
      return animations.quick;
    }
    return animations.slideRight;
  }

  return animations.quick;
};
