import { PageRendererProps } from 'gatsby';

export interface Props extends PageRendererProps {
  title: string;
}
