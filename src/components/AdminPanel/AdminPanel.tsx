import { memo, useContext, useEffect, useState } from 'react';

import { PostsContext } from '@providers/PostsProvider';
import { getCurrentUser } from '@utils/auth';

import { Wrapper, Form } from './styled';

const AdminPanel = (): JSX.Element => {
  const {
    userStore: {
      firebase: { dbData },
    },
  } = useContext(PostsContext);
  const {
    stsTokenManager: { accessToken },
  } = getCurrentUser();
  const [emailsDb, setEmailsDb] = useState<string[]>([]);
  const [author, setAuthor] = useState('');
  const [authorEmail, setAuthorEmail] = useState('');
  const [subject, setSubject] = useState('');
  const [htmlContent, setHtmlContent] = useState('');

  const sendEmail = (): void => {
    fetch('https://api.codesigh.com/newsletter', {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${accessToken}`,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        author: author,
        authorEmail: authorEmail,
        subject: subject,
        to: emailsDb,
        html: htmlContent,
      }),
    })
      .then((res) => res.json())
      .then((res) => {
        console.log('here is the response: ', res);
      })
      .catch((err) => {
        console.error('here is the error: ', err);
      });
  };

  useEffect(() => {
    const fetchData = async (): Promise<void> => {
      const events: string[] = [];
      try {
        await dbData
          ?.collection('newsletterData')
          .get()
          .then((querySnapshot) => {
            querySnapshot.forEach((doc) => {
              events.push(doc.id);
            });
          });
      } catch (err) {
        console.error(err);
      }
      setEmailsDb(events);
    };

    fetchData();
    return;
  }, [dbData]);

  return (
    <Wrapper>
      <h1>newsletter</h1>
      <Form>
        <input
          placeholder="Autor"
          onChange={(event) => setAuthor(event.target.value)}
        />
        <input
          placeholder="Subject"
          onChange={(event) => setSubject(event.target.value)}
        />
        <input
          placeholder="Autor email"
          onChange={(event) => setAuthorEmail(event.target.value)}
        />
        <textarea
          placeholder="HTML content"
          cols={40}
          rows={5}
          onChange={(event) => setHtmlContent(event.target.value)}
        />
        <button
          onClick={(e) => {
            e.preventDefault();
            sendEmail();
          }}
        >
          submit
        </button>
      </Form>
    </Wrapper>
  );
};

export default memo(AdminPanel);
