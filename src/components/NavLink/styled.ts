import { Link } from 'gatsby';
import styled from 'styled-components';

export const StyledLink = styled(Link)`
  box-shadow: none;
`;
