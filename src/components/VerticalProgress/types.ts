export interface VerticalProgressProps {
  progress: number | null;
  name: string;
  iconName: string;
}
