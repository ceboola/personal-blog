import styled from 'styled-components';

import CommonButton from '@common/CommonButton';
import { media } from '@utils/media';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  margin: 70px auto 40px auto;
  text-align: center;
  background-color: #17173f;
  color: #fff;
  border-radius: 9px;
  border-top: 10px solid #00c899;
  border-bottom: 10px solid #00c899;
  width: max-content;
  height: max-content;
  padding: 28px;
  box-shadow: 1px 1px 35px 10px rgb(25 31 53);
  ${media.xxs`
      width: 320px;
      box-shadow: 0px 0px 15px 10px rgb(25 31 53);
  `}
`;

export const Form = styled.form`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  input[type='text'],
  input[type='password'] {
    font-size: 13px;
    font-weight: 900;
    display: block;
    margin: 5px auto;
    background: #17173f;
    border: 3px outset #00c899;
    border-radius: 5px;
    padding: 3px 5px;
    width: 280px;
    outline: none;
    color: #fff;
    &::placeholder {
      font-weight: 900;
      font-size: 14px;
      vertical-align: middle;
      color: unset;
    }
  }
`;

export const Button = styled(CommonButton)`
  margin-top: 25px;
  background-color: #00c899;
  width: 200px;
`;

export const Title = styled.div`
  color: #fff;
  display: flex;
  flex-direction: row;
  background-image: linear-gradient(#00c899, #00c899);
  background-size: 100% 3px;
  background-position: 0% 100%;
  background-repeat: no-repeat;
  margin: 10px auto;
  p {
    margin: 0;
  }
  p:first-of-type {
    color: #00c899;
    font-weight: 900;
  }
`;
