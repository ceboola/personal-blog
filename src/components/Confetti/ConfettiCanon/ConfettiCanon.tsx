import React from 'react';

import ConfettiDot from '@components/Confetti/ConfettiDot';
import { randomInRange, randomIntInRange } from '@helpers/randoms';

import { ConfettiCanonProps } from './types';

const ConfettiCannon = ({
  anchorRef,
  colors,
  dotCount,
  initialHorizontal,
  initialUpwards,
  size,
}: ConfettiCanonProps): JSX.Element => (
  <>
    {/* 0 as a placeholder instead of the implicit undefined */}
    {new Array(dotCount).fill(0).map((_, index) => (
      <ConfettiDot
        key={index}
        anchorRef={anchorRef}
        color={colors[randomIntInRange(0, colors.length)]}
        initialHorizontal={randomInRange(
          initialHorizontal.min,
          initialHorizontal.max,
        )}
        initialUpwards={randomInRange(initialUpwards.min, initialUpwards.max)}
        rotate={randomInRange(0, 360)}
        size={randomInRange(size.min, size.max)}
      />
    ))}
  </>
);

export default ConfettiCannon;
