import { memo } from 'react';

import { StyledLink } from './styled';
import { Props } from './types';

export const NavLink: React.FC<Props> = ({ children, to }) => {
  const activeStyles = {
    color: '#128723',
  };
  return to === '/' ? (
    <StyledLink
      itemProp="url"
      to={to}
      partiallyActive={false}
      activeStyle={activeStyles}
    >
      {children}
    </StyledLink>
  ) : to === '/pl/' ? (
    <StyledLink
      itemProp="url"
      to={to}
      partiallyActive={false}
      activeStyle={activeStyles}
    >
      {children}
    </StyledLink>
  ) : (
    <StyledLink
      to={to}
      itemProp="url"
      getProps={({ isPartiallyCurrent }) => ({
        style: {
          color: isPartiallyCurrent && '#128723',
        },
      })}
    >
      {children}
    </StyledLink>
  );
};

export default memo(NavLink);
