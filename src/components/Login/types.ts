export interface UiConfigProps {
  GoogleAuthProvider: {
    PROVIDER_ID: string;
  };
  EmailAuthProvider: {
    PROVIDER_ID: string;
  };
}

export interface LoginProps {
  path: string;
}
