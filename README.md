Howdy!!

<p align="center">
  <a href="https://www.gatsbyjs.org">
    <img alt="Gatsby" src="https://www.gatsbyjs.org/monogram.svg" width="60" />
  </a>
</p>

## Features

- i18n (Polish & English)
- Gatsby-Image (Responsive images)
- PrismJS for blog posts
- Fitting text to current size of container for PageStats
- Tags & Categories
- Comments & Google PageViews & TimeToRead
- Suggestions for further reading below blog post
- Husky + Lint-Staged
- SEO
- PWA
- Search blog posts
- Fully automated newsletter (handled on backend too) + unsubscribing
- LogIn/SignIn feature with admin panel
- Extensive website statistics
- MessageHub notification in app

## Tech Stack

- Hosted on private VPS with autodeploy to Gitlab CI/CD from Kubernetes/K3S pods
- styled-components for styling
- ESLint & Prettier for linting
- Typescript
- Jest for testing
- Lighthouse results on Gitlab CI/CD
- Custom Code Splitting into chunks
- Renovate for auto update dependencies
