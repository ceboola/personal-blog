import styled from 'styled-components';

import CommonButton from '@common/CommonButton';
import CommonIcon from '@common/CommonIcon';
import { media } from '@utils/media';
import { rhythm } from '@utils/typography';

export const Wrapper = styled.div`
  background-color: #17173f;
  max-width: 100vw;
  width: 100vw;
  position: relative;
  left: 50%;
  right: 50%;
  margin: 40px -50vw;
  ${media.lg`
    margin: 40px -10px;
    left: 0;
    right: 0;
    padding: 0 10px;
  `}
`;

export const Content = styled.div`
  position: relative;
  margin-left: auto;
  margin-right: auto;
  max-width: ${rhythm(34)};
  padding: ${`${rhythm(2)} 0`};
  display: flex;
  flex-direction: column;
  ${media.lg`
    padding: ${`${rhythm(2)} 10px`};
  `}
`;

export const Description = styled.div`
  margin: 0;
  font-size: ${`${rhythm(1)}`};
  font-weight: 600;
  color: white;
`;

export const Title = styled.h2`
  font-size: ${`${rhythm(0.7)}`};
  margin: 10px 0;
  padding: 0;
  color: white;
  font-weight: 400;
`;

export const CTATitle = styled.h3`
  font-size: ${`${rhythm(0.4)}`};
  margin: 10px 0;
  padding: 0;
  color: white;
  font-weight: 400;
`;

export const StyledButton = styled(CommonButton)`
  padding: 10px 70px;
`;

export const StyledIcon = styled(CommonIcon)`
  position: absolute;
  top: -25px;
  left: -40px;
  ${media.xl`
    left: -10px;
  `}
  ${media.lg`
    left: 10px;
  `}
`;

export const List = styled.ul`
  list-style: none;
`;
