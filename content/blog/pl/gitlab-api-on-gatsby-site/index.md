---
lang: pl
title: Gitlab API na stronie Gatsby
date: '2020-06-11T13:46:41.169Z'
tags: ['gitlab', 'api', 'gatsby']
featuredImage: ../../images/gitlab-api-on-gatsby-site/gitlabgatsby.jpeg
category: 'Front-end'
---

Witajcie! W tym artykule pokaże wam w jaki sposób możemy połączyć się z Gitlab API i pobrać z niego interesujące nas dane jednocześnie podpinając je pod stronę postawioną na Gatsbym.

## Gitlab API + Gatsby

Jest wiele możliwości dostania nas interesujących danych z Gitlab API, od prostego GET/POST bezpośrednio z przeglądarki do bibliotek które same w sobie załatwiają tę sprawę, powiedzmy, że chcemy wykorzystać korzyści płynące z Gatsbiego i jego statycznego generowania stron (SSG). W tym przykładzie będe korzystał z biblioteki [Gitbreaker](https://github.com/jdalrymple/gitbeaker), zacznijmy od jej zainstalowania

```bash
yarn add @gitbeaker/node
```

### gatsby-node

Gatsby-node to plik w którym możemy skorzystać z wielu dobrodziejstw które wystawia nam bezpośrednio Gatsby za pomocą swojego API, daje Ci możliwość wykonania kodu raz podczas fazy jego budowania. Pełna lista wspieranych API w tym pliku znajduje się pod adresem [dokumentacji](https://www.gatsbyjs.com/docs/reference/config-files/gatsby-node/). Dzisiaj skupimy się na trzech jej funkcjach, mianowicie <b>sourceNode</b>, <b>createSchemaCustomization</b> i <b>createResolver</b>, zacznijmy jednak od pierwszej metody, a oto jak opisana jest w oficjalnej dokumentacji.

#### sourceNodes

> Punkt rozszerzenia przekazujący wtyczki węzłom źródłowym. To API jest wywoływane podczas sekwencji ładowania początkowego Gatsby. Wtyczki źródłowe używają tego haka do tworzenia węzłów. Ten interfejs API jest wywoływany dokładnie raz dla każdej wtyczki (i raz dla pliku gatsby-config.js w Twojej witrynie). Jeśli zdefiniujesz ten punkt zaczepienia w gatsby-node.js, zostanie on wywołany dokładnie raz po tym, jak wszystkie wtyczki źródłowe zakończą tworzenie węzłów.

OK! po krótkiej prezentacji na czym dzisiaj będziemy pracować, zabierzmy się do pracy. Po pierwsze zaimportujmy bibliotekę z której będziemy korzystać w pliku gatsby-node.js

```javascript
const { Gitlab } = require('@gitbeaker/node');
```

"Dlaczego używasz importu z ES5?" możesz zapytać, są pewnie ograniczenia po stronie Node, jeżeli chcesz dowiedzieć się więcej na ten temat lub bezpośrednio korzystać z przywilejów ES6 w tym pliku to zapraszam do [lektory](https://github.com/gatsbyjs/gatsby/issues/7810)

Po zaimportowaniu paczki, stwórzmy zapytanie do Gitlabowego API, powiedzmy, że chcemy pobrać informację o naszych personalnych projektach.

```javascript
exports.sourceNodes = async ({
  actions,
  createContentDigest,
  createNodeId,
}) => {
  const { createNode } = actions;
  //gitlab
  const GITLAB_NODE_TYPE = `Gitlab`;
  const api = new Gitlab({
    token: process.env.GITLAB_TOKEN,
  });
  const user = await api.Users.current();
  const projects = await api.Users.projects(user.id, { statistics: true });
};
```

W skrócie co tutaj robimy:

- tworzymy typ node dla naszej schemy graphql
- łączymy się z api za pomocą naszych danych uwierzetylnienia (gitlab token możemy uzyskać na stronie gitlab.com w Preferences > Access Tokens)
- używamy naszego id usera z Users API do połączenia się bezpośrednio z kolejnym węzłem projektów, przekazujemy tutaj nasze <b>user.id</b> i parametr statystyk ustawiony na <b>true</b> żeby dostać jeszcze więcej interesujących nas statystyk np. ilość commitów w projekcie, jego rozmiar itp.

Jeżeli nie jesteś zaznajomiony ze zmiennymi process.env powinieneś zajrzeć tutaj [dotenv](https://www.npmjs.com/package/dotenv) w skrócie są to zmienne których nie chcemy nikomu pokazać.

### Zapisz dane do GraphQL

Po połączeniu się z Gitlab API powinniśmy móc odczytać z niego dane, powiedzmy, że chcemy zwrócić nasze projekty:

```javascript
exports.sourceNodes = async ({
  actions,
  createContentDigest,
  createNodeId,
}) => {
  const { createNode } = actions;
  //gitlab
  const GITLAB_NODE_TYPE = `Gitlab`;
  const api = new Gitlab({
    token: process.env.GITLAB_TOKEN,
  });
  const user = await api.Users.current();
  const projects = await api.Users.projects(user.id, { statistics: true });

  await Promise.all(
    projects.map(async (project) => {
      createNode({
        ...project,
        id: createNodeId(`${GITLAB_NODE_TYPE}-${project.id}`),
        parent: null,
        children: [],
        internal: {
          type: GITLAB_NODE_TYPE,
          content: JSON.stringify(project),
          contentDigest: createContentDigest(project),
        },
      });

};
```

Dla każdego projektu mapujemy i uzyskujemy interesujące nas dane, zachowujemy te dane w naszym typie <b>Gitlab</b>. Teraz po wystartowaniu naszego projektu powinniśmy uzyskać te dane w panelu GraphiQL!

![GraphQL data](../../images/gitlab-api-on-gatsby-site/graphql.png)

## Ekstra

Ale czekaj! Dodajmy w takim razie niestandardowe pole ponieważ nie dostajemy teraz takich informacji jak użyte języki programowania bezpośrednio z naszego endpointu API, te pole znajduje się w innym endpoincie więc je dodajmy!

```javascript
exports.sourceNodes = async ({
  actions,
  createContentDigest,
  createNodeId,
}) => {
  const { createNode } = actions;
  //gitlab
  const GITLAB_NODE_TYPE = `Gitlab`;
  const api = new Gitlab({
    token: process.env.GITLAB_TOKEN,
  });
  const user = await api.Users.current();
  const projects = await api.Users.projects(user.id, { statistics: true });

  await Promise.all(
    projects.map(async (project) => {
      createNode({
        ...project,
        usedLanguages: await api.Projects.languages(project.id),
        id: createNodeId(`${GITLAB_NODE_TYPE}-${project.id}`),
        parent: null,
        children: [],
        internal: {
          type: GITLAB_NODE_TYPE,
          content: JSON.stringify(project),
          contentDigest: createContentDigest(project),
        },
      });

};
```

Jaka jest różnica? Dodaliśmy nowe pole nazwane <b>usedLanguages</b>. Pobieramy dla każdego <b>project.id</b> nasze języki programowania, teraz mamy dostęp do języków programowania jakie użyliśmy dla każdego projektu z osobna!

![GraphQL languages](../../images/gitlab-api-on-gatsby-site/graphql2.png)

Fajne prawda? Ale jesteśmy niezadowoleni i chcielibyśmy dostać te wyniki zsumowane tak aby zwróciło nam dane wszystkich projektów jako jeden obiekt. Zacznijmy od rozrysowania problemu na kartce i spróbujmy rozwiązać problem matematyczny.

![Mathematics problem](../../images/gitlab-api-on-gatsby-site/problem.jpg)

Problem jest taki, że gdy dodajemy wszystkie języki tego samego typu dostajemy niewspółmierny wynik, dla przykładu JS: 20%, 50%, 50%, 20%, 0%, 10%, jak widzisz jest to grubo powyżej zakładanych 100%, pierwsza myśl rozwiązania problemu:

- zsumować wszystie liczby i podzielić je przez ich ilość (150/6=25%), to był zły pomysł, nawet jeżeli na pierwszy rzut oka liczba wydaje się prawdziwa w póżniejszym etapie wyjdzie, że jednak nam gdzieś uciekają te procenty

####Drugi pomysł

![Mathematics problem](../../images/gitlab-api-on-gatsby-site/problem2.jpg)

Najpierw podzielić każdą liczbę przez 10, zsumować je, a potem podzielić każdą z osobna przez sumę zsumowanych liczb, więc w naszym przykładzie:

- 2 + 5 + 5 + 2 + 1 = 15
- każda wartość podzielona przez 15
- 0.13 + 0.33 + 0.33 + 0.13 + 0.06
- teraz każda wartość wymnożona przez 100
- 13 + 33 + 33 + 13 + 6 ~ 100%

Teraz wygląda obiecująco! Spróbujmy to w takim razie zaimplementować :)

Najpierw musimy stworzyć typy w naszej schemie, skorzystajmy z nowej metody API zwanej <b>createSchemaCustomization</b>

> Dostosuj schemat GraphQL Gatsby, tworząc definicje typów, rozszerzenia pól lub dodając schematy innych firm.

```javascript
exports.createSchemaCustomization = ({ actions }) => {
  const { createTypes } = actions;
  const typeDefs = `
  type SummarizedStats @nodeInterface {
    programmingLanguages: GitlabUsedLanguages
  }
  `;
  createTypes(typeDefs);
};
```

Więc tutaj stworzyliśmy typ <b>SummarizedStats</b> dla naszej schemy i w środku niej zadeklarowaliśmy pole <b>programmingLanguages</b> gdzie będziemy zapisywać nasze zsumowane wyniki. W takim razie dostarczmy naszech schemie jakieś dane dla <b>programmingLanguages</b>!

### Z papieru na kod

Po pierwsze stwórzmy sobie helper który będzie nam zaokrąglał nasze wyniki:

```javascript
const round = (number, places) => {
  return +(Math.round(number + 'e+' + places) + 'e-' + places);
};
```

Teraz musimy naszą scheme wypełnić prawdziwymi danymi. Jak możemy to zrobić? Z pomocą przychodzi nam kolejna metoda z gatsbiowego API nazwana <b>createResolver</b>

> Dodaje niestandardowe narzędzia do rozpoznawania pól do schematu GraphQL.
> Umożliwia dodawanie nowych pól do typów przez dostarczanie konfiguracji pól lub dodawanie funkcji resolver do istniejących pól.

#### HoF 🔥🔥

```javascript
exports.createResolvers = async ({ createResolvers }) => {
  createResolvers({
    Query: {
      SummarizedStats: {
        type: 'SummarizedStats',
        resolve(source, args, context) {
          const round = (number, places) => {
            return +(Math.round(number + 'e+' + places) + 'e-' + places);
          };
          const getAllGitlab = context.nodeModel.getAllNodes({
            type: 'Gitlab',
          });
          const mapField = (resource, field) =>
            resource.map((val) => val[field]);

          const allLang = mapField(getAllGitlab, 'usedLanguages');
          const programmingLanguages = allLang.reduce(
            (acc, val) => {
              for (const key in acc) {
                const checkValue = val[key] ?? 0;
                acc[key] = round(
                  acc[key] +
                    checkValue /
                      allLang.filter((val) => {
                        return val[key];
                      }).length,
                  2,
                );
              }
              return acc;
            },
            {
              CSS: 0,
              Dockerfile: 0,
              HTML: 0,
              JavaScript: 0,
              Pug: 0,
              Shell: 0,
              TypeScript: 0,
            },
          );
          for (const property in programmingLanguages) {
            programmingLanguages[property] =
              programmingLanguages[property] / 10;
          }
          const sumValues = Object.values(programmingLanguages).reduce(
            (a, b) => a + b,
          );
          const sumProgrammingLanguages = Object.fromEntries(
            Object.entries(programmingLanguages).map(([k, v]) => [
              k,
              round((v / sumValues) * 100, 2),
            ]),
          );

          return {
            programmingLanguages: sumProgrammingLanguages,
          };
        },
      },
    },
  });
};
```

Więęęc co tutaj robimy?

- Tworzymy pole dla naszego <b>SummarizedStats</b> z typu <b>Gitlab</b> ze wszystkimi jej własnościami (context.nodeModel.getAllNodes)
- Potem mapujemy je by uzyskać wszystkie <b>usedLanguages</b> (nawet 0 czy nulle)
- Następnie używamy reduce by zebrać wszystkie poprawne rezultaty które nie zawierają zero czy null, potem dzielimy je przez uzyskaną ilość poprawnych wartości (zero/null się nie liczą) i musisz pamiętać o przekazaniu początkowej wartości dla obiektu, w przeciwnym razie dostaniesz w niektórych przypadkach wartość <b>NaN</b>
- Po tym jak uzyskasz zsumowany obiekt wartości wszystkich języków z każdego projektu <b>podziel</b> go przez 10
- Na końcu przemnóż go przez 100 żeby otrzymać poprawne wartości procentowe i zaokrąglij go do dwóch miejsc po przecinku, następnie zwróć go do naszego pola <b>programmingLanguages</b>

Po wszystkim jeżeli wszystko poszło zgodnie z planem zobaczysz swoje podsumowane wyniki na <b>GraphiQL</b>

#### Voila!

![Voila](../../images/gitlab-api-on-gatsby-site/voila.png)

Teraz zależy od Ciebie co zrobisz z tymi danymi! Użyj swojej wyobraźni i spróbuj czegoś kreatywnego, pomyśl jak ciekawie można przedstawić takie dane. Jeżeli nie masz pomysłu to w najbliższym czasie będe pokazywał jak można je w prosty sposób pokazać na wykresie słupkowym. Trzymajcie się!
