import { animated } from 'react-spring';
import styled from 'styled-components';

export const StyledConfettiDot = styled.svg`
  height: 10px;
  pointer-events: none;
  position: absolute;
  width: 10px;
  will-change: transform;
  overflow: hidden;
  z-index: 1;
`;

export const AnimatedConfettiDot = animated(StyledConfettiDot);
