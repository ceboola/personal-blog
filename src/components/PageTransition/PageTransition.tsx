import { useRef, useEffect, FC } from 'react';

import { animated, useTransition } from 'react-spring';

import { getAnimations } from '@helpers/getAnimations';

import { PageTransitionProps } from './types';

const PageTransition: FC<PageTransitionProps> = ({ children, props }) => {
  const { location, pageContext } = props;
  const { pathname } = location;
  const previousPathname = useRef('');

  const animation = getAnimations({
    pathname,
    previousPathname: previousPathname.current,
    pageContext,
  });
  const transitions = useTransition(pathname, animation);
  useEffect(() => {
    previousPathname.current = pathname;
  }, [pathname]);
  const conditions = [
    '/',
    '/stats/',
    '/tags/',
    '/contact/',
    '/about/',
    '/dashboard/',
    '/dashboard/user/',
    '/404/',
    '/pl/',
    '/pl/stats/',
    '/pl/tags/',
    '/pl/contact/',
    '/pl/about/',
    '/pl/dashboard/',
    '/pl/dashboard/user/',
    '/pl/404/',
  ];
  const checkPagesWithoutTransition = conditions.indexOf(pathname);
  if (checkPagesWithoutTransition >= 0) return <>{children}</>;

  return transitions((style, item) => {
    return item && <animated.div style={style}>{children}</animated.div>;
  });
};

export default PageTransition;
