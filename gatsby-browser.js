/* eslint-disable import/no-duplicates */
/* eslint-disable prettier/prettier */
import loadable from '@loadable/component';
import { IntlProvider } from 'gatsby-plugin-react-intl';
import 'firebase/auth';
import 'firebase/firestore';
// custom typeface's
require('typeface-ubuntu');
require('typeface-montserrat');
import { ThemeProvider } from 'styled-components';

import Layout from '@components/Layout';
import PageTransition from '@components/PageTransition';
import PostsProvider from '@providers/PostsProvider';

import 'prismjs/themes/prism-tomorrow.css';

const IntersectionObserver = loadable(() =>
  import(
    /* webpackChunkName: "IntersectionObserver" */
    'intersection-observer'
  ),
);

export const onClientEntry = () => {
  // IntersectionObserver polyfill for gatsby-background-image/gatsby-image (Safari, IE)
  if (!(`IntersectionObserver` in window)) {
    IntersectionObserver;
    console.log(`# IntersectionObserver is polyfilled!`);
  }
  window.addEventListener('load', () => {
    document.body.className = document.body.className.replace(/\bno-js\b/, '');
  });
};

export const wrapPageElement = ({ element, props }) => {
  const { locale, messages } = element.props;
  return (
    <IntlProvider locale={locale} messages={messages}>
      <ThemeProvider theme={{}}>
        <PostsProvider
          posts={
            props.data.allMarkdownRemark && props.data.allMarkdownRemark.edges
          }
          data={props.data}
          pageContext={props.pageContext}
        >
          <PageTransition props={props}>
            <Layout>{element}</Layout>
          </PageTransition>
        </PostsProvider>
      </ThemeProvider>
    </IntlProvider>
  );
};
