import { SpringConfig } from 'react-spring';

export interface MessageHubProps {
  config?: SpringConfig;
  timeout?: number;
}
