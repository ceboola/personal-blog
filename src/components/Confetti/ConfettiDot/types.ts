export interface ConfettiProps {
  color: string;
  size: number;
}

export interface ConfettiDotProps {
  anchorRef: React.RefObject<HTMLElement>;
  color: string;
  initialHorizontal: number;
  initialUpwards: number;
  rotate: number;
  size: number;
}
