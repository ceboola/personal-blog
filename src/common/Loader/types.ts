export interface LoaderProps {
  width?: number;
  height?: number;
  strokeColor?: string;
}
