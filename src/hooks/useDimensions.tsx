import { useRef, useState, useEffect, MutableRefObject } from 'react';

import debounce from 'lodash.debounce';

import useIsomorphicLayoutEffect from '@hooks/useIsoMorphicLayoutEffect';
/**
 * A hook used to measure the height of the page.
 * Assign the return ref to a div and the height value will be stored in height.
 * @returns {Array} [{height, width}, ref]
 */
export function useDimensions(): readonly [
  { readonly height: number; readonly width: number },
  MutableRefObject<HTMLDivElement | null>,
] {
  const [height, setHeight] = useState(0);
  const [width, setWidth] = useState(0);

  // The following measures the size of the div and listens to changes
  const ref = useRef<HTMLDivElement | null>(null);
  const RESET_TIMEOUT = 300;

  const divDimensions = (): void => {
    if (null !== ref.current) {
      setHeight(ref.current.offsetHeight);
      setWidth(ref.current.offsetWidth);
    }
  };

  useIsomorphicLayoutEffect(() => {
    divDimensions();
  }, [divDimensions]);

  const debouncedDimensions = debounce(divDimensions, RESET_TIMEOUT);

  useEffect(() => {
    window.addEventListener('resize', debouncedDimensions, {
      capture: false,
      passive: true,
    });
    return () => {
      window.removeEventListener('resize', debouncedDimensions);
    };
  });

  return [{ height, width }, ref] as const;
}
