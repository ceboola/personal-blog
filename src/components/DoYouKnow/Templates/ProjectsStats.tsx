import { useContext } from 'react';

import { useIntl } from 'gatsby-plugin-react-intl';

import VerticalProgress from '@components/VerticalProgress';
import { PostsContext } from '@providers/PostsProvider';

import { Wrapper, Stats } from './styled';

export const ProjectsStats = (): JSX.Element => {
  const intl = useIntl();
  const {
    data: {
      SummarizedStats: { programmingLanguages },
    },
  } = useContext(PostsContext);

  return (
    <Wrapper>
      {intl.formatMessage({
        id: 'doYouKnow.projectsStats.description',
      })}
      <Stats>
        {Object.entries(programmingLanguages).map(([key, val], i) => (
          <VerticalProgress
            key={`${key}_${i}`}
            progress={val}
            name={key}
            iconName={key}
          />
        ))}
      </Stats>
    </Wrapper>
  );
};
