import { useContext, memo } from 'react';

import { useIntl } from 'gatsby-plugin-react-intl';
import kebabCase from 'lodash/kebabCase';

import { useToggle } from '@hooks/useToggle';
import { PostsContext } from '@providers/PostsProvider';

import {
  Wrapper,
  Title,
  SmallTitle,
  StyledLink,
  Container,
  Tag,
  StyledIcon,
  RevealButton,
  ArrowIcon,
  TagsWrapper,
} from './styled';
import { TagsCardProps } from './types';

const TagsList: React.FC<TagsCardProps> = ({
  tags,
  tag,
  totalPosts,
  slug,
  revealed,
}) => {
  const [isRevealed, toggleRevealed] = useToggle(revealed as boolean);
  const intl = useIntl();
  const {
    data,
    pageContext: { clearSlug },
  } = useContext(PostsContext);
  const checkTagsTemplate = /\/tags\//.test(clearSlug as string);
  const sortedTags = tags.sort(function (a, b) {
    return b.totalCount - a.totalCount;
  });

  const locale =
    data.markdownRemark && data.markdownRemark.frontmatter.lang !== 'en'
      ? `/${data.markdownRemark.frontmatter.lang}`
      : intl.locale !== 'en'
      ? `/${intl.locale}`
      : '';

  const checkLocaleForHomePage =
    intl.locale !== 'en' ? `/${intl.locale}/` : '/';
  const isHomeLocation = slug === checkLocaleForHomePage || slug;

  return (
    <Wrapper
      expand={isHomeLocation}
      reveal={isRevealed}
      otherView={checkTagsTemplate}
    >
      {isHomeLocation && (
        <RevealButton
          aria-label={!isRevealed ? 'open sidebar menu' : 'close sidebar menu'}
          onClick={() => toggleRevealed()}
          reveal={isRevealed}
        >
          <ArrowIcon name="arrow-right" rotate={isRevealed} />
        </RevealButton>
      )}
      <Title>{`${intl.formatMessage({ id: 'tags' })} (${tags.length})`}</Title>
      <TagsWrapper>
        {sortedTags.map((singleTag, index) => {
          const activeTag = tag?.includes(singleTag.fieldValue ?? '');
          let tagLink = '';
          if (
            typeof window !== `undefined` &&
            window.location.pathname === slug
          ) {
            tagLink = `${locale}/tags/${kebabCase(
              singleTag.fieldValue as string,
            )}/`;
          } else {
            tagLink = activeTag
              ? `${locale ? `${locale}/` : '/'}`
              : `${locale}/tags/${kebabCase(singleTag.fieldValue as string)}/`;
          }

          return (
            <Container key={`${singleTag.fieldValue}_${index}`}>
              <StyledLink isactive={activeTag} to={tagLink}>
                <StyledIcon
                  name="hash"
                  viewBox="0 0 24 24"
                  isactive={activeTag}
                />
                <Tag>{singleTag.fieldValue}</Tag> ({singleTag.totalCount})
              </StyledLink>
            </Container>
          );
        })}
      </TagsWrapper>
      <SmallTitle>{`${intl.formatMessage({
        id: 'articles',
      })} (${totalPosts})`}</SmallTitle>
    </Wrapper>
  );
};

export default memo(TagsList);
