import { useContext, memo, useMemo } from 'react';

import { Link } from 'gatsby';
import { useIntl } from 'gatsby-plugin-react-intl';

import Newsletter from '@components/Newsletter';
import SocialMedia from '@components/SocialMedia';
import { PostsContext } from '@providers/PostsProvider';

import {
  Wrapper,
  StyledBackground,
  InfoWrapper,
  StyledIcon,
  CenterContainer,
  BuildTime,
  PageViews,
  RightContainer,
} from './styled';

const Footer: React.FC = () => {
  const {
    pageContext: { lastBuildTime },
    data,
  } = useContext(PostsContext);
  const intl = useIntl();

  return useMemo(
    () => (
      <Wrapper>
        {' '}
        <StyledBackground name="wave" viewBox="0 0 24 4" />
        <InfoWrapper>
          <Newsletter />
          <CenterContainer>
            <div>{intl.formatMessage({ id: 'footer.built_with' })}</div>
            <div>
              <a
                title="gatsby link"
                target="_blank"
                rel="noreferrer"
                href="https://www.gatsbyjs.org"
              >
                <StyledIcon
                  name="gatsby"
                  viewBox="0 0 24 24"
                  width={20}
                  height={20}
                />
              </a>
              <a
                title="flaticon link"
                target="_blank"
                rel="noreferrer"
                href="https://www.flaticon.com"
              >
                <StyledIcon name="flaticon" height={20} />
              </a>
            </div>
            <div>2020-{new Date().getFullYear()}</div>
            <div>
              Gatsby
              <b>v{process.env.gatsby_version}</b>
            </div>
            <BuildTime>
              {intl.formatMessage({ id: 'last_update' })}:{' '}
              {lastBuildTime && lastBuildTime.buildTime}
            </BuildTime>
          </CenterContainer>
          <RightContainer>
            <SocialMedia />
            {data.currentPageViews && (
              <PageViews>
                <Link to="/stats/">
                  {intl.formatMessage(
                    {
                      id: 'footer.page_views_text',
                    },
                    {
                      visitSum: <b>{data.currentPageViews.totalCount}</b>,
                      sessionSum: <b>{data.currentPageViews.totalSessions}</b>,
                    },
                  )}
                </Link>
              </PageViews>
            )}
          </RightContainer>
        </InfoWrapper>
      </Wrapper>
    ),
    [data.currentPageViews, intl, lastBuildTime],
  );
};

export default memo(Footer);
