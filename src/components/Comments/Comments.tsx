import { useContext, useEffect, useRef, useState } from 'react';

import HyvorTalk from 'hyvor-talk-react';

import Loader from '@common/Loader';

import { PostsContext } from '@providers/PostsProvider';

import { StyledComments } from './styled';
import { Props } from './types';

const Comments = ({ websiteId, id }: Props): JSX.Element | null => {
  const { pageContext: slug } = useContext(PostsContext);
  const [render, setRender] = useState(false);

  useEffect(() => {
    setTimeout(() => {
      setRender(true);
    }, 1000);
  }, [slug]);
  return render ? <HyvorTalk.Embed websiteId={websiteId} id={id} /> : null;
};

const CommentsCount: React.FC<Props> = ({ websiteId, id, mode }) => {
  const refx = useRef<HTMLDivElement | null>(null);
  const [isFetched, setIsFetched] = useState(false);
  const [comments, setComments] = useState<string | null>('');
  // workaround for now while HyvorTalk not handling fetch spinner
  useEffect(() => {
    setTimeout(() => {
      if (refx.current) {
        setIsFetched(true);
        setComments(refx.current?.children[1].textContent);
      }
    }, 1500);
  });
  return (
    <div ref={refx}>
      {!isFetched ? <Loader /> : <div>{comments ? comments : 0}</div>}
      <StyledComments>
        <HyvorTalk.CommentCount websiteId={websiteId} id={id} mode={mode} />
      </StyledComments>
    </div>
  );
};

export { Comments, CommentsCount };
