interface Fluid {
  sizes: string;
  aspectRatio: number;
  width: number;
  height: number;
  src: string;
  srcSet: string;
  base64?: string;
  tracedSVG?: string;
  srcWebp?: string;
  srcSetWebp?: string;
  media?: string;
}

interface FluidObj {
  fluid: Fluid;
}

interface ChildImageSharp {
  childImageSharp: FluidObj;
}

export interface Props {
  activeTag?: string | string[];
}
