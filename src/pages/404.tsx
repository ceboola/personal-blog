import { graphql, PageRendererProps } from 'gatsby';

import Page404 from '@components/404';

const NotFoundPage: React.FC<PageRendererProps> = ({ location }) => {
  return <Page404 location={location} />;
};

export default NotFoundPage;

export const pageQuery = graphql`
  query($slug: String!) {
    site {
      siteMetadata {
        title
      }
    }
    SummarizedStats {
      ...SummarizedStats
    }
    gitlab(name: { eq: "personal-blog" }) {
      ...CurrentProject
    }
    currentPageViews: pageViews(path: { eq: $slug }) {
      ...SitePageViews
    }
  }
`;
