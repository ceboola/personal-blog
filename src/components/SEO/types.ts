export interface Props {
  description?: string;
  meta?: [];
  article?: QueryProps;
  title: string;
}
