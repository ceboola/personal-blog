import Image, { FixedObject } from 'gatsby-image';
import styled from 'styled-components';

import { rhythm } from '@utils/typography';

export const Container = styled.div`
  display: flex;
  margin: ${rhythm(2.5)} 0;
  justify-content: center;
`;

export const Description = styled.div`
  display: flex;
  align-items: center;
`;

export const StyledImage = styled(Image)<{ fixed: FixedObject }>`
  margin-right: ${rhythm(1 / 2)};
  margin-bottom: 0;
  min-width: 50px;
  border-radius: 100%;
`;
