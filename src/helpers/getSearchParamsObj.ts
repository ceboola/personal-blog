interface ParamsProps {
  email?: string;
}

export const getSearchParamsObj = (): ParamsProps => {
  const isBrowser = typeof window !== 'undefined';
  let o;
  if (isBrowser) {
    const a = window.location.search.split('&');
    o = a.reduce((o, v) => {
      const kv = v.split('=');
      kv[0] = kv[0].replace('?', '');
      o[kv[0]] = kv[1];
      return o;
    }, {} as Record<string, string>);
  }
  return o as ParamsProps;
};
