import { Link } from 'gatsby';
import styled, { css } from 'styled-components';

import CommonButton from '@common/CommonButton';
import CommonIcon from '@common/CommonIcon';
import { media } from '@utils/media';

export const Wrapper = styled.div<{
  expand: string | boolean | undefined | null;
  reveal: boolean | (() => void);
  otherView?: boolean;
}>`
  top: 5em;
  z-index: 2;
  max-width: 100%;
  position: fixed;
  display: flex;
  flex-direction: column;
  width: 160px;
  min-height: 0;
  background: #fff;
  padding: 14px;
  border: none;
  border-radius: 0.3rem;
  box-shadow: 0 1px 3px 0 #d4d4d5, 0 0 0 1px #d4d4d5;
  right: 5px;
  transition: 0.5s;
  transform: ${(props) =>
    props.expand ? 'translateX(100%)' : 'translateX(0%)'};
  ${(props) =>
    props.reveal &&
    css`
      transform: translateX(0%);
    `};
  ${(props) =>
    props.otherView &&
    css`
      margin-bottom: 28px;
      ${media.xl`
        position: relative;
        order: -1;
        top: 0;
        width: 100%;
        ${TagsWrapper} {
          flex-direction: row;
          flex: 0 1 100%;
          padding: 5px 0;
          flex-wrap: wrap;
        }
        ${Container} {
          margin-right: 20px;
        }
        ${SmallTitle} {
          display: none;
        }
      `}
      ${media.lg`
        margin: 0 8px 28px 8px;
        right: 0;
      `}
    `};
`;

export const Title = styled.h4`
  margin: 0 -14px 0 -14px;
  text-transform: capitalize;
  border-bottom: 1px solid rgba(34, 36, 38, 0.1);
  padding: 0 0 14px 14px;
`;

export const SmallTitle = styled.h5`
  margin: 0 -14px 0 -14px;
  text-transform: capitalize;
  border-top: 1px solid rgba(34, 36, 38, 0.1);
  padding: 14px 0 0 14px;
  font-size: 12px;
`;

export const Container = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  padding-top: 5px;
`;

export const StyledLink = styled(Link)<{ isactive?: boolean }>`
  color: ${(props) => (props.isactive ? '#128723' : '#14143c')};
  font-weight: 700;
  display: flex;
  box-shadow: none;
  transition: 0.3s;
  & :hover {
    color: #128723;
    & svg {
      fill: #30bb45;
      transition: 0.3s;
    }
  }
`;

export const Tag = styled.div`
  margin-right: 5px;
`;

export const StyledIcon = styled(CommonIcon)<{ isactive?: boolean }>`
  width: 16px;
  height: 16px;
  fill: ${(props) => (props.isactive ? '#30bb45' : '#14143c')};
  margin-left: -5px;
  margin-right: 5px;
  margin-top: 7px;
  transition: 0.3s;
`;

export const RevealButton = styled(CommonButton)<{
  reveal: boolean | (() => void);
}>`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  position: absolute;
  top: 2px;
  left: -40px;
  width: 40px;
  height: 40px;
  background-color: white;
  border-radius: 50%;
  border: 1px solid black;
  ${(props) =>
    props.reveal &&
    css`
      left: unset;
      right: 0;
      border: none;
    `};
`;

export const ArrowIcon = styled(CommonIcon)<{ rotate: boolean | (() => void) }>`
  transform: ${(props) => (props.rotate ? 'rotate(0deg)' : 'rotate(180deg)')};
  height: 16px;
  width: 16px;
`;

export const TagsWrapper = styled.div`
  display: flex;
  flex-direction: column;
  overflow: hidden;
  overflow-y: scroll;
  max-height: 165px;
  scrollbar-width: none;
  -ms-overflow-style: none;
  &::-webkit-scrollbar {
    display: none;
  }
  ${media.xl`
    max-height: unset;
  `}
`;
