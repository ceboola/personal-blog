import styled from 'styled-components';

export const Button = styled.button`
  padding: 5px 20px;
  font-size: 14px;
  font-weight: 700;
  border: 0px;
  border-radius: 3px;
  appearance: none;
  cursor: pointer;
  outline: none;
  text-transform: capitalize;
  background-color: #47426f;
  color: #fff;
`;
