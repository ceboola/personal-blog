export interface Props {
  websiteId: number;
  id: string | null | undefined;
  mode?: string;
}
