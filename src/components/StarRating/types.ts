import firebase from 'gatsby-plugin-firebase';

export interface PostRatingProps extends firebase.firestore.DocumentData {
  exist: boolean;
  loader: boolean;
  avgRating: number;
  numRatings: number;
  personalRating: number;
}
