import styled, { keyframes } from 'styled-components';

import CommonIcon from '@common/CommonIcon';

const rotateA = keyframes`
  100% {
    transform: rotate(360deg);
  }
`;

const dashA = keyframes`
  0% {
    stroke-dasharray: 1, 150;
    stroke-dashoffset: 0;
  }
  50% {
    stroke-dasharray: 90, 150;
    stroke-dashoffset: -35;
  }
  100% {
    stroke-dasharray: 90, 150;
    stroke-dashoffset: -124;
  }
`;
export const StyledComments = styled.div`
  display: none;
`;

export const StyledCommonIcon = styled(CommonIcon)<{ strokeColor: string }>`
  stroke: ${(props) => props.strokeColor};
  text-align: center;
  animation: ${rotateA} 2s linear infinite;
  stroke-linecap: round;
  > use {
    animation: ${dashA} 1.5s ease-in-out infinite;
    > svg {
    }
  }
`;
