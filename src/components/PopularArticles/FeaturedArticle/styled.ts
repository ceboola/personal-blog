import Img, { FluidObject } from 'gatsby-image';
import styled from 'styled-components';

import DateBadge from '@common/DateBadge';
import { StyledIcon as RegularIcon } from '@components/PopularArticles/styled';
import { media } from '@utils/media';

export const StyledDateBadge = styled(DateBadge)``;
export const Icon = styled(RegularIcon)`
  ${media.xl`
      width: 15vw;
      height: 15vw;
  `}
  ${media.md`
      width: 25vw;
      height: 25vw;
  `}
`;
export const Wrapper = styled.div`
  position: relative;
  height: 100%;
  flex: 0 0 66.66667%;
  background-color: #00c899;
  box-shadow: 0 0 5px rgba(0, 0, 0, 0.15);
  overflow: hidden;
  will-change: transform;
  img {
    transition: 0.3s !important;
    transform: scale(1);
  }
  &:hover {
    img {
      transform: scale(1.1);
      opacity: 0.1 !important;
      filter: grayscale(100%);
    }
    ${Icon} {
      transform: translate(-50%, -50%) scale(1);
      transition: all 300ms 100ms cubic-bezier(0.175, 0.885, 0.32, 1.275);
    }
    ${StyledDateBadge} {
      transition: all 0.25s ease;
      transform: translateY(-100%);
    }
  }
  ${media.md`
      flex: 0 0 55%;
  `}
  ${media.sm`
      flex: 0 0 100%;
  `}
`;

export const FeaturedDesc = styled.span`
  position: absolute;
  bottom: 10%;
  left: 0;
  border-left-color: #fff;
  border-left-style: solid;
  border-left-width: 0.25rem;
  padding: 0 10px;
  width: 100%;
  color: #fff;
  font-size: 24px;
  font-weight: 700;
  text-shadow: -1px -1px 0 #000000, 1px -1px 0 #000000, -1px 1px 0 #000000,
    1px 1px 0 #000000;
`;

export const FeaturedImage = styled(Img)<{ fluid: FluidObject }>`
  width: calc(100% + 2px);
  min-height: 400px;
  max-height: 525px;
`;
