import { animated } from 'react-spring';
import styled, { css } from 'styled-components';

import CommonIcon from '@common/CommonIcon';
import { media } from '@utils/media';

export const Wrapper = styled(animated.div)`
  padding: 20px;
  color: #fff;
  left: -15px;
  position: absolute;
  top: 42px;
  background-color: #17173f;
  width: 300px;
  overflow: hidden scroll;
  max-height: 80vh;
  box-shadow: 16px 16px 16px 0 rgba(0, 0, 0, 0.5);
  z-index: 1;
  scrollbar-width: none;
  -ms-overflow-style: none;
  &::-webkit-scrollbar {
    display: none;
  }
  ${media.sm`
      width: 100%;
      max-height: 100vh;
  `}
  & > p {
    padding: 0;
    margin: 0;
    font-size: 13px;
    width: 100%;
    height: 100%;
    display: flex;
    align-items: center;
  }
`;

export const SearchList = styled.div`
  max-height: max-content;
  box-sizing: content-box;
  position: relative;
  & > ul {
    list-style: none;
    margin-left: 0;
  }
`;

export const Title = styled.div`
  font-family: 'Montserrat';
  font-weight: 700;
  font-size: 14px;
  color: #00c899;
`;

export const Description = styled.div`
  color: #fff;
  font-size: 12px;
`;

export const AnimatedDiv = styled(animated.div)<{
  position: 'bottom' | 'top';
  show: boolean;
}>`
  position: sticky;
  width: 0;
  height: 0;
  left: 100%;
  transition: 0.5s;
  opacity: ${(props: { show: boolean }) => (props.show ? '0' : '1')};
  ${media.sm`
  margin-right: 10px;
`}
  ${media.xxs`
  margin: 0;
`}
  ${(props) =>
    props.position === 'bottom' &&
    css`
      bottom: 20px;
    `};
  ${(props) =>
    props.position === 'top' &&
    css`
      top: 0;
    `};
`;

export const StyledIcon = styled(CommonIcon)<{ rotate: boolean }>`
  fill: #fff;
  transform: ${(props) => (props.rotate ? 'rotate(90deg)' : 'rotate(-90deg)')};
`;
