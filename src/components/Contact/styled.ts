import styled from 'styled-components';

import { media } from '@utils/media';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

export const Union = styled.div`
  display: flex;
  align-items: center;
  margin: 16px 0;
  background-image: linear-gradient(#00c899, #00c899);
  background-size: 100% 3px;
  background-position: 0% 100%;
  background-repeat: no-repeat;
  width: max-content;
  padding-bottom: 6px;
  svg {
    margin-right: 16px;
    fill: #17173f;
  }
`;
