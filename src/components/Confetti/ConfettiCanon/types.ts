export interface ConfettiCanonProps {
  anchorRef: React.RefObject<HTMLElement>;
  colors: string[];
  dotCount: number;
  initialHorizontal: {
    min: number;
    max: number;
  };
  initialUpwards: {
    min: number;
    max: number;
  };
  size: {
    min: number;
    max: number;
  };
}
