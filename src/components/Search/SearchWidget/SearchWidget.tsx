import { useState, useRef, useEffect, memo } from 'react';

import { graphql, useStaticQuery } from 'gatsby';
import { useIntl } from 'gatsby-plugin-react-intl';
import lunr, { Index } from 'lunr';
import { useSpring, animated } from 'react-spring';

import CommonIcon from '@common/CommonIcon';
import SearchResults from '@components/Search/SearchResults';
import { useDeviceDetect } from '@hooks/useDeviceDetect';
import { useDimensions } from '@hooks/useDimensions';

import {
  Wrapper,
  Search,
  CloseButton,
  StyledIcon,
  StyledButton,
  Phrase,
} from './styled';
import { SearchWidgetProps } from './types';

const SEARCH_QUERY = graphql`
  query SearchIndexQuery {
    LunrIndex
  }
`;

const SearchWidget = (): JSX.Element => {
  const intl = useIntl();
  const [{ width }, ref] = useDimensions();
  const inputEl = useRef<HTMLInputElement>(null);
  const buttonEl = useRef<HTMLButtonElement>(null);
  const { isMobile } = useDeviceDetect();
  const [value, setValue] = useState('');
  const [show, setShow] = useState(false);
  const [results, setResults] = useState<SearchWidgetProps[]>([]);
  const [open, toggle] = useState(false);
  const { LunrIndex } = useStaticQuery(SEARCH_QUERY);
  const index = Index.load(LunrIndex.index);
  const { store } = LunrIndex;

  const props = useSpring({
    to: { width: open ? width - 70 : 160, visibility: 'visible' },
    from: { width: 0, visibility: 'hidden' },
    config: { duration: open ? 300 : 400 },
  } as const);

  const handleChange = (
    e:
      | React.FormEvent<HTMLInputElement>
      | React.MouseEvent<HTMLButtonElement, MouseEvent>,
  ): void => {
    const query = e.currentTarget.value || '';
    setValue(query);

    if (!query.length) {
      setResults([]);
    }
    const keywords = query.trim().replace(/\*/g, '').toLowerCase().split(/\s+/);

    if (keywords[keywords.length - 1].length < 2) {
      return;
    }
    try {
      let andSearch: SearchWidgetProps[] = [];
      keywords
        .filter((el: string) => el.length > 1)
        .forEach((el: string, i: number) => {
          const keywordSearch = index
            .query(function (q) {
              q.term(el, {
                editDistance: el.length > 5 ? 1 : 0,
              });
              q.term(el, {
                wildcard:
                  lunr.Query.wildcard.LEADING | lunr.Query.wildcard.TRAILING,
              });
            })
            .map(({ ref }) => {
              return {
                slug: ref,
                ...store[ref],
              };
            });
          andSearch =
            i > 0
              ? andSearch.filter((x) =>
                  keywordSearch.some((el) => el.slug === x.slug),
                )
              : keywordSearch;
        });
      setResults(andSearch);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    inputEl.current?.clientWidth === 0 && buttonEl.current?.click();
  }, [inputEl.current?.clientWidth]);

  return (
    <Wrapper ref={ref} coverEl={show}>
      <StyledButton
        onClick={(e): void => {
          setShow((prevCheck) => !prevCheck);
          setTimeout(() => {
            inputEl.current?.focus();
          }, 100);
          toggle((prevCheck) => !prevCheck);
          handleChange(e);
        }}
      >
        <StyledIcon
          aria-label="search icon"
          name="search"
          width={30}
          height={30}
        />
      </StyledButton>
      <Search show={show} role="search">
        <label style={{ position: 'relative' }} htmlFor="search-input">
          <animated.input
            style={props}
            id="search-input"
            ref={inputEl}
            type="search"
            value={value}
            onChange={handleChange}
            onBlur={() => {
              setTimeout(() => {
                !isMobile && buttonEl.current?.click();
              }, 200);
            }}
            placeholder={intl.formatMessage({ id: 'searchPosts' })}
            autoComplete="off"
          />
          {value && (
            <CloseButton
              ref={buttonEl}
              type="button"
              aria-label="Reset search"
              onClick={(e): void => {
                handleChange(e);
                setShow(false);
                toggle(false);
              }}
            >
              <Phrase>esc</Phrase>
              <CommonIcon
                aria-label="close search"
                name="close"
                width={15}
                height={15}
              />
            </CloseButton>
          )}
        </label>
      </Search>
      {value.trim().length > 1 && (
        <SearchResults results={results} input={inputEl} />
      )}
    </Wrapper>
  );
};
export default memo(SearchWidget);
