import { animated } from 'react-spring';
import styled from 'styled-components';

import CommonIcon from '@common/CommonIcon';
import { media } from '@utils/media';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin: 14px 0;
`;
export const Vertical = styled.div`
  position: relative;
  width: 36px;
  height: 100px;
  display: inline-block;
  margin: 0 10px;
  ${media.xxs`
    margin: 0 4px;
  `}
`;

export const ProgressBar = styled(animated.div)`
  position: absolute;
  bottom: 0;
  width: 100%;
  font-size: 12px;
  line-height: 20px;
  color: #fff;
  background-color: #337ab7;
  box-shadow: inset 0 -1px 0 rgba(0, 0, 0, 0.15);
  transition: width 0.6s ease;
`;

export const Percent = styled(animated.div)`
  font-size: 11px;
  color: #00c899;
  position: absolute;
  top: -20px;
  display: flex;
  justify-content: center;
  width: 100%;
`;

export const Language = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 9px;
  margin-top: 5px;
  ${media.xxs`
    font-size: 8px;
  `}
`;

export const StyledIcon = styled(CommonIcon)`
  margin: 6px 0;
  fill: #007a8b;
`;
