import { memo } from 'react';

import { Wrapper } from './styled';

const AdminPanel = (): JSX.Element => {
  return <Wrapper>hello from userpanel</Wrapper>;
};

export default memo(AdminPanel);
