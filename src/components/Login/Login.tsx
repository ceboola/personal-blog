import { FC, useEffect, useContext } from 'react';

import loadable from '@loadable/component';
import { navigate } from '@reach/router';
import firebase from 'gatsby-plugin-firebase';
import { useIntl } from 'gatsby-plugin-react-intl';
import { Helmet } from 'react-helmet';

const StyledFirebaseAuth = loadable(
  () =>
    import(
      /* webpackChunkName: "StyledFirebaseAuth" */ 'react-firebaseui/StyledFirebaseAuth'
    ),
);

import { PostsContext } from '@providers/PostsProvider';
import { setUser, isLoggedIn } from '@utils/auth';

import { Wrapper, Title } from './styled';
import { UiConfigProps, LoginProps } from './types';

const Login: FC<LoginProps> = () => {
  const { data } = useContext(PostsContext);
  const { title } = data.site.siteMetadata;
  const intl = useIntl();
  const lang = intl.locale === 'en' ? '' : '/pl';

  useEffect(() => {
    isLoggedIn() && navigate(`${lang}/dashboard/user/`);
  }, [lang]);
  if (!firebase || typeof window === 'undefined') return null;

  const getUiConfig = (auth: UiConfigProps): firebaseui.auth.Config => {
    return {
      signInFlow: 'popup',
      signInOptions: [
        auth.GoogleAuthProvider.PROVIDER_ID,
        auth.EmailAuthProvider.PROVIDER_ID,
      ],
      callbacks: {
        signInSuccessWithAuthResult: (result) => {
          setUser(result.user);
          navigate(`${lang}/dashboard/user/`);
        },
      } as firebaseui.auth.Config['callbacks'],
    };
  };
  return (
    <Wrapper>
      <Helmet
        title={`${intl.formatMessage({ id: 'loginStatus.login' })} | ${title}`}
      />
      <Title>
        <p>Dash</p>
        <p>Panel</p>
      </Title>
      <p>Please sign-in to access to the private route:</p>
      <StyledFirebaseAuth
        uiConfig={getUiConfig(firebase.auth)}
        firebaseAuth={firebase.auth()}
      />
    </Wrapper>
  );
};

export default Login;
