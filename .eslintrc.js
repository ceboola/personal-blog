module.exports = {
  parser: 'babel-eslint',
  plugins: ['react', 'import', 'jsx-a11y', 'jam3'],
  extends: [
    'eslint:recommended',
    'plugin:react/recommended',
    'plugin:prettier/recommended',
    'plugin:react-hooks/recommended',
    'plugin:jsx-a11y/recommended',
    'prettier',
  ],
  settings: {
    react: {
      version: 'detect',
    },
    'import/parsers': {
      '@typescript-eslint/parser': ['.ts', '.tsx'],
    },
    'import/resolver': {
      typescript: {
        alwaysTryTypes: true,
      },
    },
  },
  env: {
    browser: true,
    node: true,
    es6: true,
  },
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 2020, // Allows for the parsing of modern ECMAScript features
    sourceType: 'module', // Allows for the use of imports
    allowImportExportEverywhere: true,
    project: './tsconfig.json',
    tsconfigRootDir: './',
  },
  rules: {
    'react/prop-types': 'off', // Disable prop-types as we use TypeScript for type checking
    'no-const-assign': 'error',
    'no-param-reassign': 'error',
    'prefer-const': 'error',
    'no-var': 'error',
    'react/display-name': 'off',
    'react/jsx-curly-brace-presence': ['error', 'never'],
    'jsx-a11y/anchor-is-valid': 'error',
    'import/no-anonymous-default-export': 'error',
    'import/dynamic-import-chunkname': 'error',
    'react-hooks/exhaustive-deps': ['error'],
    'import/order': [
      'error',
      { 'newlines-between': 'always', alphabetize: { order: 'asc' } },
    ],
    'import/no-duplicates': 'error',
    'import/no-cycle': 'error',
    '@typescript-eslint/no-unused-vars': 'warn',
    'jsx-a11y/no-onchange': 'error',
    'jam3/no-sanitizer-with-danger': [
      'error',
      {
        wrapperName: ['dompurify', 'sanitizer'],
      },
    ],
    'jam3/forbid-methods': 'error',
  },
  overrides: [
    {
      files: ['*.js'],
      rules: {
        '@typescript-eslint/no-var-requires': 'off',
        '@typescript-eslint/explicit-module-boundary-types': 'off',
        'react/jsx-uses-react': 'off',
        'react/react-in-jsx-scope': 'off',
      },
    },
    {
      files: ['**/*.ts', '**/*.tsx'],
      plugins: ['@typescript-eslint', 'react'],
      parser: '@typescript-eslint/parser', // Specifies the ESLint parser
      extends: ['plugin:@typescript-eslint/recommended'],
      rules: {
        'react/prop-types': 'off', // Disable prop-types as we use TypeScript for type checking
        '@typescript-eslint/explicit-function-return-type': [
          'error',
          {
            allowExpressions: true,
          },
        ],
        '@typescript-eslint/explicit-module-boundary-types': 'error',
        'react/jsx-uses-react': 'error',
        'react/react-in-jsx-scope': 'off',
        // import plugins
        'import/no-unresolved': 'error',
        'import/named': 'error',
        'import/namespace': 'error',
        'import/default': 'error',
        'import/export': 'error',
        'import/order': [
          'error',
          {
            pathGroups: [
              {
                pattern: './styled',
                group: 'index',
              },
              {
                pattern: './types',
                group: 'index',
              },
              {
                pattern: 'react',
                group: 'builtin',
                position: 'before',
              },
              // {
              //   pattern: '@react*',
              //   group: 'builtin',
              //   position: 'after',
              // },
            ],
            pathGroupsExcludedImportTypes: ['builtin'],
            alphabetize: { order: 'asc', caseInsensitive: true },
            groups: [
              'builtin',
              'unknown',
              'external',
              'internal',
              'parent',
              'sibling',
              'index',
            ],
            'newlines-between': 'always-and-inside-groups',
          },
        ],
      },
    },
  ],
};
