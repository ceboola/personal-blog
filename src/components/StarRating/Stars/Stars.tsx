import { useMemo } from 'react';

import { getCurrentUser } from '@utils/auth';

import { Wrapper, StyledIcon } from './styled';
import { StarsProps } from './types';

const Stars: React.FC<StarsProps> = ({
  index,
  rating,
  hoverRating,
  onMouseEnter,
  onMouseLeave,
  onSaveRating,
}) => {
  const { uid } = getCurrentUser();
  const fill = useMemo(() => {
    if (hoverRating >= index) {
      return 'yellow';
    } else if (!hoverRating && rating >= index) {
      return 'yellow';
    }
    return 'none';
  }, [rating, hoverRating, index]);

  return (
    <Wrapper
      onMouseEnter={() => uid && onMouseEnter(index)}
      onMouseLeave={() => uid && onMouseLeave()}
      onClick={() => uid && onSaveRating(index)}
    >
      <StyledIcon width={24} height={24} name="star" fill={fill} />
    </Wrapper>
  );
};

export default Stars;
