export const animations = {
  fadeIn: {
    initial: { opacity: 0 },
    from: { opacity: 0 },
    enter: { opacity: 1 },
    leave: { opacity: 0, display: 'none' },
    config: { duration: 600 },
  },
  quick: {
    from: { opacity: 0 },
    enter: { opacity: 1 },
    leave: { opacity: 0 },
    config: { duration: 1 },
  },
  slideLeft: {
    from: {
      opacity: 0,
      transform: 'translate(100%, 0)',
    },
    enter: [
      { opacity: 1, transform: 'translate(0%, 0)' },
      { transform: 'none', immediate: true },
    ],
    leave: {
      opacity: 0,
      transform: 'translate(-100%, 0)',
      immediate: true,
      display: 'none',
    },
    config: { duration: 200 },
  },
  slideRight: {
    from: {
      opacity: 0,
      transform: 'translate(-100%, 0)',
    },
    enter: [
      { opacity: 1, transform: 'translate(0%, 0)' },
      { transform: 'none', immediate: true },
    ],
    leave: {
      opacity: 0,
      transform: 'translate(100%, 0)',
      immediate: true,
      display: 'none',
    },
    config: { duration: 200 },
  },
  slideDown: {
    from: {
      opacity: 0,
      transform: 'translate(0, -100%)',
    },
    enter: [{ opacity: 1, transform: 'translate(0%, 0)' }, { transform: 0 }],
    leave: { opacity: 0, transform: 'translate(0, 100%)', display: 'none' },
  },
  slideUp: {
    from: { opacity: 0, transform: 'translate(0, 100%)' },
    enter: [
      { opacity: 1, transform: 'translate(0%, 0)' },
      { transform: 'none' },
    ],
    leave: { opacity: 0, transform: 'translate(0, -100%)' },
    config: { duration: 200 },
  },
};
