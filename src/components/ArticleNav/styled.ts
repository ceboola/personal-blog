import { Link } from 'gatsby';
import styled from 'styled-components';

import CommonIcon from '@common/CommonIcon';
import { media } from '@utils/media';

export const List = styled.ul`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  list-style: none;
  padding: 0;
`;

export const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 5em;
  height: 5em;
  line-height: 5em;
  text-align: center;
  box-sizing: border-box;
  border-radius: 10em;
  border: 1px solid #00c899;
  box-shadow: 0 0 0 0 transparent;
  transition: all ease-out 0.1s;
  background-color: #fff;
  text-decoration: none;
  ${media.lg`
      border: 0;
      width: auto;
  `}
`;

export const Info = styled.div`
  display: none;
  box-sizing: border-box;
  transition: all ease-out 0.2s;
  margin: 0 10px;
  min-width: 0;
  max-width: 15em;
  ${media.lg`
      display: initial;
  `}
`;

const Item = styled(Link)`
  position: fixed;
  bottom: 30%;
  display: block;
  overflow: hidden;
  border-radius: 10em;
  transition: none;
  box-shadow: none;
  z-index: 1;
  &:hover,
  &:focus {
    border-radius: 0;
    ${Container} {
      box-shadow: 0 0 0 25em #00c899;
      ${media.lg`
          background-color: #00c899;
  `}
    }
    ${Info} {
      display: block;
    }
  }
  ${media.lg`
      position: initial;
      display: flex;
      flex: 0 1 100%;
      border: 0;
      &:hover {
        text-decoration: underline
      }
  `}
`;

export const Wrapper = styled.nav`
  position: relative;
  ${media.lg`
      display: flex;
  `}
`;

export const PrevItem = styled(Item)`
  left: 0;
  text-align: left;
`;

export const NextItem = styled(Item)`
  right: 0;
  text-align: right;
`;

export const Teaser = styled.section<{ reverse: boolean }>`
  display: flex;
  flex-direction: ${(props) => (props.reverse ? 'row-reverse' : 'row')};
  padding: 0.5em;
  transition: all ease-out 0.2s;
  overflow: visible;
  ${media.lg`
      width: 100%;
  `}
`;

export const Title = styled.h4`
  display: block;
  max-height: 1.25em;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
  margin: 1.25em 0 0.25em;
  font-size: 1em;
  line-height: 1.25em;
  font-weight: 700;
  color: #17173f;
  text-transform: none;
  font-family: 'Montserrat';
`;

export const Description = styled.span`
  display: block;
  max-height: 1.334em;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
  margin: 0;
  font-size: 0.75em;
  line-height: 1.334em;
  font-weight: 400;
  color: white;
  ${media.lg`
      color: black;
  `}
`;

export const StyledIcon = styled(CommonIcon)<{ rotate: boolean }>`
  transform: ${(props) => props.rotate && 'rotate(180deg)'};
  width: 27px;
  height: 27px;
`;
