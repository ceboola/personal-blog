import styled from 'styled-components';

import { media } from '@utils/media';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  height: 100%;
  ${media.sm`
      margin-bottom: 32px;
  `}
`;

export const ContainerIcon = styled.div`
  width: 24px;
  height: 24px;
  & + & {
    margin-left: 40px;
  }
`;
