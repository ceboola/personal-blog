import { memo } from 'react';

import CommonIcon from '@common/CommonIcon';

import { socialMediaData } from './static';

import { Wrapper, ContainerIcon } from './styled';

const SocialMedia: React.FC = () => {
  return (
    <Wrapper>
      {socialMediaData.map(({ url, icon }, index) => {
        return (
          <ContainerIcon key={`${url}_${index}`}>
            <a
              title={`${icon} link`}
              href={url}
              target="_blank"
              rel="noreferrer"
            >
              <CommonIcon name={icon} viewBox="0 0 24 24" />
            </a>
          </ContainerIcon>
        );
      })}
    </Wrapper>
  );
};

export default memo(SocialMedia);
