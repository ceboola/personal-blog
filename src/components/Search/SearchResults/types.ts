export interface SearchResultsProps {
  results: Array<{
    excerpt: string;
    slug: string;
    title: string;
    lang: string;
  }>;
  input: React.RefObject<HTMLInputElement>;
}
