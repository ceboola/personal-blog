import { graphql } from 'gatsby';

import Tags from '@components/Tags';

const TagsPage: React.FC = () => {
  return <Tags />;
};

export default TagsPage;

export const pageQuery = graphql`
  query($language: String!, $slug: String!) {
    site {
      siteMetadata {
        title
      }
    }
    SummarizedStats {
      ...SummarizedStats
    }
    gitlab(name: { eq: "personal-blog" }) {
      ...CurrentProject
    }
    currentPageViews: pageViews(path: { eq: $slug }) {
      ...SitePageViews
    }
    allMarkdownRemark(filter: { frontmatter: { lang: { eq: $language } } }) {
      group(field: frontmatter___tags) {
        fieldValue
        totalCount
      }
    }
  }
`;
