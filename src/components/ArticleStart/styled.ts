import { Link } from 'gatsby';
import Img, { FluidObject } from 'gatsby-image';
import styled, { css } from 'styled-components';

import CommonIcon from '@common/CommonIcon';
import DateBadge from '@common/DateBadge';
import TimeToRead from '@common/TimeToRead';
import { media } from '@utils/media';

export const StyledDateBadge = styled(DateBadge)``;
export const StyledIconHover = styled(CommonIcon)<{
  width?: string;
  height?: string;
}>`
  position: absolute;
  top: 50%;
  left: 50%;
  border-radius: 50%;
  width: ${(props) => (props.width ? props.width : '50%')};
  height: ${(props) => (props.height ? props.height : '50%')};
  line-height: ${(props) => (props.height ? props.height : '50%')};
  background: #f3ecff;
  box-shadow: 0 0 5px rgba(0, 0, 0, 0.15);
  transform: translate(-50%, -50%) scale(0);
  transition: all 300ms 0ms cubic-bezier(0.6, -0.28, 0.735, 0.045);
  z-index: 1;
  ${media.lg`
      width: 100px;
      height: 100px;
  `}
`;

export const Article = styled.article`
  height: 100%;
  overflow: hidden;
  img {
    transition: 0.3s !important;
    transform: scale(1) translateZ(0);
  }
  &:hover {
    img {
      transform: scale(1.2);
      opacity: 0.1 !important;
      filter: grayscale(100%);
    }
    ${StyledIconHover} {
      transform: translate(-50%, -50%) scale(1);
      transition: all 300ms 100ms cubic-bezier(0.175, 0.885, 0.32, 1.275);
    }
  }
`;

export const Title = styled.div`
  line-height: 21px;
  width: 100%;
  height: 50px;
  margin: 6px 0;
  font-size: 21px;
`;
export const StyledLink = styled(Link)<{ to: string }>`
  color: #14143c;
  font-weight: 900;
  box-shadow: none;
  background-image: linear-gradient(#128723, #128723);
  background-size: 0% 4px;
  background-position: 0% 100%;
  background-repeat: no-repeat;
  transition: 0.3s;
  &:hover {
    color: #128723;
    background-size: 100% 4px;
  }
`;

export const ArticleContainer = styled.div`
  display: flex;
  align-content: space-between;
  flex-flow: row wrap;
  position: relative;
  width: 100%;
  min-height: 400px;
`;

export const StyledImg = styled(Img)<{ fluid: FluidObject }>`
  margin-bottom: 14px;
  height: 200px;
`;

export const TagList = styled.ul`
  height: 30px;
  overflow: hidden;
  list-style: none;
  display: flex;
  flex-wrap: wrap;
  margin: 0;
  padding: 0;
  font-size: 12px;
  font-weight: 700;
  text-transform: uppercase;
  line-height: 16px;
  white-space: nowrap;
  ${StyledLink} {
    font-weight: 700;
  }
`;

export const Tag = styled.li<{ isActive: boolean }>`
  color: #30bb45;
  margin: 0;
  & :not(:last-child):after {
    content: '•';
    padding: 0 5px 0 5px;
    color: #79869c;
  }
  & a {
    color: #14143c;
    ${(props) =>
      props.isActive &&
      css`
        color: #128723;
        background-image: linear-gradient(#128723, #128723);
        background-size: 100% 2px;
        background-position: 0% 100%;
        background-repeat: no-repeat;
      `};
  }
`;

export const ReadMore = styled.span`
  font-size: 15px;
  line-height: 24px;
  display: inline-flex;
  align-items: center;
  align-self: flex-start;
  color: #30bb45;
  transition: 0.3s;
  transform: translateX(calc(-100% + 27px));
  transition-property: transform, color;
  & > a {
    display: flex;
  }
  &:hover svg {
    transition: inherit;
    fill: #128723;
  }
`;

export const Wrapper = styled.div`
  position: relative;
  flex: 0 1 calc(33% - 40px);
  margin: 10px 20px;
  overflow: hidden;
  will-change: transform;
  &:hover {
    ${ReadMore} {
      text-decoration: none;
      transform: none;
    }
    ${StyledDateBadge} {
      transition: all 0.25s ease;
      transform: translateY(-100%);
    }
  }
  ${media.md`
      flex: 0 1 calc(50% - 40px);
  `}
  ${media.xxs`
      flex: 0 1 100%;
  `}
`;

export const StyledIcon = styled(CommonIcon)`
  width: 24px;
  height: 24px;
  fill: #30bb45;
  margin-left: 20px;
`;

export const Description = styled.section`
  height: 110px;
  display: -webkit-box;
  -webkit-line-clamp: 4;
  -webkit-box-orient: vertical;
  overflow: hidden;
  p {
    margin: 0;
  }
  ${media.xxs`
      height: auto;
  `}
`;

export const ContainerInfo = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: 28px;
`;

export const HoverWrapper = styled.div`
  position: relative;
  background-color: #00c899;
  will-change: transform;
`;

export const StyledTimeToRead = styled(TimeToRead)`
  display: flex;
  font-size: 12px;
  margin: 6px 0;
  font-family: 'Montserrat';
  & svg {
    fill: #128723;
    margin-right: 4px;
  }
`;
