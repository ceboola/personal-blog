import { forwardRef, memo } from 'react';

import { Button } from './styled';
import type { CommonButtonProps } from './types';

const CommonButton = forwardRef<HTMLButtonElement, CommonButtonProps>(
  (props, ref) => {
    return (
      <Button {...props} ref={ref}>
        {props.children}
      </Button>
    );
  },
);

export default memo(CommonButton);
