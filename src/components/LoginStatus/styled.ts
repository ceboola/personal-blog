import { Link } from 'gatsby';
import styled from 'styled-components';

import { media } from '@utils/media';
import { rhythm } from '@utils/typography';

export const Wrapper = styled.div`
  margin: 5px ${`calc(-100vw / 2 + ${rhythm(36)} / 2)`};
  width: max-content;
  font-size: 10px;
  padding: 3px;

  p {
    margin: 0;
    padding: 2px;
    &:nth-child(2) {
      border: 1px solid black;
    }
  }

  ${media.lg`
      margin-left: 0;
      margin-right: 0;
  `}
`;

export const StyledLink = styled(Link)`
  box-shadow: none;
  color: #000;
  background-image: linear-gradient(#00c899, #00c899);
  background-size: 100% 3px;
  background-position: 0% 100%;
  background-repeat: no-repeat;
  padding-bottom: 1px;
`;
