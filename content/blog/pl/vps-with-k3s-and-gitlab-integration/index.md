---
lang: pl
title: VPS z K3S i integracja z Gitlabem
date: '2020-06-14T21:22:00.284Z'
featuredImage: ../../images/vps-with-k3s-and-gitlab-integration/k3s-feature.jpg
tags: ['autodevops', 'gitlab', 'kubernetes', 'k3s']
category: 'DevOps'
---

#### Co to jest Kubernetes?

Czy kiedykolwiek słyszałeś o czymś takim jak orkiestracja w swoim życiu? Jeżeli tak to prawdopodobnie będziesz zainteresowany/a dzisiejszym tematem. Kubernetes umożliwia uruchamianie skonteneryzowanych aplikacji, dzięki czemu osoby zajmujące się DevOpsem mogą wykonywać swoją pracę szybciej niż przy dotychczasowym modelu pracy, teraz o znaczną część dba właśnie Kubernetes który automatyzuje procesy testów czy wrzutek, został zaprojektowany w celu zautomatyzowania uruchamiania, skalowania i obsługi kontenerów aplikacji. Jest odpowiedzialny za cały cykl życia kontenera -> alokacje/ustawianie/dostarczanie/wewnętrzną sieć/monitorowanie i jednocześnie naprawanie ich w czasie rzeczywistym. My tu gadu-gadu, a w tytule ani słowa na temat Kubernetesa!

#### K3S huh?

K3S jest to po prostu odchudzona wersja Kubernetesa. Wybrałem te dystrybucje z prostego powodu, jest po prostu lekka. Nie widzę żadnych przeciwskazań żeby użyć jej jako zamiennika jeżeli dostarcza mi tego co potrzebuję, zresztą mój VPS jest najtańszą możliwą opcją (1vCore, 2GB RAM), gdy po przeciwnej stronie jest K8S(pierwsza + ostatnia litera słowa kubernetes, natomiast 8 oznacza skróconą część wewnątrz > "ubernete") który w wymaganiach minimalnych preferuje na start 2GB wolnego ramu. Aktualnie mój K3S obsługuje trzy aplikacje: 2 frontend, 1 backend. Poniżej zrzut z serwera ile na ten moment zużywa zasobów.

![Resources on VPS after installing K3S](../../images/vps-with-k3s-and-gitlab-integration/resources.jpg)

OK! Po przynudzającym wstępie dotyczącym K3S przejdźmy do działań! Instalacja i konfiguracja jest dosyć prosta i prostolinijna.

## 1. Zaintaluj K3S na Twoim VPSie

Najpierw sprawdź aktualną listę wspieranych clusterów kubernetesa przez gitlab:
https://docs.gitlab.com/ee/user/project/clusters/#supported-cluster-versions

W moim przykładzie skorzystam z wersji <u>v1.19.9-rc1+k3s1</u>

```bash
curl -sfL https://get.k3s.io | INSTALL_K3S_VERSION="v1.19.9-rc1+k3s1" --no-deploy traefik sh -
```

Jak widać użyliśmy flagi "--no-deploy traefik" przy instalacji, spowodowane jest to tym, że K3S standardowo instaluje nam traefik ale w związku z tym, że chcemy skorzystać z dobrodziejstw integracji Gitlab z K3S dodamy nasz kontroler bezpośrednio w panelu w następnych krokach. Gitlab sam w sobie oferuje nam instalację ingress-nginx, gdybyśmy w tym momencie zainstalowali traefik to w następstwie Gitlab nie byłby w stanie doinstalować nam ingress-nginx (są to dwa pakiety odpowiedzalne za to samo). Nie przejmuj się potem się rozjaśni gdy przejdziemy do części Gitlab UI.

Notka: posiada również firewall
https://docs.gitlab.com/ee/user/project/clusters/protect/web_application_firewall/index.html

## 2. Stwórz ServiceAccount z rolą cluster-admin

```bash
cat <<EOF | kubectl apply -f -
apiVersion: v1
kind: ServiceAccount
metadata:
  name: gitlab-admin
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: gitlab-admin
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: gitlab-admin
  namespace: kube-system
EOF
```

## 3. Wyciągnij secret i token dla Gitlaba

```bash
K3S_DEPLOY_SECRET_NAME=`kubectl get serviceaccount default -o jsonpath='{.secrets[0].name}'`
K3S_TOKEN=`kubectl get secret $KUBE_DEPLOY_SECRET_NAME -o jsonpath='{.data.token}'|base64 --decode`
K3S_CERT=`kubectl get secret $KUBE_DEPLOY_SECRET_NAME -o jsonpath='{.data.ca\.crt}'|base64 --decode`
```

potem

```bash
echo $K3S_TOKEN
echo $K3S_CERT
```

I to na tyle jeżeli chodzi o część instalacji K3S na Twoim serwerze VPS, teraz przejdźmy do integracji z Gitlabem (zachowaj TOKEN i CERT będzie nam potem potrzebny przy integracji w panelu)

## 4. Integracja z Gitlabem

### Stwórz nowy projekt

![New project](../../images/vps-with-k3s-and-gitlab-integration/new_project.jpg)

### Z lewego menu wybierz Operations > Kubernetes

![Operations](../../images/vps-with-k3s-and-gitlab-integration/operations.jpg)

### Kliknij "connect cluster with certificate" -> "connect existing cluster"

Powinieneś zobaczyć dokładnie to co na obrazku poniżej, wypełnij pola i zastąp moje placeholdery swoimi prawdziwymi danymi z serwera VPS. Jeżeli nie jesteś pewien na jakim porcie(standardowo powinna odpalić się na porcie 6443) stoi aplikacja możesz to sprawdzić za pomocą:

```bash
cat /etc/rancher/k3s/k3s.yaml
```

poszukaj frazy server, powinna tak wyglądać:

```
server: https://yourIp:yourPort
```

Jeżeli masz w planach stawiać więcej niż jeden projekt na aktualnym kubernetesie, powinieneś w każdym projekcie z osobna używać unikalnych przedrostków, jeżeli tylko jeden projekt będzie korzystał z K3S możesz te pole zostawić puste.

![New cluster](../../images/vps-with-k3s-and-gitlab-integration/new_cluster.jpg)

Po wypełnienu pól kliknij "add kubernetes cluster". Prawie skończone! Już tylko kilka kroków dzieli Cię od twoich automatycznych wrzutek na serwer!

Teraz znowu lewe menu, wróć do <u><b>Operations</b> > <b>Kubernetes</b></u>.

Kliknij Twój nowo stworzony klaster. W "<u><b>details</b></u>" musisz podać tylko swoją aktualną domenę, reszta została już dodana na etapie dodawania klastera.

Potem przeskocz do zakładki obok <u><b>Applications</b></u>.

Będąc szczerym potrzebujesz tylko dwóch pierwszych rzeczy aby kubernetes wstał i zaczął być dostępny na świat: <b>Ingress</b> który jest naszym kontrolerem odpowiedzialnym za reverse-proxy i load-balancer, <b>Cert-Manager</b> za automatyczne odnawianie certyfikatów SSL, reszta zależy od Ciebie co chcesz zainstalować.

![New applications](../../images/vps-with-k3s-and-gitlab-integration/new_applications.jpg)

Po tym wszystkim K3S powinien być już gotów do działania! Możesz manualnie aktywować ficzery autodevopsa bezpośrednio w lewym menu <u><b>Settings</b> > <b>CI/CD</b></u>

![New autodevops](../../images/vps-with-k3s-and-gitlab-integration/new_autodevops.jpg)

Ale w mojej opini lepiej od samego początku ustawić swój własny konfigurowalny plik dotyczący CI/CD. Nie robiąc tego musimy przymykać oko na wiele rzeczy które domyślnie robi szablon stworzony przez pracowników Gitlaba, więc stwórzmy swój własny.

### Stwórz plik .gitlab-ci.yml w głównym katalogu projektu

Tutaj to już według uznania dodajemy/modyfikujemy to co nam akurat potrzebne, możesz dodać szablon i nic więcej nie robić, tylko wtedy mamy masę pipelinów które niekoniecznie chcemy mieć.

```yaml
include:
  - template: Auto-DevOps.gitlab-ci.yml
```

Dlatego lepiej jak zapoznasz się z dokumentacją i dopasujesz sobie wedle uznania.
Dokumentacja: https://docs.gitlab.com/ee/topics/autodevops/customize.html

przykład wykorzystania konfiguracji:

```yaml
include:
  - template: Auto-DevOps.gitlab-ci.yml

variables:
  POSTGRES_ENABLED: 'false'
  SAST_DISABLED: 'true'
  LICENSE_MANAGEMENT_DISABLED: 'true'
  DEPENDENCY_SCANNING_DISABLED: 'true'
  CONTAINER_SCANNING_DISABLED: 'true'
  CODE_QUALITY_DISABLED: 'true'
  PERFORMANCE_DISABLED: 'true'

production:
  environment:
    url: https://blog.$KUBE_INGRESS_BASE_DOMAIN
  rules:
    - if: '$CI_COMMIT_REF_NAME == "master"'
```

### Czy mogę korzystać z wielu aplikacji na tym samym K3S!?

Tak jest to możliwe, kroki są takie same, tylko w kolejnych projektach nie instalujemy rzeczy z zakładki <b>Applications</b>. Podajemy dokładnie te same certyfikaty i tokeny do kolejnych projektów, jedyna rzecz która będzie je różnić to prefix (project namespace prefix)

### Ostatnia rzecz ale równie ważna!

Z tego co pamiętam w przeszłości miałem problemy z publikacją aplikacji na K3S, prawdopodobnie API które wykorzystywało K3S różniło się wersją która była na Gitlabie, nie wiem jak teraz ale prewencyjnie nie zaszkodzi określić jego wersji.

Dodaj <b><u>.gitlab</u></b> folder w głównym katalogu projektu, potem w środku folderu .gitlab stwórz nowy plik <b><u>auto-deploy-values.yaml</u></b>, dodaj do niego zawartość poniżej.

```yaml
deploymentApiVersion: apps/v1
```

W tym pliku można nadpisywać wartości helm charta, czasami będziesz potrzebował coś dodać np. do kontrolera ingress czy chociażby zmienić port aplikacji.

Cała lista tutaj:

https://gitlab.com/gitlab-org/cluster-integration/auto-deploy-image/-/blob/master/assets/auto-deploy-app/values.yaml

I to wszystko! 😎😎😎
