import { useContext } from 'react';

import { graphql, Link } from 'gatsby';

import Article from '@components/Article';
import ArticleNav from '@components/ArticleNav';
import { Comments } from '@components/Comments';
import SEO from '@components/SEO';
import SiteHeader from '@components/SiteHeader';
import TagsList from '@components/TagsList';
import { useWindowSize } from '@hooks/useWindowSize';
import { PostsContext } from '@providers/PostsProvider';

import { Props } from './types';

const BlogPostTemplate: React.FC<Props> = ({ pageContext, location }) => {
  const { data } = useContext(PostsContext);
  const [{ width }] = useWindowSize();
  const post = data.markdownRemark;
  const totalPosts = data.totalPosts.totalCount;
  const {
    title,
    description,
    date,
    tags: tag,
    featuredImage,
  } = post.frontmatter;
  const { excerpt, timeToRead } = post;
  const siteTitle = data.site.siteMetadata.title;
  const { slug } = pageContext;
  const tags = data.tags.group;
  const currentPageViews = data.currentPageViews.totalCount;

  return (
    <>
      <SiteHeader title={siteTitle} location={location} />
      <SEO title={title} description={description || excerpt} article={data} />
      <Article
        title={title}
        post={post.html}
        date={date}
        featuredImage={featuredImage}
        slug={slug}
        currentPageViews={currentPageViews ?? 0}
        timeToRead={timeToRead}
      />
      <ArticleNav pageContext={pageContext} />
      <TagsList
        link={Link}
        tags={tags}
        tag={tag}
        totalPosts={totalPosts}
        slug={slug}
        revealed={width > 1285}
      />
      {slug && <Comments websiteId={877} id={slug} />}
    </>
  );
};

export default BlogPostTemplate;

export const pageQuery = graphql`
  query BlogPostBySlug($slug: String!, $lang: String!) {
    site {
      siteMetadata {
        title
      }
    }
    SummarizedStats {
      ...SummarizedStats
    }
    gitlab(name: { eq: "personal-blog" }) {
      ...CurrentProject
    }
    tags: allMarkdownRemark(filter: { frontmatter: { lang: { eq: $lang } } }) {
      group(field: frontmatter___tags) {
        fieldValue
        totalCount
      }
    }
    totalPosts: allMarkdownRemark(
      filter: { frontmatter: { lang: { eq: $lang } } }
    ) {
      totalCount
    }
    markdownRemark(fields: { slug: { eq: $slug } }) {
      id
      excerpt(pruneLength: 160)
      html
      timeToRead
      frontmatter {
        title
        rawDate: date
        date(formatString: "MMMM DD, YYYY")
        description
        tags
        lang
        featuredImage {
          childImageSharp {
            fluid(maxWidth: 1366, maxHeight: 600, quality: 85) {
              ...GatsbyImageSharpFluid_withWebp
            }
          }
        }
      }
      fields {
        slug
      }
    }
    GAData: allPageViews {
      edges {
        node {
          id
          totalCount
        }
      }
    }
    currentPageViews: pageViews(path: { eq: $slug }) {
      ...SitePageViews
    }
  }
`;
