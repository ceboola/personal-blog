import styled, { css } from 'styled-components';

import CommonButton from '@common/CommonButton';
import { media } from '@utils/media';

import { ShadowProps } from './types';

export const Container = styled.div`
  position: relative;
  width: 100%;
  display: flex;
`;
export const Wrapper = styled.ul`
  display: inherit;
  flex-direction: row;
  width: 100%;
  justify-content: flex-start;
  list-style-type: none;
  font-weight: 700;
  font-size: 15px;
  flex-wrap: wrap;
  ${media.md`
      flex-wrap: nowrap;
      width: 768px;
      overflow-x: auto;
      &::-webkit-scrollbar {
        display: none;
      }
  `}
  ${media.xxs`
      margin: 0;
  `}
`;
export const Shadow = styled.div<ShadowProps>`
  background: ${(props) =>
    props.direction === 'left'
      ? 'linear-gradient(90deg, rgba(23,23,63,1) 0%, rgba(23,23,63,1) 40%, rgba(23,23,63,0.9640231092436975) 70%, rgba(23,23,63,0.7931547619047619) 90%, rgba(23,23,63,0.11808473389355745) 100%)'
      : 'linear-gradient(90deg, rgba(23,23,63,0.11808473389355745) 0%, rgba(23,23,63,0.7931547619047619) 10%, rgba(23,23,63,0.9640231092436975) 40%, rgba(23,23,63,1) 70%, rgba(23,23,63,1) 100%)'};
  ${(props: ShadowProps) =>
    props.direction === 'right' &&
    css`
      position: absolute;
      z-index: 2;
      height: 40px;
      width: 40px;
      top: 0;
      right: 0;
      opacity: ${(props: ShadowProps) => (props.show ? '1' : '0')};
      transition: 0.5s;
    `};
  ${(props) =>
    props.direction === 'left' &&
    css`
      position: absolute;
      z-index: 2;
      height: 40px;
      width: 40px;
      top: 0;
      left: 0;
      opacity: ${(props: ShadowProps) => (props.show ? '1' : '0')};
      transition: 0.5s;
    `};
`;
export const Category = styled.li`
  flex-shrink: 0;
  & :not(:last-child) {
    margin-right: 15px;
  }
`;

export const StyledButton = styled(CommonButton)<{ active: boolean }>`
  position: relative;
  overflow: hidden;
  background: transparent;
  transition: all 0.3s ease-in-out;
  color: ${(props) => (props.active ? '#00C899' : '#fff')};
  z-index: 1;
  ${(props) =>
    !props.active &&
    css`
      &:hover {
        color: #00c899;
        &:before {
          opacity: 1;
          transform: translateX(102%);
        }
        &:after {
          opacity: 1;
          transform: translateX(-102%);
        }
      }
    `};
  &:after {
    content: '';
    width: 100%;
    height: 100%;
    display: block;
    background: #47426f;
    position: absolute;
    right: 0%;
    opacity: 1;
    top: 0;
    z-index: -1;
    transition: all 0.45s cubic-bezier(0.77, 0, 0.175, 1);
    box-shadow: ${(props) =>
      props.active ? 'inset 0px 0px 0px 3px #00c899' : 'none'};
  }
  &:before {
    content: '';
    width: 100%;
    height: 100%;
    display: block;
    background: #00c899;
    position: absolute;
    left: -1%;
    opacity: 1;
    top: 0;
    z-index: -1;
    transition: all 0.45s cubic-bezier(0.77, 0, 0.175, 1);
  }
`;
