/* eslint-disable react-hooks/rules-of-hooks */
import { useEffect, useState } from 'react';

export const useWindowScroll = (ms: number): number => {
  if (typeof window === 'undefined') return 0;
  // Store the state
  const [scrollPos, setScrollPos] = useState(window.pageYOffset);

  // On Scroll
  const onScroll = (): void => {
    setScrollPos(window.pageYOffset);
  };

  // Add and remove the window listener
  useEffect(() => {
    const timer = setTimeout(() => {
      window.addEventListener('scroll', onScroll);
    }, ms);
    return () => {
      clearTimeout(timer);
      window.removeEventListener('scroll', onScroll);
    };
  });

  return scrollPos;
};
