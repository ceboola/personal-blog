require('dotenv').config();

const path = require(`path`);

const crypto = require('crypto');
const fs = require('fs').promises;

const { createFilePath } = require(`gatsby-source-filesystem`);
const { Gitlab } = require('@gitbeaker/node');
const LoadablePlugin = require('@loadable/webpack-plugin');
const glob = require('glob');
const { google } = require('googleapis');
const fetch = require('isomorphic-fetch');
const kebabCase = require('lodash.kebabcase');
const md5 = require('md5');
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');
const { GraphQLJSONObject } = require(`graphql-type-json`);
const striptags = require(`striptags`);
const lunr = require(`lunr`);

global.DOMParser = require('xmldom').DOMParser;

const packageJson = require('./package');

const GOOGLE_EMAIL = process.env.GATSBY_CLIENT_EMAIL;
const GOOGLE_KEY = process.env.GATSBY_PRIVATE_KEY;
const GOOGLE_VIEWID = process.env.GATSBY_VIEWID;

const pattern = new RegExp('\\/(?:pl\\/pl\\/|en\\/(?:en\\/|pl\\/)?)');

const languages = ['en', 'pl']; // gatsby-plugin-react-intl options

function flattenMessages(nestedMessages, prefix = '') {
  return Object.keys(nestedMessages).reduce((messages, key) => {
    const value = nestedMessages[key];
    const prefixedKey = prefix ? `${prefix}.${key}` : key;

    if (typeof value === 'string') {
      messages[prefixedKey] = value;
    } else {
      Object.assign(messages, flattenMessages(value, prefixedKey));
    }

    return messages;
  }, {});
}

exports.createPages = async ({ graphql, actions, cache }) => {
  const { createPage } = actions;
  const blogPost = path.resolve(`src/templates/blog-post/blog-post.tsx`);
  const tagTemplate = path.resolve('src/templates/tags/tags.tsx');
  const result = await graphql(`
    {
      allMarkdownRemark(
        sort: { order: DESC, fields: [frontmatter___date] }
        limit: 2000
      ) {
        edges {
          node {
            timeToRead
            excerpt
            fields {
              slug
            }
            frontmatter {
              title
              tags
              description
              lang
            }
          }
        }
      }
      tagsGroup: allMarkdownRemark(limit: 2000) {
        group(field: frontmatter___tags) {
          fieldValue
          totalCount
        }
      }
      tagsEn: allMarkdownRemark(
        filter: { frontmatter: { lang: { eq: "en" } } }
      ) {
        group(field: frontmatter___tags) {
          fieldValue
          totalCount
        }
      }
      tagsPl: allMarkdownRemark(
        filter: { frontmatter: { lang: { eq: "pl" } } }
      ) {
        group(field: frontmatter___tags) {
          fieldValue
          totalCount
        }
      }
      lastBuildTime: site {
        buildTime(formatString: "DD/MM/YYYY")
      }
      projectSize: gitlab(name: { eq: "personal-blog" }) {
        statistics {
          repository_size
        }
      }
    }
  `);

  if (result.errors) {
    throw result.errors;
  }
  await cache.set('buildTimeCache', result.data.lastBuildTime);

  const getMessages = (path, language) => {
    try {
      const messages = require(`${path}/${language}.json`);

      return flattenMessages(messages);
    } catch (error) {
      if (error.code === 'MODULE_NOT_FOUND') {
        process.env.NODE_ENV !== 'test' &&
          console.error(
            `[gatsby-plugin-react-intl] couldn't find file "${path}/${language}.json"`,
          );
      }
      throw error;
    }
  };

  // Create blog posts pages.
  const posts = result.data.allMarkdownRemark.edges;
  const mappedTimeToRead = await result.data.allMarkdownRemark.edges.map(
    (val) => {
      return {
        timeToRead: val.node.timeToRead,
        lang: val.node.frontmatter.lang,
      };
    },
  );
  await cache.set('timeToReadCache', mappedTimeToRead);
  await cache.set('tagsCache', result.data.tagsGroup.group);
  await cache.set('tagsCacheEn', result.data.tagsEn.group);
  await cache.set('tagsCachePl', result.data.tagsPl.group);
  await cache.set(
    'projectSizeCache',
    result.data.projectSize.statistics.repository_size,
  );

  const plPosts = posts.map((post) => {
    const language = post.node.frontmatter.lang;
    if (language === 'pl') {
      return post;
    }
  });
  const enPosts = posts.map((post) => {
    const language = post.node.frontmatter.lang;
    if (language === 'en') {
      return post;
    }
  });
  const plPostsFiltered = plPosts.filter((n) => n);
  const enPostsFiltered = enPosts.filter((n) => n);
  let previousPl;
  let nextPl;
  let previousEn;
  let nextEn;

  posts.map(async (post, index) => {
    const previous = index === posts.length - 1 ? null : posts[index + 1].node;
    const next = index === 0 ? null : posts[index - 1].node;
    const language = post.node.frontmatter.lang;
    const slug = post.node.fields.slug;

    previousPl =
      plPostsFiltered.findIndex((x) => x.node.fields.slug === slug) - 1
        ? plPostsFiltered.findIndex((x) => x.node.fields.slug === slug) + 1
        : plPostsFiltered.findIndex((x) => x.node.fields.slug === slug) + 1;
    nextPl =
      plPostsFiltered.findIndex((x) => x.node.fields.slug === slug) === 0
        ? null
        : plPostsFiltered.findIndex((x) => x.node.fields.slug === slug) - 1;
    previousEn =
      enPostsFiltered.findIndex((x) => x.node.fields.slug === slug) - 1
        ? enPostsFiltered.findIndex((x) => x.node.fields.slug === slug) + 1
        : enPostsFiltered.findIndex((x) => x.node.fields.slug === slug) + 1;
    nextEn =
      enPostsFiltered.findIndex((x) => x.node.fields.slug === slug) === 0
        ? null
        : enPostsFiltered.findIndex((x) => x.node.fields.slug === slug) - 1;
    createPage({
      path: slug,
      component: blogPost,
      context: {
        posts: posts,
        slug,
        clearSlug: slug.replace(/\/pl/, ''),
        lastBuildTime: result.data.lastBuildTime,
        previous,
        next,
        lang: language,
        plPostsFiltered,
        previousPl: plPostsFiltered[previousPl],
        nextPl: plPostsFiltered[nextPl],
        previousEn: enPostsFiltered[previousEn],
        nextEn: enPostsFiltered[nextEn],
        intl: {
          language: language,
          languages,
          messages: getMessages('./i18n/', post.node.frontmatter.lang),
        },
      },
    });
  });

  const tags = result.data.tagsGroup.group;
  tags.forEach((tag) => {
    createPage({
      path: `/tags/${kebabCase(tag.fieldValue)}/`,
      component: tagTemplate,
      context: {
        tag: tag.fieldValue,
      },
    });
  });
};

exports.onCreatePage = async ({ page, actions, cache }) => {
  const { deletePage, createPage } = actions;
  const buildTimeFromCache = await cache.get('buildTimeCache');

  createPage({
    ...page,
    context: {
      slug: page.path,
      clearSlug: page.path.replace(/\/pl/, ''),
      lastBuildTime: buildTimeFromCache,
      ...page.context,
    },
  });

  if (pattern.test(page.path)) {
    deletePage(page);
  }
};

exports.onCreateNode = ({ node, actions, getNode }) => {
  const { createNodeField } = actions;

  if (node.internal.type === `MarkdownRemark`) {
    const value = createFilePath({ node, getNode });
    createNodeField({
      name: `slug`,
      node,
      value,
    });
  }
};

exports.onCreateWebpackConfig = ({ actions, stage, plugins, loaders }) => {
  // if (stage.startsWith('develop')) {
  //   actions.setWebpackConfig({
  //     resolve: {
  //       alias: {
  //         'react-dom': '@hot-loader/react-dom',
  //       },
  //     },
  //   });
  // }
  if (stage === 'build-html') {
    actions.setWebpackConfig({
      module: {
        rules: [
          {
            test: /canvas/,
            use: loaders.null(),
          },
        ],
      },
    });
  }
  if (stage.startsWith('build-javascript')) {
    actions.setWebpackConfig({
      module: {
        rules: [
          {
            test: /react-spring/,
            sideEffects: true,
          },
        ],
      },
    });
  }
  actions.setWebpackConfig({
    resolve: {
      plugins: [new TsconfigPathsPlugin()],
    },
    plugins: [
      new LoadablePlugin(),
      plugins.define({
        'process.env.name': JSON.stringify(packageJson.name),
        'process.env.version': JSON.stringify(packageJson.version),
        'process.env.gatsby_version': JSON.stringify(
          packageJson.dependencies.gatsby,
        ),
      }),
    ],
  });
};

exports.onCreateBabelConfig = ({ actions }) => {
  actions.setBabelPlugin({
    name: '@babel/plugin-transform-react-jsx',
    options: {
      runtime: 'automatic',
    },
  });
};

// updates only on build
exports.onPostBuild = async ({ graphql }) => {
  const { data } = await graphql(`
    {
      pages: allSitePage {
        nodes {
          path
        }
      }
    }
  `);

  const publicPath = path.join(__dirname, 'public');
  const hash = md5(Math.random().toString(36).substring(7));

  const htmlAndJSFiles = glob.sync(`${publicPath}/**/*.{html,js}`);
  console.log(
    '[onPostBuild] Replacing page-data.json references in the following files:',
  );
  for (const file of htmlAndJSFiles) {
    const stats = await fs.stat(file, 'utf8');
    if (!stats.isFile()) continue;
    console.log(file);
    const content = await fs.readFile(file, 'utf8');
    const result = content
      .replace(/page-data.json/g, `page-data.json?${hash}`)
      .replace(/app-data.json/g, `app-data.json?${hash}`);
    await fs.writeFile(file, result, 'utf8');
  }

  return fs.writeFile(
    path.resolve(__dirname, 'all-pages.txt'),
    data.pages.nodes.map((node) => node.path).join('\n'),
  );
};

exports.sourceNodes = async ({
  actions,
  createContentDigest,
  createNodeId,
}) => {
  const { createNode } = actions;
  // google auth logic
  const scopes = 'https://www.googleapis.com/auth/analytics.readonly';
  const jwt = new google.auth.JWT(
    GOOGLE_EMAIL,
    null,
    GOOGLE_KEY.replace(new RegExp('\\\\n', 'g'), '\n'),
    scopes,
  );
  await jwt.authorize();

  const analyticsReporting = google.analyticsreporting({
    version: 'v4',
    auth: jwt,
  });

  // Analytics Reporting v4 query
  const result = await analyticsReporting.reports.batchGet({
    requestBody: {
      reportRequests: [
        {
          viewId: GOOGLE_VIEWID,
          dateRanges: [
            {
              startDate: '2020-04-01',
              endDate: 'today',
            },
          ],
          metrics: [
            {
              expression: 'ga:pageViews',
            },
            {
              expression: 'ga:sessions',
            },
          ],
          dimensions: [
            {
              name: 'ga:pagePath',
            },
          ],
          orderBys: [
            {
              sortOrder: 'DESCENDING',
              fieldName: 'ga:pageviews',
            },
          ],
        },
      ],
    },
  });

  // Add analytics data to graphql
  const { rows } = result.data.reports[0].data;
  // console.log('rows', rows);
  for (const { dimensions, metrics } of rows) {
    const path = dimensions[0];
    const totalCount = metrics[0].values[0];
    const totalSessions = metrics[0].values[1];
    createNode({
      path,
      totalCount: Number(totalCount),
      totalSessions: Number(totalSessions),
      id: path,
      internal: {
        type: `PageViews`,
        contentDigest: crypto
          .createHash(`md5`)
          .update(JSON.stringify({ path, totalCount }))
          .digest(`hex`),
        mediaType: `text/plain`,
        description: `Page views per path`,
      },
    });
  }

  //gitlab
  const GITLAB_NODE_TYPE = `Gitlab`;
  const api = new Gitlab({
    token: process.env.GITLAB_TOKEN,
  });
  const user = await api.Users.current();
  const projects = await api.Users.projects(user.id, { statistics: true });

  await Promise.all(
    projects.map(async (project) => {
      createNode({
        ...project,
        usedLanguages: await api.Projects.languages(project.id),
        id: createNodeId(`${GITLAB_NODE_TYPE}-${project.id}`),
        parent: null,
        children: [],
        internal: {
          type: GITLAB_NODE_TYPE,
          content: JSON.stringify(project),
          contentDigest: createContentDigest(project),
        },
      });
    }),
  );
};

exports.createSchemaCustomization = ({ actions }) => {
  const { createTypes } = actions;
  const typeDefs = `
  type PopularTag @dontInfer {
    totalCount: Int
    fieldValue: String
  }
  type SummarizedStats @dontInfer {
    commits: Int
    projects: Int
    articles: Int
    articlesPl: Int
    articlesEn: Int
    programmingLanguages: GitlabUsedLanguages
    pages: [PageViews]
    comments: Int
    totalTimeToRead: Int
    totalTimeToReadPl: Int
    totalTimeToReadEn: Int
    totalTags: Int
    totalUniqueTags: Int
    mostPopularTagEn: PopularTag
    mostPopularTagPl: PopularTag
    projectSize: Float
  }
  `;
  createTypes(typeDefs);
};

exports.createResolvers = async ({ cache, createResolvers }) => {
  createResolvers({
    Query: {
      LunrIndex: {
        type: GraphQLJSONObject,
        resolve: (source, args, context, info) => {
          const blogNodes = context.nodeModel.getAllNodes({
            type: `MarkdownRemark`,
          });
          const type = info.schema.getType(`MarkdownRemark`);
          return createIndex(blogNodes, type, cache);
        },
      },
      SummarizedStats: {
        type: 'SummarizedStats',
        resolve: async (source, args, context) => {
          const timeToReadFromCache = await cache.get('timeToReadCache');
          const tagsFromCache = await cache.get('tagsCache');
          const tagsFromCacheEn = await cache.get('tagsCacheEn');
          const tagsFromCachePl = await cache.get('tagsCachePl');
          const projectSizeMB = (await cache.get('projectSizeCache')) / 1000000;
          const getAllGitlab = context.nodeModel.getAllNodes({
            type: 'Gitlab',
          });
          const getMarkdown = context.nodeModel.getAllNodes({
            type: 'MarkdownRemark',
          });
          const mapField = (resource, field) =>
            resource.map((val) => val[field]);
          const allCommits = getAllGitlab.reduce((acc, val) => {
            return acc + val.statistics.commit_count;
          }, 0);

          const allProjects = mapField(getAllGitlab, 'name');
          const allLang = mapField(getAllGitlab, 'usedLanguages');
          const allArticles = mapField(getMarkdown, 'id');
          const languagesFromArticles = getMarkdown.reduce(
            (r, obj) => r.concat(obj.frontmatter.lang),
            [],
          );

          const urlencoded = new URLSearchParams();
          urlencoded.append('website_id', '877');
          urlencoded.append('api_key', process.env.HYVOR_TOKEN);

          const requestOptions = {
            method: 'POST',
            body: urlencoded,
            redirect: 'follow',
          };

          const fetchHyvorData = () => {
            return fetch(
              'https://talk.hyvor.com/api/v1/comments',
              requestOptions,
            )
              .then((response) => {
                return response.json();
              })
              .then((result) => result)
              .catch((error) => console.log('error', error));
          };

          const hyvorComments = await fetchHyvorData();

          const filteredPagesWithGA = context.nodeModel
            .getAllNodes({
              type: 'SitePage',
            })
            .map((x) => {
              const item = context.nodeModel
                .getAllNodes({
                  type: 'PageViews',
                })
                .find((item) => item.path === x.path);
              if (item) {
                return item;
              }
            })
            .filter((item) => item !== undefined)
            .sort((a, b) => {
              return b.totalCount - a.totalCount;
            });

          function round(number, places) {
            return +(Math.round(number + 'e+' + places) + 'e-' + places);
          }
          const programmingLanguages = allLang.reduce(
            (acc, val) => {
              for (const key in acc) {
                const checkValue = val[key] ?? 0;
                acc[key] = round(
                  acc[key] +
                    checkValue /
                      allLang.filter((val) => {
                        return val[key];
                      }).length,
                  2,
                );
              }
              return acc;
            },
            {
              CSS: 0,
              Dockerfile: 0,
              HTML: 0,
              JavaScript: 0,
              Pug: 0,
              Shell: 0,
              TypeScript: 0,
            },
          );
          for (const property in programmingLanguages) {
            programmingLanguages[property] =
              programmingLanguages[property] / 10;
          }
          const sumValues = Object.values(programmingLanguages).reduce(
            (a, b) => a + b,
          );
          const sumProgrammingLanguages = Object.fromEntries(
            Object.entries(programmingLanguages).map(([k, v]) => [
              k,
              round((v / sumValues) * 100, 2),
            ]),
          );

          const totalTags = tagsFromCache.reduce(
            (acc, val) => acc + val.totalCount,
            0,
          );
          const mostPopularTagByLang = (data) => {
            return data.reduce(
              (prev, current) =>
                prev.totalCount > current.totalCount ? prev : current,
              0,
            );
          };

          const timeToRead = timeToReadFromCache.reduce(
            (acc, val) => acc + val.timeToRead,
            0,
          );
          const timeToReadByLang = (lang) => {
            return timeToReadFromCache
              .filter((val) => val.lang === lang)
              .reduce((acc, val) => acc + val.timeToRead, 0);
          };

          return {
            commits: allCommits,
            projects: allProjects.length,
            articles: allArticles.length,
            articlesPl: languagesFromArticles.filter((value) => value === 'pl')
              .length,
            articlesEn: languagesFromArticles.filter((value) => value === 'en')
              .length,
            programmingLanguages: sumProgrammingLanguages,
            pages: filteredPagesWithGA,
            comments: hyvorComments.data.length,
            totalTimeToRead: timeToRead,
            totalTimeToReadPl: timeToReadByLang('pl'),
            totalTimeToReadEn: timeToReadByLang('en'),
            totalTags,
            totalUniqueTags: tagsFromCache.length,
            mostPopularTagEn: mostPopularTagByLang(tagsFromCacheEn),
            mostPopularTagPl: mostPopularTagByLang(tagsFromCachePl),
            projectSize: projectSizeMB.toFixed(1),
          };
        },
      },
    },
  });
};

const createIndex = async (blogNodes, type, cache) => {
  const cacheKey = `IndexLunr`;
  const cached = await cache.get(cacheKey);
  if (cached) {
    return cached;
  }
  const documents = [];
  const store = {};

  for (const node of blogNodes) {
    const { slug } = node.fields;
    const { title, lang } = node.frontmatter;
    const [html, excerpt] = await Promise.all([
      type.getFields().html.resolve(node),
      type.getFields().excerpt.resolve(node, { pruneLength: 80 }),
    ]);

    documents.push({
      slug,
      title: node.frontmatter.title,
      lang: node.frontmatter.lang,
      content: striptags(html),
    });

    store[slug] = {
      lang,
      title,
      excerpt,
    };
  }
  const index = lunr(function () {
    this.ref('slug');
    this.field('title');
    this.field('lang');
    this.field('content');
    for (const doc of documents) {
      this.add(doc);
    }
  });

  const json = { index: index.toJSON(), store };
  await cache.set(cacheKey, json);
  return json;
};
