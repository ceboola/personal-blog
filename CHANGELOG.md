# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.6.0](https://gitlab.com/ceboola/personal-blog/compare/v1.5.0...v1.6.0) (2021-06-24)

### Features

- added JSON-LD schema for blog post ([c4e90fa](https://gitlab.com/ceboola/personal-blog/commit/c4e90fa52b643814eac9a2c4bed980a8e8d83ed0))
- added rss for en/pl articles ([72c31f6](https://gitlab.com/ceboola/personal-blog/commit/72c31f64ee8a958a43a07007128dee79ff577263))
- added star rating component for articles, fixed loading comments ([9671a04](https://gitlab.com/ceboola/personal-blog/commit/9671a042809acf2f8acdcbfbdaf7e4fab51402fc))

### Bug Fixes

- **deps:** pin dependency gatsby-plugin-advanced-sitemap to 2.0.0 ([424104e](https://gitlab.com/ceboola/personal-blog/commit/424104eae1582a9fb8a3c51b0d0421fc02f8f1d0))
- **deps:** pin dependency gatsby-plugin-react-intl to 3.0.0 ([c1f5956](https://gitlab.com/ceboola/personal-blog/commit/c1f5956c67d2a52c5154e31befee47154c69c329))
- **deps:** update dependency @gitbeaker/node to v29.2.4 ([d2460ff](https://gitlab.com/ceboola/personal-blog/commit/d2460ff964019b88444e704144e8c5a6e9d3927a))
- **deps:** update dependency @gitbeaker/node to v29.3.0 ([9398c11](https://gitlab.com/ceboola/personal-blog/commit/9398c118bd17e61cf88a4562af6bb8d9b06dbea6))
- **deps:** update dependency @gitbeaker/node to v30 ([45a5920](https://gitlab.com/ceboola/personal-blog/commit/45a592049a637b1e8e4fa10e231c3ca74f3506f0))
- **deps:** update dependency @gitbeaker/node to v30.5.0 ([039c5b8](https://gitlab.com/ceboola/personal-blog/commit/039c5b8eb72de5b299be9745a68d83263df63053))
- **deps:** update dependency firebase to v8.6.3 ([ec97956](https://gitlab.com/ceboola/personal-blog/commit/ec97956fbdf5f53e0208f6a6b73b17afea4ef179))
- **deps:** update dependency firebase to v8.6.8 ([0d602bd](https://gitlab.com/ceboola/personal-blog/commit/0d602bd9f1010f0543a990e887787e4efa55991f))
- **deps:** update dependency gatsby to v3.6.2 ([a605f65](https://gitlab.com/ceboola/personal-blog/commit/a605f65b2eda74126faedc4ee14d3eb8acb27473))
- **deps:** update dependency googleapis to v74.2.0 ([48c6be6](https://gitlab.com/ceboola/personal-blog/commit/48c6be6f3f438be2651a232ede4864469a6885a8))
- **deps:** update dependency googleapis to v75 ([45fc93f](https://gitlab.com/ceboola/personal-blog/commit/45fc93f8c5207cdb0a37ab4d4ac6193368a95f2f))
- **deps:** update dependency googleapis to v78 ([a38dcd2](https://gitlab.com/ceboola/personal-blog/commit/a38dcd2f017abadda36e59c0ec774948eab16c25))
- **deps:** update dependency react-spring to v9.2.0 ([e6cd198](https://gitlab.com/ceboola/personal-blog/commit/e6cd198015fd7950698a81ad3cacdbfead581558))
- **deps:** update dependency react-spring to v9.2.1 ([b4ff6e8](https://gitlab.com/ceboola/personal-blog/commit/b4ff6e8bc150f9e8f7836d95788f46792641dee0))
- **deps:** update dependency react-spring to v9.2.2 ([97161d7](https://gitlab.com/ceboola/personal-blog/commit/97161d72323501c7637323ad2dd21dacd52f71b4))
- **deps:** update dependency react-spring to v9.2.3 ([f262dbb](https://gitlab.com/ceboola/personal-blog/commit/f262dbb6c28c20ba2e97293e7f04f7942a8ec90d))
- **deps:** update dependency react-stickynode to v3.1.0 ([6ad99e2](https://gitlab.com/ceboola/personal-blog/commit/6ad99e26d8ac983a40205725da0b555c27b2d42e))
- **deps:** update dependency striptags to v3.2.0 ([6d19eb7](https://gitlab.com/ceboola/personal-blog/commit/6d19eb7b07e7d86c5012435156a273fac6cb814e))
- fixed memoization on context values ([4965637](https://gitlab.com/ceboola/personal-blog/commit/49656376b04b0e598389be0e00dccfee402b7cc4))
- migrated from gatsby-plugin-intl to gatsby-plugin-react-intl ([698ef3a](https://gitlab.com/ceboola/personal-blog/commit/698ef3ab590307f4995f2f001ec9e780294a7bff))
- upgrade Gitlab Manages Apps to the latest version ([28a3fef](https://gitlab.com/ceboola/personal-blog/commit/28a3fef66b0763b3ac6ed59d08cf23bb99c84b6b))

### Chore

- **deps:** pin dependencies ([c3f7ebf](https://gitlab.com/ceboola/personal-blog/commit/c3f7ebf83e9fd561de077b5f74427bc625a49d6e))
- **deps:** update babel monorepo to v7.14.5 ([031e4f1](https://gitlab.com/ceboola/personal-blog/commit/031e4f17976dd8d19854fe5519bb5dd3be401352))
- **deps:** update dependency @graphql-codegen/typescript to v1.22.3 ([694fa22](https://gitlab.com/ceboola/personal-blog/commit/694fa223c13a501f0c4cb0326b57ca0269ee61b1))
- **deps:** update dependency @testing-library/jest-dom to v5.13.0 ([9502627](https://gitlab.com/ceboola/personal-blog/commit/95026271007feabfc26f9934075b527fc2a1cf0c))
- **deps:** update dependency @testing-library/jest-dom to v5.14.1 ([71571f9](https://gitlab.com/ceboola/personal-blog/commit/71571f9626847c7c45f7a551903b89547d203f59))
- **deps:** update dependency @testing-library/react to v12 ([4a7b876](https://gitlab.com/ceboola/personal-blog/commit/4a7b876411a221cb5feb22bde11a41f2e69edaaa))
- **deps:** update dependency @types/react to v17.0.11 ([6ab4256](https://gitlab.com/ceboola/personal-blog/commit/6ab4256049a23845aa6f970361ed879ba89c858f))
- **deps:** update dependency @types/react to v17.0.9 ([ef0983c](https://gitlab.com/ceboola/personal-blog/commit/ef0983c38f3774cc511d8beb4ef389795fb1d734))
- **deps:** update dependency @types/react-dom to v17.0.8 ([d3d54b1](https://gitlab.com/ceboola/personal-blog/commit/d3d54b192b40d8b5467a77912c8a58312eb18196))
- **deps:** update dependency @types/styled-components to v5.1.10 ([85a9f64](https://gitlab.com/ceboola/personal-blog/commit/85a9f642fd30f82be925a4e329dc1963f81165fc))
- **deps:** update dependency eslint to v7.29.0 ([b920daa](https://gitlab.com/ceboola/personal-blog/commit/b920daa3aa2bbef676ffd49250d1c9660884ca52))
- **deps:** update dependency eslint-plugin-import to v2.23.4 ([cecb643](https://gitlab.com/ceboola/personal-blog/commit/cecb643039cce7d6ab2f6649917bd666d2af96a7))
- **deps:** update dependency eslint-plugin-react to v7.24.0 ([8cd1f43](https://gitlab.com/ceboola/personal-blog/commit/8cd1f43e4f9e49415b644a00a7771c5bf7632192))
- **deps:** update dependency gatsby-plugin-webpack-bundle-analyser-v2 to v1.1.24 ([295cc33](https://gitlab.com/ceboola/personal-blog/commit/295cc3358b68dd7899f5e00ca2506b88f221080f))
- **deps:** update dependency typescript to v4.3.2 ([b1c140c](https://gitlab.com/ceboola/personal-blog/commit/b1c140c3e1226e8c28f1b636ae67509b5a265a7b))
- **deps:** update dependency typescript to v4.3.4 ([67e84f5](https://gitlab.com/ceboola/personal-blog/commit/67e84f505730830e793a488864596201f773c0bd))
- **deps:** update gatsby monorepo ([2d31842](https://gitlab.com/ceboola/personal-blog/commit/2d318428a9e4970d1d4534f40a2661031611b2ea))
- **deps:** update jest monorepo ([9f386f6](https://gitlab.com/ceboola/personal-blog/commit/9f386f643218e354bcf14e2b13f60d96dd7f2f81))
- **deps:** update jest monorepo to v27.0.5 ([dc71be2](https://gitlab.com/ceboola/personal-blog/commit/dc71be26f1f744a523f5801338f102e172557a0b))
- **deps:** update typescript-eslint monorepo to v4.26.0 ([5db220c](https://gitlab.com/ceboola/personal-blog/commit/5db220c1df8103df03b51484561e030ae95a1b0c))
- **deps:** update typescript-eslint monorepo to v4.26.1 ([196d6c1](https://gitlab.com/ceboola/personal-blog/commit/196d6c1f158b85b65a32afe02694c11e19933230))
- **deps:** update typescript-eslint monorepo to v4.28.0 ([e603760](https://gitlab.com/ceboola/personal-blog/commit/e6037600fcd192ae9355663c54091cdbbb18db09))

## [1.5.0](https://gitlab.com/ceboola/personal-blog/compare/v1.4.0...v1.5.0) (2021-05-26)

### Features

- added image optimize script for node ([eb436cd](https://gitlab.com/ceboola/personal-blog/commit/eb436cd3f663798d863d4262dfbc3c70d316243d))
- added new article about integrating Renovate with gitlab ([0cd8d72](https://gitlab.com/ceboola/personal-blog/commit/0cd8d72b5476c482e5afc243c5685622a60c78d9))
- added new article called "Gitlab API on Gatsby site", fixed TagsList styles ([9fd851f](https://gitlab.com/ceboola/personal-blog/commit/9fd851f17993afc67749dc9d0bdb4042bb63dddb))
- added new CurrentProjectStats template, compressed svg, moved repeating queries to graphql fragments ([76db824](https://gitlab.com/ceboola/personal-blog/commit/76db8249d3cce6dc2d2eaad9d8a262a17a0d095c))
- added page stats ([1ba6ca6](https://gitlab.com/ceboola/personal-blog/commit/1ba6ca67178fb4a4cde4ff744e644f2885b8892d))
- added pageviews/sessions from GA in footer for each site ([7c532cc](https://gitlab.com/ceboola/personal-blog/commit/7c532ccd728873faeea2a4387c4cc40c108712fb))
- migrated from gatsby-image to gatsby-plugin-image ([762d005](https://gitlab.com/ceboola/personal-blog/commit/762d0058995806d93c8aabcb19b69e73ddc64142))

### Bug Fixes

- changed docker image (added git package) for cloc ([ab8b629](https://gitlab.com/ceboola/personal-blog/commit/ab8b629c6b2bf832f5f27a97d3566bd7e9ae3fc0))
- fix accessibility after lighthouse audit ([aeb5dcf](https://gitlab.com/ceboola/personal-blog/commit/aeb5dcf4a409cfbf0e044abc18a0e70586b289b5))
- update readme ([aec5549](https://gitlab.com/ceboola/personal-blog/commit/aec5549e5abb33ed3a1d08c6acd46eeeb40693d2))
- **deps:** update dependency @gitbeaker/node to v28.4.1 ([3a25e8b](https://gitlab.com/ceboola/personal-blog/commit/3a25e8b47926c791f409ef5706d2824c472f61d5))
- **deps:** update dependency @gitbeaker/node to v29 ([50458ee](https://gitlab.com/ceboola/personal-blog/commit/50458eebfbe4fe0d3d09d578817679259e22b95b))
- **deps:** update dependency @loadable/component to v5.15.0 ([414a91c](https://gitlab.com/ceboola/personal-blog/commit/414a91cb8debbc268eba9edb04877b1aee0ecd67))
- **deps:** update dependency canvas to v2.8.0 ([2e2081e](https://gitlab.com/ceboola/personal-blog/commit/2e2081e15c252976a58e416e0542496892270f49))
- **deps:** update dependency dotenv to v10 ([921f850](https://gitlab.com/ceboola/personal-blog/commit/921f8503df734266cf6754e5a4d262a08bd49806))
- **deps:** update dependency dotenv to v8.6.0 ([3e4f006](https://gitlab.com/ceboola/personal-blog/commit/3e4f0066c8ef1c389b6bbb424a49f7e324204d3a))
- **deps:** update dependency dotenv to v9 ([e40a16d](https://gitlab.com/ceboola/personal-blog/commit/e40a16da2fd1986d0be80c3b96efba9c56bae205))
- **deps:** update dependency dotenv to v9 ([4e00470](https://gitlab.com/ceboola/personal-blog/commit/4e00470a10855337f091bfb6ef14eb22c2d535f6))
- **deps:** update dependency dotenv to v9.0.2 ([426c8cd](https://gitlab.com/ceboola/personal-blog/commit/426c8cdaacabf09183712444f2191ea4a6a2bac1))
- **deps:** update dependency firebase to v8.5.0 ([5d0e1fa](https://gitlab.com/ceboola/personal-blog/commit/5d0e1fa43041e49bf52babd7d5a552aa51cd7ffb))
- **deps:** update dependency firebase to v8.6.2 ([d5878e7](https://gitlab.com/ceboola/personal-blog/commit/d5878e7b8ef3586c4d5d765f8b9c6a3a87a44c07))
- **deps:** update dependency gatsby-plugin-firebase to ^0.2.0-rc ([a85a6ae](https://gitlab.com/ceboola/personal-blog/commit/a85a6aeebe40788684a90166342ed6816942375e))
- **deps:** update dependency gatsby-plugin-svg-sprite-loader to ^0.2.0 ([a47d69c](https://gitlab.com/ceboola/personal-blog/commit/a47d69ca8016d8eaaeb5eabce553203458bf21f2))
- **deps:** update dependency googleapis to v73 ([d5fcf19](https://gitlab.com/ceboola/personal-blog/commit/d5fcf19cc9379eb8d27ab7f1ffb3a9f7f3387226))
- **deps:** update dependency googleapis to v74 ([65bf519](https://gitlab.com/ceboola/personal-blog/commit/65bf519d55de1f696f81f5749396dbc4a81649d2))
- **deps:** update dependency react-firebaseui to v5 ([3f25a42](https://gitlab.com/ceboola/personal-blog/commit/3f25a42abb62c9b8e12ecf626ea4008df24f6d81))
- **deps:** update dependency react-intersection-observer to v8.32.0 ([9ad07b1](https://gitlab.com/ceboola/personal-blog/commit/9ad07b14ed856b555c5ffae1a0e46981f8e8448a))
- **deps:** update gatsby monorepo to v3.4.1 ([d7dec86](https://gitlab.com/ceboola/personal-blog/commit/d7dec8648b923713951514b98a88e7f49ec095de))
- **deps:** update typography-js monorepo ([e18102e](https://gitlab.com/ceboola/personal-blog/commit/e18102e7caef55b468c4bef6618ca88f08cd04e2))
- fixed animation on progress value ([20901b6](https://gitlab.com/ceboola/personal-blog/commit/20901b676b6ed33f2d62e90bb45a6427cdc08a44))
- fixed eslint extensions to be valid in v8.0.0 ([27f0726](https://gitlab.com/ceboola/personal-blog/commit/27f0726d8e9c0075a72b1d1dce728d2142f640e6))
- fixed styles in footer and popular artciles sections ([9747bf5](https://gitlab.com/ceboola/personal-blog/commit/9747bf5d0ffec353bfeadd998231d3fba98e6626))
- ignore specific dependencies for renovate ([f787616](https://gitlab.com/ceboola/personal-blog/commit/f787616d93a47c7bf9035d77c7cd05f21167b7da))
- preventing null values on new pages when no one visit it before (pageviews GA) ([0ec63d2](https://gitlab.com/ceboola/personal-blog/commit/0ec63d2fa0be479884277f5684055b246020f55e))
- **deps:** update dependency xmldom to ^0.6.0 ([c9515a5](https://gitlab.com/ceboola/personal-blog/commit/c9515a573d93405637f1f4fbf252106a39f185e4))

### Chore

- **deps:** pin dependencies ([aec31dc](https://gitlab.com/ceboola/personal-blog/commit/aec31dc08099a267ebaea43236a7d01314a16170))
- **deps:** pin dependencies ([1b843cf](https://gitlab.com/ceboola/personal-blog/commit/1b843cff226ec83043a20fb54151db92e6eb530e))
- **deps:** pin dependencies ([e3c20ad](https://gitlab.com/ceboola/personal-blog/commit/e3c20ad43ec38fe24938c4b372ba39059e85016f))
- **deps:** pin dependency imagemin-lint-staged to 0.4.0 ([09ac06b](https://gitlab.com/ceboola/personal-blog/commit/09ac06bfc4938de64ad5122fea050cff4e79d087))
- **deps:** update babel monorepo to v7.14.2 ([3427bef](https://gitlab.com/ceboola/personal-blog/commit/3427befa05d2545e768f587b0f8241a3b6721465))
- **deps:** update ceboola/lighthouse docker tag to v1.2 ([9b5b220](https://gitlab.com/ceboola/personal-blog/commit/9b5b220be4425bcbe34a4a7933697561f6f5aee1))
- **deps:** update dependency @loadable/webpack-plugin to v5.15.0 ([3de6cbc](https://gitlab.com/ceboola/personal-blog/commit/3de6cbc1e7c4bc21e4f130511d8fc6a79fea7c67))
- **deps:** update dependency @testing-library/react to v11.2.7 ([25e23e5](https://gitlab.com/ceboola/personal-blog/commit/25e23e51dae81627219f7cf16d20c9e1e340491b))
- **deps:** update dependency @types/jest to ^26.0.22 ([906a81d](https://gitlab.com/ceboola/personal-blog/commit/906a81db1c181b085fc10eeb03bf254210d10fa9))
- **deps:** update dependency @types/react to v17.0.5 ([a03a4c2](https://gitlab.com/ceboola/personal-blog/commit/a03a4c261bb21a01d9b3b35be187329e955216ad))
- **deps:** update dependency @types/react to v17.0.7 ([09afbba](https://gitlab.com/ceboola/personal-blog/commit/09afbba6807b630a9c05c78c43e65dc8ccc8f989))
- **deps:** update dependency @types/react-dom to v17.0.5 ([9838699](https://gitlab.com/ceboola/personal-blog/commit/9838699070bfa1d5bfaab0496c097c526bbf65c9))
- **deps:** update dependency babel-preset-gatsby to v1 ([ff9820d](https://gitlab.com/ceboola/personal-blog/commit/ff9820d2c79962cd83808f37b10db2173102c6c0))
- **deps:** update dependency concurrently to v6 ([a16fe88](https://gitlab.com/ceboola/personal-blog/commit/a16fe887bff987cc487ee334628fb2b60e19f3ee))
- **deps:** update dependency concurrently to v6.2.0 ([7226859](https://gitlab.com/ceboola/personal-blog/commit/7226859348a4c865fee10290da045dca445291fa))
- **deps:** update dependency eslint to v7.27.0 ([cbf5b8c](https://gitlab.com/ceboola/personal-blog/commit/cbf5b8c3c2114067c991a9fae68fc18bf9662f65))
- **deps:** update dependency eslint-config-prettier to v8 ([c70d62e](https://gitlab.com/ceboola/personal-blog/commit/c70d62e89bb5acf5b57231246288562181c00d71))
- **deps:** update dependency eslint-plugin-import to v2.23.3 ([d5113b3](https://gitlab.com/ceboola/personal-blog/commit/d5113b3f8f24850575a68c29c8d0b294b6e83a9b))
- **deps:** update dependency graphql to v15 ([86b5112](https://gitlab.com/ceboola/personal-blog/commit/86b5112cdb595f4c27f4a511835fc0bb7318d0aa))
- **deps:** update dependency husky to v6 ([1bcefa9](https://gitlab.com/ceboola/personal-blog/commit/1bcefa910ac281b5bd9fcba2d8e2c2b666d89a00))
- **deps:** update dependency lint-staged to v11 ([2bca40e](https://gitlab.com/ceboola/personal-blog/commit/2bca40ecbb054ed1ef401fa2a5d2b43c78a362cd))
- **deps:** update dependency prettier to v2.3.0 ([5c19415](https://gitlab.com/ceboola/personal-blog/commit/5c19415407d1ed5c8c7d79f801b3da1b1413ca3e))
- **deps:** update dependency standard-version to v9.3.0 ([535e14f](https://gitlab.com/ceboola/personal-blog/commit/535e14fe43d84188a8917dd6de16d4ce270a6c5a))
- **deps:** update dependency styled-components to v5.3.0 ([902971d](https://gitlab.com/ceboola/personal-blog/commit/902971dd51c1a5ade44ed3fd2ee880e23e768b70))
- **deps:** update gatsby monorepo ([821d1e1](https://gitlab.com/ceboola/personal-blog/commit/821d1e1ca5dd4211709cc4d3ad2e711ab3f043da))
- **deps:** update gatsby monorepo ([d0fe332](https://gitlab.com/ceboola/personal-blog/commit/d0fe3321137d1d664ed8a019b255dae387962c36))
- **deps:** update jest monorepo to v27 ([1b0686e](https://gitlab.com/ceboola/personal-blog/commit/1b0686e241e5cc3a67465fc796d0c0660b6f9192))
- **deps:** update typescript-eslint monorepo to v4.22.1 ([af39ba9](https://gitlab.com/ceboola/personal-blog/commit/af39ba94b594528c5e72e864ab2ec2509809dbd8))
- **deps:** update typescript-eslint monorepo to v4.25.0 ([13d8fce](https://gitlab.com/ceboola/personal-blog/commit/13d8fcec8f4843a8b68a29c0eddd02e53747469d))
- added imagemin-lint-staged to compress images on commits ([44e0e68](https://gitlab.com/ceboola/personal-blog/commit/44e0e68fbdc5606591498ec8d66afc56f11bf349))

## [1.4.0](https://gitlab.com/ceboola/personal-blog/compare/v1.3.0...v1.4.0) (2021-04-28)

### Features

- added calculation of all lang in gitlab projects showed on simple react-spring graph, refactor for DoYouKnow component with accepts now templates and showing randomly one of them, added VerticalProgress component ([ceb9705](https://gitlab.com/ceboola/personal-blog/commit/ceb9705a80d0fb94bef3b9534be247ecb3e1c337))

## [1.3.0](https://gitlab.com/ceboola/personal-blog/compare/v1.2.0...v1.3.0) (2021-04-23)

### Features

- added DoYouKnow component with fetched data from Gitlab API by node createResolvers, added content on contact page ([2913ed1](https://gitlab.com/ceboola/personal-blog/commit/2913ed1987aa0f39fb8ae934c3b2229a5c36269c))

### Bug Fixes

- fixed build image (added missing packages python3 \ py3-pip \ build-base \ g++ \ cairo-dev \ jpeg-dev \ pango-dev \ giflib-dev \ for node-canvas) ([e462d6e](https://gitlab.com/ceboola/personal-blog/commit/e462d6e5f1cd43af189d8f243d2b74eb23dc2b63))
- fixed clamping text on popular aticles, added webpack loader for canvas, back to dompurify intstead of xss ([aad2586](https://gitlab.com/ceboola/personal-blog/commit/aad2586b005fdc24f95e0500afeaee89334c1051))
- fixed long notifications in MessageHub component, fixed some styles for mobile ([bfdcb20](https://gitlab.com/ceboola/personal-blog/commit/bfdcb20115b4e2198ff58a33ca40d92c02e85ada))
- moved firebaseui, react-sticky, react-textfit to separated loadable chunks ([546a006](https://gitlab.com/ceboola/personal-blog/commit/546a006de5f69336a146f0d3ade811a45c14a732))

### Chore

- changed dependency from dompurify to xss for node support ([fdc246f](https://gitlab.com/ceboola/personal-blog/commit/fdc246f115679e1a1ab5267f0e35c9760d228c1a))

## [1.2.0](https://gitlab.com/ceboola/personal-blog/compare/v1.1.0...v1.2.0) (2021-04-18)

### Features

- added unique hash to page-data.json,html,js resources for busting hard cache from browser ([0bb14da](https://gitlab.com/ceboola/personal-blog/commit/0bb14daa85942330e1deb478e332f50782336a45))
- new google analytics id, xml dom parser for i18n custom xml tags, removed redundant drilling props used instead context, seo improvements, added content for aboutme page, added new rules for eslint about xss, sanitized dangerouslyInnerHTML ([b53fbd5](https://gitlab.com/ceboola/personal-blog/commit/b53fbd55e01c0d034044153b7ac0ab84e13b93eb))

### Bug Fixes

- fixed cache policy to gatsby guidelines, changed lighthouse measurement probes, changed image in Dockerfile ([98ba94f](https://gitlab.com/ceboola/personal-blog/commit/98ba94f95f53bffa65347e36341418dd2693232f))
- fixed nginx cache control for files ([d5462d1](https://gitlab.com/ceboola/personal-blog/commit/d5462d152809fe2ea519cfac3e6af7d9c390d2bc))

## [1.1.0](https://gitlab.com/ceboola/personal-blog/compare/v1.0.0...v1.1.0) (2021-04-14)

### Features

- added article from queue, added app version from webpack plugin, fixed links to tags, redesign ArticleStart component, generated types, changed font to Ubuntu ([73436d7](https://gitlab.com/ceboola/personal-blog/commit/73436d77048aef34a79b6e589450fb4f058103ca))

### Bug Fixes

- moved newsletter subscription to backend side -> firebase secure rules ([0a56a1b](https://gitlab.com/ceboola/personal-blog/commit/0a56a1b13ee81c85fad4b7dff5491fb5d98fda3f))

## [1.0.0](https://gitlab.com/ceboola/personal-blog/compare/v0.2.11...v1.0.0) (2021-04-10)

### Features

- added unsubscribe page, added helper for gettings params from url, updated i18n ([4d2fbaa](https://gitlab.com/ceboola/personal-blog/commit/4d2fbaad7befa403facc5cf4e125ccc292e5e85a))

### Bug Fixes

- fixed transition blank page between animation ([d258c6c](https://gitlab.com/ceboola/personal-blog/commit/d258c6c53ab4d68466dab6956f06d6fa35d132be))

### [0.2.11](https://gitlab.com/ceboola/personal-blog/compare/v0.2.10...v0.2.11) (2021-04-03)

### Features

- added Dashboard component for logged in users + AdminPanel component only for admins, added Confetti animation component, moved Loader component to commons, updated i18n ([3b993fb](https://gitlab.com/ceboola/personal-blog/commit/3b993fb6bd32a3dff0b7351a7d5eda5aae697fcd))
- added login/signup and newsletter components combined with firebase, added MessageHub component which shows information about success/errors on site ([eb8bde6](https://gitlab.com/ceboola/personal-blog/commit/eb8bde62fb293176f5c8b83b37954a2ecff846a0))

### Bug Fixes

- fixed ConfettiAnimation rerendering on Dashboard, updated newsletter form to backend restrictions, updated i18n, separated dashboard to user and admin UI ([4b2e065](https://gitlab.com/ceboola/personal-blog/commit/4b2e06507bc097041ceb5b2196c40797bd45ad60))

### [0.2.10](https://gitlab.com/ceboola/personal-blog/compare/v0.2.9...v0.2.10) (2021-03-06)

### Features

- added missing pages Contact & About, added useDeviceDetect hook, implemented active link in navigation ([ccd97ba](https://gitlab.com/ceboola/personal-blog/commit/ccd97ba8302e49ecce1dc3d41d9768e07a241459))
- added react-spring page transitions, support for webp images ([5c986db](https://gitlab.com/ceboola/personal-blog/commit/5c986dbd74c15a15f34bef12e48d8b8c67a3c1b7))
- added search component, added parallax effect in articles ([0445441](https://gitlab.com/ceboola/personal-blog/commit/0445441f15f91675fb4297f150b2138e07017b09))
- added ShareMedia component with sticky behavior ([a421232](https://gitlab.com/ceboola/personal-blog/commit/a421232a549edb7bb28ff9a2782c0db2635a0b5f))
- increased performance: changed scroll listeners to intersection observers, added hooks: useThrottle and useWindowScroll ([129e8f9](https://gitlab.com/ceboola/personal-blog/commit/129e8f94ff53ff6252c0582a171ea71376aa801b))

### Chore

- updated dependencies, updated husky to v5 with cli migration ([b154638](https://gitlab.com/ceboola/personal-blog/commit/b1546389ae7eef163a1603cd034f37d00a144e53))

### [0.2.9](https://gitlab.com/ceboola/personal-blog/compare/v0.2.8...v0.2.9) (2021-01-15)

### Features

- added custom babel config for better perf on bundles, compressed svgs ([d55f069](https://gitlab.com/ceboola/personal-blog/commit/d55f0691e2fb0ce3be9227d8950c6ef0b3677c51))
- added loadable-components for better splitting components, increased lighthouse report values, added experimental flags for gatsby developing ([4974780](https://gitlab.com/ceboola/personal-blog/commit/49747808ee7f8ea5246111d704cab5fb776b52d2))
- introduced lint-staged for better perfomance when commits with husky, updated README ([2515528](https://gitlab.com/ceboola/personal-blog/commit/25155284f24311915b405bf9488eabbeb07f9b6f))
- introduced new jsx transform from react 17 ([3df95fc](https://gitlab.com/ceboola/personal-blog/commit/3df95fcb14833cbe788cae1390177af636c2e3ea))
- introduced new rules at eslint and tsconfig, updated files to new rules, new useClientRect hook ([8f32305](https://gitlab.com/ceboola/personal-blog/commit/8f32305f0faa29b0ba41642b3be0ff476cd5421c))

### Bug Fixes

- fixed dockerfile with sharp/vips problem on build, removed console logs ([e1272d9](https://gitlab.com/ceboola/personal-blog/commit/e1272d9d244d4611b1887ff7a376258d818d64bc))
- fixed typescript missmatch between types ([6f27989](https://gitlab.com/ceboola/personal-blog/commit/6f27989614457d13181fe9b07c4dc2cedb1faeff))

### [0.2.8](https://gitlab.com/ceboola/personal-blog/compare/v0.2.7...v0.2.8) (2021-01-09)

### Features

- added mobile version, refactored timetoread common component, added hooks: useWindowSize, useScrollPosition, updated i18n ([ad04ab6](https://gitlab.com/ceboola/personal-blog/commit/ad04ab6e8a8e03976a78a5f699b826df70b357f8))

### [0.2.7](https://gitlab.com/ceboola/personal-blog/compare/v0.2.6...v0.2.7) (2020-12-26)

### Features

- added formatting for big numbers, pluralized i18n content ([b35c532](https://gitlab.com/ceboola/personal-blog/commit/b35c532f94642d3c6274910e2861f4cbe919cb10))
- added i18n en/pl for whole site ([565e685](https://gitlab.com/ceboola/personal-blog/commit/565e685ed28a62a241e59973f38b67594946e858))
- added time to read feature, added flaticon in footer ([eb940ce](https://gitlab.com/ceboola/personal-blog/commit/eb940ceaeeb38753335ca47f0c0223273c43547c))

### Bug Fixes

- added custom hook for mismatch between the initial, non-hydrated UI ([b76a80b](https://gitlab.com/ceboola/personal-blog/commit/b76a80b169b78ffa0378061bc714375b67db5d92))
- fixed graphql version to be same in all dependencies(missmatch between gatsby and graphql-codegen), generated new graphql types ([588b3eb](https://gitlab.com/ceboola/personal-blog/commit/588b3eb2a6c8dddd7d4f18fcf76105505d641c6f))

### Chore

- update dependencies ([ddc6478](https://gitlab.com/ceboola/personal-blog/commit/ddc64786b615480b0aa5a95b0e707f2e6fa24922))

### [0.2.6](https://gitlab.com/ceboola/personal-blog/compare/v0.2.5...v0.2.6) (2020-11-25)

### Features

- added auto-sorting import in ts/tsx files by eslint plugin, reformated imports ([7756c89](https://gitlab.com/ceboola/personal-blog/commit/7756c8920023ea360e8990ebecf5f86f7e0f494c))

### Bug Fixes

- added missing packages for Dockerfile ([fe576fe](https://gitlab.com/ceboola/personal-blog/commit/fe576feb1e171364a23ee91facc5a5d2c8d2ec16))
- missing .env variable file on build pipeline ([cec518d](https://gitlab.com/ceboola/personal-blog/commit/cec518d59bf593c2a4a5c59fb3f0705cfdfd3ef9))
- regex for google analytics private key ([f9e728c](https://gitlab.com/ceboola/personal-blog/commit/f9e728c9aae7f414b581eb853cd912d2ed3519a7))

### [0.2.5](https://gitlab.com/ceboola/personal-blog/compare/v0.2.4...v0.2.5) (2020-11-19)

### Features

- added PageStats via Google, added useDimensions ([b7744ba](https://gitlab.com/ceboola/personal-blog/commit/b7744ba9a8eba805f37723565f6424acf434c50a))
- added spinning loader on fetching comments, resolved TS errors ([f6be1f5](https://gitlab.com/ceboola/personal-blog/commit/f6be1f5b3db6ea3a7a336554668198a0e7dce821))

### Chore

- updated package.json dependencies ([d6f97c0](https://gitlab.com/ceboola/personal-blog/commit/d6f97c0050d8903e482cc2b1770703d1a5bd88c2))

### [0.2.4](https://gitlab.com/ceboola/personal-blog/compare/v0.2.3...v0.2.4) (2020-10-21)

### Features

- added context PostsProvider, added preloading fonts to avoid FOUC, introduced common components CommonButton, CommonIcon and DateBadge, added useToggle hook, added loader for svg symbols on body ([d3df2ac](https://gitlab.com/ceboola/personal-blog/commit/d3df2ac14dac0be27c7bffdbaaba0c54c88dbf52))

### Bug Fixes

- gatsby ssr rendering ([c905921](https://gitlab.com/ceboola/personal-blog/commit/c90592137ff787a0936a20dddb0894d80828b397))
- import issue ([270fc00](https://gitlab.com/ceboola/personal-blog/commit/270fc00a9ccd70328212847909cd57609d651e32))

### Styling

- new styles for ArticleNav, TagsList ([81ff365](https://gitlab.com/ceboola/personal-blog/commit/81ff3659fa39d4b0e84658727e8905b1f4ae2f72))

### [0.2.3](https://gitlab.com/ceboola/personal-blog/compare/v0.2.2...v0.2.3) (2020-10-08)

### Features

- added PopularArticles, added PostsProvider context, added chmod on files ([f0b8f67](https://gitlab.com/ceboola/personal-blog/commit/f0b8f67a412a9f83f5fb195ba5d236b63505be88))

### Styling

- fix svg icon hover effect in footer ([b86524c](https://gitlab.com/ceboola/personal-blog/commit/b86524c20827a052c88bbb41f8e6a5aabb119c66))

### [0.2.2](https://gitlab.com/ceboola/personal-blog/compare/v0.2.1...v0.2.2) (2020-09-30)

### Features

- added Footer component ([bec63c7](https://gitlab.com/ceboola/personal-blog/commit/bec63c78ff6a04cdecc57f8266156c1f3c1db32f))
- added SocialMedia component ([c676b64](https://gitlab.com/ceboola/personal-blog/commit/c676b641a66e2f3bb7a401a678488599c4c81ad5))

### [0.2.1](https://gitlab.com/ceboola/personal-blog/compare/v0.2.0...v0.2.1) (2020-09-23)

### Features

- added CommentsCount component, featuredImage in single article ([88b57f4](https://gitlab.com/ceboola/personal-blog/commit/88b57f4c15ed016d9b33040d8f8c0ea9094da5a1))
- added counters for tags and articles to TagsList ([9e1372a](https://gitlab.com/ceboola/personal-blog/commit/9e1372ad63bb874538cef29364017da345bd06fb))
- added globalStyles, added backgroundImage feature, changed quality of image files ([12a0ee0](https://gitlab.com/ceboola/personal-blog/commit/12a0ee08271305ce5e50740cef5e020f0bb1c3ce))

### Styling

- added styling for Article and ArticleStart ([c483865](https://gitlab.com/ceboola/personal-blog/commit/c4838657afc818c5a46bb6e983671cc215bddfe9))

#### [v0.2.0](https://gitlab.com/ceboola/personal-blog/compare/v0.1.0...v0.2.0)

> 17 September 2020

- feat: added automated changelogs to project [`c5b6155`](https://gitlab.com/ceboola/personal-blog/commit/c5b6155097999d90512dec4feac3755212da51e1)

#### v0.1.0

> 17 September 2020

- Pbc5 [`#5`](https://gitlab.com/ceboola/personal-blog/merge_requests/5)
- test lighthouse comparision on MR [`#4`](https://gitlab.com/ceboola/personal-blog/merge_requests/4)
- Pbc3 [`#3`](https://gitlab.com/ceboola/personal-blog/merge_requests/3)
- Pbc2 [`#2`](https://gitlab.com/ceboola/personal-blog/merge_requests/2)
- first PR [`#1`](https://gitlab.com/ceboola/personal-blog/merge_requests/1)
- initial commit [`1baee87`](https://gitlab.com/ceboola/personal-blog/commit/1baee87adc93abc5c2d59736dbd153244044b7ac)
- lint, prettier, graphql schema, types [`48767c4`](https://gitlab.com/ceboola/personal-blog/commit/48767c44e35978fb97e0a3b0efc2b34e46a5182b)
- added jest configuration [`19f951b`](https://gitlab.com/ceboola/personal-blog/commit/19f951be39c4fe023ceb0035606713566f4e4e3d)
<!-- auto-changelog-above -->
- feat: added TagsList component
- feat: added TypeScript to project
- feat: added Comments component
- fix: build stage
- feat: added webpack aliases
- fix: types in project
