import { forwardRef, useImperativeHandle, useState } from 'react';

import { useTransition } from 'react-spring';

import {
  Container,
  Message,
  Button,
  Content,
  Life,
  Description,
  StyledIcon,
} from './styled';
import { MessageHubProps } from './types';

const MessageHub = forwardRef<unknown, MessageHubProps>(
  (
    { config = { tension: 225, friction: 20, precision: 0.1 }, timeout = 3000 },
    messageHubControllerRef,
  ) => {
    const [refMap] = useState(() => new WeakMap());
    const [cancelMap] = useState(() => new WeakMap());
    const [messages, setMessages] = useState<
      Array<{ key: string; message: string }>
    >([]);

    const transition = useTransition(messages, {
      key: (item: Record<string, string>) => item.key,
      from: { opacity: 0, maxHeight: 0, life: '100%' },
      enter: (item) => async (next) => {
        const diffParentChildHeight =
          refMap.get(item).offsetParent.clientHeight -
          refMap.get(item).clientHeight;
        cancelMap.set(item, () => {
          setMessages((state) => state.filter((i) => i.key !== item.key));
        });
        await next({
          opacity: 1,
          maxHeight:
            diffParentChildHeight +
            refMap.get(item).offsetTop * 2 +
            refMap.get(item).clientHeight,
          config,
        });
        await next({ life: '0%', config: { duration: timeout } });
        cancelMap.get(item)();
      },
      leave: () => async (next) => {
        await next({
          opacity: 0,
          maxHeight: 0,
          config,
        });
      },
    });

    useImperativeHandle(messageHubControllerRef, () => ({
      displayMessage: (message: string) => {
        setMessages((state) => {
          const newMessageKey = `${state.length + 1}`;
          return [...state, { key: newMessageKey, message }];
        });
      },
    }));

    if (typeof window === 'undefined') return null;

    return (
      <Container>
        {transition(({ life, ...style }, item) => (
          <Message style={style as never}>
            <Content>
              <Life style={{ right: life }} />
              <Description ref={(ref) => ref && refMap.set(item, ref)}>
                {item.message}
              </Description>
              <Button
                onClick={(e) => {
                  e.preventDefault();
                  e.stopPropagation();
                  cancelMap.has(item) && cancelMap.get(item)();
                }}
              >
                <StyledIcon
                  aria-label="close search"
                  name="close"
                  width={15}
                  height={15}
                />
              </Button>
            </Content>
          </Message>
        ))}
      </Container>
    );
  },
);

export default MessageHub;
