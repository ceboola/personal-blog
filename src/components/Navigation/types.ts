export interface Props {
  refs: React.RefObject<HTMLElement>;
  isSticky: boolean;
}

export interface NavData {
  name: string;
  url: string;
}
