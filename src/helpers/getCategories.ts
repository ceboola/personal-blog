interface getCategoriesProps {
  node: {
    frontmatter: {
      category: string;
    };
  };
}

export const getCategories = (
  items: getCategoriesProps[],
  defaultVal: string,
): string[] => {
  const categoryItems = items.map((item) => {
    return item.node.frontmatter.category;
  });
  const uniqueCategories = new Set(categoryItems);
  let categories = Array.from(uniqueCategories);
  categories = [defaultVal, ...categories];
  return categories;
};
