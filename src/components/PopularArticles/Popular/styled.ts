import Img, { FluidObject } from 'gatsby-image';
import styled from 'styled-components';

import DateBadge from '@common/DateBadge';
import TimeToRead from '@common/TimeToRead';
import { StyledIcon as Icon } from '@components/PopularArticles/styled';
import { media } from '@utils/media';

export const StyledDateBadge = styled(DateBadge)`
  line-height: 16px;
  right: 0;
  padding: 0 10px;
  transition: all 0.35s ease;
  z-index: 2;
  span {
    font-size: 11px;
    padding: 0;
    margin: 0;
  }
`;

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  flex: 0 0 calc(33.33333% - 20px);
  margin-left: 20px;
  margin-right: -1px;
  ${media.lg`
      margin-left: 15px;
      flex: 0 0 calc(33.33333% - 15px);
  `}
  ${media.md`
      margin-right: -16px;
      flex: 0 0 calc(45% - 10px);
  `}
  ${media.sm`
      margin: 0;
      flex: 0 0 100%;
  `}
`;

export const Heading = styled.div`
  display: inherit;
  justify-content: flex-start;
  align-items: center;
  width: 100%;
  font-weight: 900;
  font-size: 19px;
`;

export const PopularImage = styled(Img)<{ fluid: FluidObject }>`
  width: 125px;
  height: 125px;
  ${media.xxl`
      width: 100px;
      height: 100px;
  `}
  ${media.sm`
      width: 100%;
      height: 150px;
  `}
`;

export const InfoWrapper = styled.div`
  font-weight: 700;
  font-size: 16px;
  margin-left: 24px;
  position: relative;
  width: calc(100% - 124px);
  ${media.lg`
      margin-left: 13px;
      width: calc(100% - 113px);
  `}
  ${media.sm`
      margin-left: 10px;
      margin-right: -10px;
      font-size: 14px;
  `}
`;

export const Title = styled.span`
  background-image: linear-gradient(#00c899, #00c899);
  background-size: 0% 3px;
  background-position: 0% 100%;
  background-repeat: no-repeat;
  transition: 0.3s;
  padding-bottom: 4px;
  width: max-content;
`;

export const StyledTimeToRead = styled(TimeToRead)`
  position: absolute;
  bottom: 0;
  overflow: hidden;
  margin: 0;
  white-space: nowrap;
  & > div {
    align-items: unset;
  }
  ${media.xxs`
      white-space: normal;
  `}
`;

export const StyledIcon = styled(Icon)``;

export const PopularContainer = styled.div`
  position: relative;
  display: inherit;
  flex-direction: row;
  padding: 13.4px 0;
  img {
    transition: 0.3s !important;
    transform: scale(1);
  }
  & a {
    display: flex;
    box-shadow: none;
    color: #fff;
    width: 100%;
    &:hover {
      ${Title} {
        background-size: 100% 3px;
      }
      ${StyledDateBadge} {
        transition: all 0.25s ease;
        transform: translateY(-100%);
      }
      img {
        transform: scale(1.1);
        opacity: 0.1 !important;
        filter: grayscale(100%);
      }
      ${StyledIcon} {
        transform: translate(-50%, -50%) scale(1);
        transition: all 300ms 100ms cubic-bezier(0.175, 0.885, 0.32, 1.275);
      }
    }
    ${media.xxs`
        width: 100%;
    `}
  }
`;

export const HoverWrapper = styled.div`
  position: relative;
  background-color: #00c899;
  overflow: hidden;
  display: flex;
  ${media.sm`
      width: 100%;
      flex: 0 0 70%;
  `}
`;

export const Clamp = styled.div`
  height: 85px;
  margin-top: -3px;
  display: -webkit-box;
  -webkit-line-clamp: 3;
  -webkit-box-orient: vertical;
  overflow: hidden;
  ${media.sm`
      height: 72px;
  `}
`;
