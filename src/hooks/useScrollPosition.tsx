import { RefObject, useEffect, useState } from 'react';

import debounce from 'lodash.debounce';

interface State {
  state: {
    x: number;
    y: number;
    leftEdge: boolean;
    rightEdge: boolean;
    topEdge: boolean;
    bottomEdge: boolean;
  };
  isScrollable: boolean;
}

const useScrollPosition = (ref: RefObject<HTMLElement>): State => {
  if (process.env.NODE_ENV === 'development') {
    if (typeof ref !== 'object' || typeof ref.current === 'undefined') {
      console.error('`useScrollPosition` expects a single ref argument.');
    }
  }
  const [state, setState] = useState({
    x: 0,
    y: 0,
    leftEdge: false,
    rightEdge: true,
    topEdge: true,
    bottomEdge: false,
  });
  const [isScrollable, setIsScrollable] = useState(false);
  const RESET_TIMEOUT = 50;

  useEffect(() => {
    const refCurrent = ref.current;
    const handler = (): void => {
      if (refCurrent) {
        setState({
          x: refCurrent.scrollLeft,
          y: refCurrent.scrollTop,
          leftEdge:
            refCurrent &&
            refCurrent.scrollLeft > 0 &&
            refCurrent.scrollWidth > refCurrent.clientWidth,
          rightEdge:
            Math.floor(refCurrent.scrollLeft) !==
            refCurrent.scrollWidth - refCurrent.clientWidth,
          topEdge: refCurrent.scrollTop === 0,
          bottomEdge:
            refCurrent.scrollTop ===
            refCurrent.scrollHeight - refCurrent.offsetHeight,
        });
      }
    };

    const resizeHandler = (): void => {
      if (refCurrent) {
        setIsScrollable(refCurrent.scrollWidth > refCurrent.clientWidth);
      }
    };

    const debouncedPosition = debounce(handler, RESET_TIMEOUT);
    const debounceResize = debounce(resizeHandler, RESET_TIMEOUT + 500);

    if (refCurrent) {
      setIsScrollable(refCurrent.scrollWidth > refCurrent.clientWidth);
      refCurrent.addEventListener('scroll', debouncedPosition, {
        capture: false,
        passive: true,
      });
      window.addEventListener('resize', debounceResize, {
        capture: false,
        passive: true,
      });
    }

    return () => {
      if (refCurrent) {
        refCurrent.removeEventListener('scroll', debouncedPosition);
        window.removeEventListener('resize', debounceResize);
      }
    };
  }, [ref, state]);

  return { state, isScrollable };
};

export default useScrollPosition;
