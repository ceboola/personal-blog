import { useContext } from 'react';

import { useIntl } from 'gatsby-plugin-react-intl';
import { Helmet } from 'react-helmet';

import CommonIcon from '@common/CommonIcon';
import { PostsContext } from '@providers/PostsProvider';

import { Wrapper, Union } from './styled';

const Contact: React.FC = () => {
  const { data } = useContext(PostsContext);
  const { title } = data.site.siteMetadata;
  const intl = useIntl();

  return (
    <Wrapper>
      <Helmet
        title={`${intl.formatMessage({ id: 'contact.title' })} | ${title}`}
      />
      <h1>{intl.formatMessage({ id: 'contact.title' })}</h1>
      <div>
        {intl.formatMessage(
          {
            id: 'contact.description',
          },
          {
            email: (chunks: string) => (
              <Union>
                <CommonIcon name="emailto" width={24} height={24} />
                {chunks}
              </Union>
            ),
            discord: (chunks: string) => (
              <Union>
                <CommonIcon name="discord" width={24} height={24} />
                {chunks}
              </Union>
            ),
            irc: (chunks: string) => (
              <Union>
                <CommonIcon name="hash" width={24} height={24} />
                {chunks}
              </Union>
            ),
          },
        )}
      </div>
    </Wrapper>
  );
};

export default Contact;
