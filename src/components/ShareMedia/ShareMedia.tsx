import { forwardRef } from 'react';

import { useIntl } from 'gatsby-plugin-react-intl';
import { animated, useTrail, to } from 'react-spring';

import { shareMediaData } from './static';

import { Wrapper, Container, StyledIcon, Title } from './styled';

const ShareMedia = forwardRef<HTMLDivElement>((props, ref) => {
  const intl = useIntl();
  const currentLocation = typeof window !== `undefined` && window.location.href;
  const springs = useTrail(shareMediaData.length, {
    from: { sc: 0, op: 0 },
    to: { sc: 1, op: 1 },
    config: { mass: 1, tension: 200, friction: 20 },
  });

  return (
    <Wrapper {...props} ref={ref}>
      <Title>{intl.formatMessage({ id: 'share' })}</Title>
      {springs.map(({ sc, op, ...props }, index) => (
        <Container key={index}>
          <animated.div
            style={{
              ...props,
              transform: to([sc], (sc) => `scale(${sc})`),
              opacity: to([op], (op) => op),
            }}
            key={index}
          >
            <a
              href={`${shareMediaData[index].url}${currentLocation}`}
              target="_blank"
              rel="noreferrer"
            >
              <StyledIcon
                name={shareMediaData[index].icon}
                width={20}
                height={20}
              />
            </a>
          </animated.div>
        </Container>
      ))}
    </Wrapper>
  );
});

export default ShareMedia;
