import { Link } from 'gatsby';
import styled from 'styled-components';

import CommonIcon from '@common/CommonIcon';
import { media } from '@utils/media';
import { rhythm, styledScale } from '@utils/typography';

export const Header = styled.header`
  display: flex;
`;

export const Title = styled.h1`
  ${styledScale(1.5)};
  margin-bottom: ${rhythm(1.5)};
  margin-top: 0;
  width: 100%;
  display: inherit;
  justify-content: center;
  ${media.lg`
    ${styledScale(1.3)};
  `}
  ${media.sm`
    font-size: 9vw;
  `}
`;

export const SmallTitle = styled.h3`
  font-family: Montserrat, sans-serif;
  margin-top: 0;
  margin-left: 20px;
`;

export const StyledIcon = styled(CommonIcon)`
  width: 32px;
  height: 32px;
  margin-right: 8px;
  transition: transform 0.3s ease-out;
  display: inline-block;
  transform: rotate(180deg);
`;

export const StyledLink = styled(Link)`
  box-shadow: none;
  text-decoration: none;
  color: inherit;
  position: relative;
  display: flex;
  align-items: center;
  & :hover {
    ${StyledIcon} {
      transform: rotate(180deg) translate(20px);
    }
  }
`;
