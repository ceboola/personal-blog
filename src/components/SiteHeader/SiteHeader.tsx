import { memo } from 'react';

import { Title, SmallTitle, StyledLink, StyledIcon, Header } from './styled';
import { Props } from './types';

const SiteHeader: React.FC<Props> = ({ location, title }) => {
  const rootPath = location.pathname === '/' || location.pathname === '/pl/';
  const linkTo = /pl/.test(location.pathname) ? '/pl/' : '/';
  const header = rootPath ? (
    <Title>
      <StyledLink to={`${linkTo}`}>{title}</StyledLink>
    </Title>
  ) : (
    <SmallTitle>
      <StyledLink to={`${linkTo}`}>
        <StyledIcon name="arrow-right" />
        {title}
      </StyledLink>
    </SmallTitle>
  );
  return <Header>{header}</Header>;
};

export default memo(SiteHeader);
