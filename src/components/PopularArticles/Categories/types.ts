export interface ShadowProps {
  direction: 'left' | 'right';
  show: null | boolean;
}
