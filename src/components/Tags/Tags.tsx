import { useContext } from 'react';

import { useIntl } from 'gatsby-plugin-react-intl';
import kebabCase from 'lodash/kebabCase';
import { Helmet } from 'react-helmet';

import { PostsContext } from '@providers/PostsProvider';

import { Wrapper, List, Item, StyledLink, Title } from './styled';

const Tags: React.FC = () => {
  const { data, pageContext } = useContext(PostsContext);
  const { group } = data.allMarkdownRemark;
  const { title } = data.site.siteMetadata;
  const locale =
    pageContext.language !== 'en' ? `/${pageContext.language}` : '';
  const intl = useIntl();
  const sortedTags = group.sort(function (a, b) {
    return b.totalCount - a.totalCount;
  });

  return (
    <Wrapper>
      <Helmet title={`${intl.formatMessage({ id: 'tags' })} | ${title}`} />
      <div>
        <Title>{intl.formatMessage({ id: 'tags' })}</Title>
        <List>
          {sortedTags.map((tag) => (
            <Item key={tag.fieldValue}>
              <StyledLink to={`${locale}/tags/${kebabCase(tag.fieldValue)}/`}>
                {tag.fieldValue} ({tag.totalCount})
              </StyledLink>
            </Item>
          ))}
        </List>
      </div>
    </Wrapper>
  );
};

export default Tags;
