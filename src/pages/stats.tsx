import { graphql } from 'gatsby';

import Stats from '@components/Stats';

const StatsPage: React.FC = () => {
  return <Stats />;
};

export const pageQuery = graphql`
  query($slug: String!) {
    site {
      siteMetadata {
        title
      }
    }
    SummarizedStats {
      ...SummarizedStats
    }
    gitlab(name: { eq: "personal-blog" }) {
      ...CurrentProject
    }
    currentPageViews: pageViews(path: { eq: $slug }) {
      ...SitePageViews
    }
    projectStatsCloc {
      ...ProjectStatsClocFragment
    }
    totalProjectStatsCloc: projectStatsCloc {
      ...TotalProjectStatsClocFragment
    }
    totalSessionsSite: allPageViews {
      sum(field: totalSessions)
    }
    totalPageViewsSite: allPageViews {
      sum(field: totalCount)
    }
    allPages: allSitePage {
      totalCount
    }
  }
`;

export default StatsPage;
