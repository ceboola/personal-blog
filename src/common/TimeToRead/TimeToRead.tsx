import { memo } from 'react';

import { useIntl, FormattedNumber } from 'gatsby-plugin-react-intl';

import CommonIcon from '@common/CommonIcon';
import { useDimensions } from '@hooks/useDimensions';

import { TimeToRead, Wrapper } from './styled';

interface TimeToReadProps {
  time: number;
  show?: boolean;
  measurment?: boolean;
}

const PageStats: React.FC<TimeToReadProps> = ({
  time,
  show = true,
  measurment = true,
  ...otherProps
}) => {
  const intl = useIntl();
  const [{ width }, ref] = useDimensions();

  return (
    <TimeToRead ref={ref} show={show} {...otherProps}>
      <Wrapper>
        <CommonIcon name="stopwatch" width={15} height={15} />
        <FormattedNumber
          value={time}
          unit="minute"
          unitDisplay="long"
          style="unit"
        />{' '}
        {measurment
          ? width > 110 && intl.formatMessage({ id: 'read' })
          : intl.formatMessage({ id: 'read' })}
      </Wrapper>
    </TimeToRead>
  );
};

export default memo(PageStats);
