import styled from 'styled-components';

import CommonIcon from '@common/CommonIcon';
import { media } from '@utils/media';
import { rhythm } from '@utils/typography';

export const Wrapper = styled.footer`
  position: relative;
  width: 100vw;
  left: 50%;
  margin-left: -50vw;
  padding-top: ${`${rhythm(5)}`};
  margin-top: auto;
  & svg {
    transition: 0.3s;
    fill: white;
    & :hover {
      fill: #128723;
    }
  }
`;

export const StyledBackground = styled(CommonIcon)`
  position: relative;
  left: -1px;
  z-index: -1;
  margin-left: -15px;
`;

export const InfoWrapper = styled.div`
  height: 200px;
  width: 100%;
  flex-direction: row;
  display: flex;
  justify-content: space-around;
  align-items: center;
  margin: -1rem auto 0 auto;
  background-color: #17173f;
  color: white;
  font-weight: 700;
  padding: 32px 0;
  & a {
    box-shadow: none;
    color: inherit;
  }
  ${media.sm`
      flex-direction: column;
      height: max-content;
  `}
`;

export const StyledIcon = styled(CommonIcon)`
  display: inherit;
`;

export const CenterContainer = styled.div`
  display: inherit;
  flex-direction: column;
  align-items: center;
  & > div {
    display: inherit;
    align-items: center;
    margin: 4px 0;
    &:nth-child(4) {
      font-size: 11px;
      b {
        margin-left: 3px;
        color: #00c899;
      }
    }
  }
  a {
    &:nth-child(2) {
      margin-left: 22px;
    }
  }
  ${media.sm`
      order: 2;
      margin-bottom: 5px;
      width: 100%;
      margin-bottom: 32px;
  `}
`;

export const RightContainer = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  align-items: center;
  justify-content: center;
`;

export const BuildTime = styled.div`
  position: absolute;
  bottom: 0;
  right: 20px;
  font-weight: 400;
  font-size: 12px;
`;

export const PageViews = styled.div`
  font-size: 11px;
  max-width: 150px;
  margin-top: auto;
  line-height: 16px;
  b {
    font-size: 16px;
    color: #00c899;
  }
  ${media.sm`
      max-width: 233px;
      margin-bottom: 32px;
  `}
`;
