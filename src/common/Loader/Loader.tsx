import { StyledCommonIcon } from './styled';
import { LoaderProps } from './types';

const Loader = ({
  width = 10,
  height = 10,
  strokeColor = '#93bfec',
}: LoaderProps): JSX.Element => {
  return (
    <StyledCommonIcon
      name="spinner"
      width={width}
      height={height}
      strokeColor={strokeColor}
    />
  );
};

export default Loader;
