import { GatsbyLinkProps } from 'gatsby';

import { MarkdownRemarkGroupConnection } from '@typings/graphql-types';

export interface TagsCardProps extends React.HTMLProps<HTMLDivElement> {
  tags: MarkdownRemarkGroupConnection[];
  link: React.ComponentClass<GatsbyLinkProps<unknown>>;
  tag?: string[];
  totalPosts: number;
  slug?: string | null | undefined;
  revealed?: boolean;
}
